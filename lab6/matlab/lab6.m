FS = 48000;

Num = [0.00000000646533502168440,0,-.0000000323266751084220,0,0.0000000646533502168440,0,-0.0000000646533502168440,0,0.0000000323266751084220,0,-0.00000000646533502168440];
Den = [1,-7.87266849009268,29.7132118074529,-70.0358090331644,113.647817539747,-132.337631703249,111.916782564002,-67.9185530116680,28.3760666585173,-7.40387939613398,0.926133412887875];

% [Num,Den] = sos2tf(SOS,G);


[H,W] = freqz(Num,Den);

% original filter response without SOS
plot(W/(2*pi),20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency')
ylabel('Magnitude (dB)')
ylim([-40 10]);
xlim([0.09 0.12]);


%% Create
  
SOS_Num = [0.0331774610388562,0,-0.0331774610388562
           0.0331774610388562,0,-0.0331774610388562
           0.0223846361306439,0,-0.0223846361306439
           0.0223846361306439,0,-0.0223846361306439
           0.0117220794355669,0,-0.0117220794355669];

SOS_Den = [1,-1.53963958515431,0.992390511236914
           1,-1.62043849159704,0.993017925820779
           1,-1.54675738172820,0.980496237929246
           1,-1.59688581879472,0.981501228904223
           1,-1.56894721281840,0.976555841128866];


[H1,W1] = freqz(SOS_Num(1,:),SOS_Den(1,:));
[H2,W2] = freqz(SOS_Num(2,:),SOS_Den(2,:));
[H3,W3] = freqz(SOS_Num(3,:),SOS_Den(3,:));
[H4,W4] = freqz(SOS_Num(4,:),SOS_Den(4,:));
[H5,W5] = freqz(SOS_Num(5,:),SOS_Den(5,:));

H = H1.*H2.*H3.*H4.*H5;

% plot second order stringed filter
subplot(2,1,1)
plot(W1/(2*pi)*FS,20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency (Hz)')
ylabel('Magnitude (dB)')
ylim([-40,10])

subplot(2,1,2)
% plot direct form filter
[H,W] = freqz(Num,Den);
plot(W/(2*pi)*FS,20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency (Hz)')
ylabel('Magnitude (dB)')
ylim([-40,10])


