% x = 1:24;
% h = [1 2 3 4 5 6 7 8 9 10 11 12 13];
x = 1:10;
h = [0,1,2,3,4,5,6,7,8,9,10,11];
array_to_bin(x,'../data/x.bin');
array_to_bin(h,'../data/h.bin');

% the sampling rate of x[n] by the conversion factor L/M = 3/2. 
e0 = h(1:2:length(h)); e1 = h(2:2:length(h));
e00 = e0(1:3:length(e0)); e01 = e0(2:3:length(e0)); e02 = e0(3:3:length(e0));
e10 = e1(1:3:length(e1)); e11 = e1(2:3:length(e1)); e12 = e1(3:3:length(e1));

% Down-sampling and polyphase filtering
u02 = x(1:2:end);
u12 = [0 x(2:2:end-1)]; 

x00 = filter(e00,1,u02); % *1  3  5 *7  9  11 *13...
x01 = filter(e01,1,u02); %  1 *3  5  7 *9  11  13...
x02 = filter(e02,1,u02); %  1  3 *5  7  9 *11  13...

x10 = filter(e10,1,u12); % *2 4 6 *8 10 12
x11 = filter(e11,1,u12);
x12 = filter(e12,1,u12); 

% Up-sampling
yy0 = [x00;x01;x02];
yy1 = [x10;x11;x12];
y0 = yy0(:);
y1 = yy1(:);
yy = [0,(y0(1:length(y0)-1))'];
y = (yy'+y1);
y'
