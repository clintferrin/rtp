function plot_audio_time(file_in,min,max)
[x,fs] = audioread(file_in);
t = (0:length(x) - 1)/fs;
plot(t,x);
xlabel('Time(seconds)');
ylabel('Amplitude');
title(file_in);

if(nargin == 3)
    xlim([min max]);
end   

return;