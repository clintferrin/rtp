addpath('../data')
% audio_to_bin('../data/LMS-2kHz_sine_1.9kHz_square.wav','../data/LMS-2kHz_sine_1.9kHz_square.bin');
% audio_to_bin('../data/LMS-300Hz_sine_350Hz_square.wav','../data/LMS-300Hz_sine_350Hz_square.bin');
% audio_to_bin('../data/LMS-white-noise.wav','../data/LMS-white-noise.bin');

% right = ch. 1, left = ch. 2
square300='../data/LMS-300Hz_sine_350Hz_square.wav'; % ch1: noise, ch. 2: square
sine2k='../data/LMS-2kHz_sine_1.9kHz_square.wav'; % ch1: noise (sine), ch2: desired+noise
white_noise='../data/LMS-white-noise.wav'; % ch1 (right) noise+desired, ch. 2 (left) noise

[x, fs] = audioread(sine2k);  
% x(1,2)
% plot_audio_time(square300,0,.01);
plot_audio_time(sine2k,0,.003);
% plot_audio_time(white_noise,0,.01);
% plot(x(1:200,2))
hold on
 
bin_to_audio('../data/sine2k.bin');
plot_audio_time('../data/sine2k.wav',1,1.003);
hold off