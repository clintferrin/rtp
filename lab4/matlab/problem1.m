addpath('~/Documents/MATLAB/matlab2tikz/src/');
% coefficients for filter

% Cutoff (Fp=1807 Fs=1867 h[n]
set(gca, 'FontName', 'lmroman10-regular.otf')

% plot filter in time domain
plot(h);
title("Impulse Response of FIR Filter")
xlabel("Bin Number")
ylabel("Magnitude")
xlim([289,589]);
% matlab2tikz('../report/media/myfigure.tex');

% matlab2tikz('../report/media/h-impulse-response.tex','extraAxisOptions','scale=\figurescale');
% save_fig('../report/media/matplot/h-impulse-response.pdf',gcf)

% plot magnitude response of filter (frequency domain)
[H,W] = freqz(h,1);
plot(W/(2*pi),20*log10(abs(H)));
ylim([-95 5]);
xlim([0 0.5]);
title("Magnitude Response (dB)")
xlabel("Frequency")
ylabel("Magnitude (dB)")
% matlab2tikz(char("../report/media/matplot/H-mag-response.tex"),'extraAxisOptions','scale=\figurescale');
save_fig('../report/media/matplot/H-mag-response.pdf',gcf)
 
 
% plot frequency response of filter
[H,W] = freqz(h,1);
plot(W/(2*pi),unwrap(angle(H)));
ylim([-500,0]);
title("Phase Response");
xlabel("Frequency");
ylabel("Phase (radians)");
% matlab2tikz('../report/media/matplot/H-phse-response.tex','extraAxisOptions','scale=\figurescale');
% save_fig('../report/media/matplot/H-phse-response.pdf',gcf)



