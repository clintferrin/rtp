%% Matlab Convolution Test
x = 1:24;
h = [1 2 3 4 5 6 7 8 9 10 11 12];
x = 1:15;
h = [1,2,3,4,5,6,7,8,9,10,11,12];

%% up/down sample
U=3;
D=2;


% x_up = reshape(vertcat(x,zeros(size(x,2),U-1)'),1,[]);
% x_filt = conv(x_up,h);
% x_filt = x_filt(1:end-size(h,2)+1);
% y = x_filt(1:D:size(x_filt,2));
% y

% x_down = downsample(x,D);
% y = conv(x_down,h);
% y
% 
% x_filt = conv(x_down,upsample(h,3));
% downsample(x_filt,3);
% y

% e0 = h(1:2:length(h)); e1 = h(2:2:length(h));
% 
% h0=h(1:D:end);
% h1=[h(2:D:end) 0];
% h2=[h(3:D:end) 0];
% h3=[h(4:D:end) 0];
% 
% h0=h(1:D:end/2);
% h1=h(2:D:end/2);
% h2=h(3:D:end/2);          
% 
% 
% x0=[x(1:D:end) 0];
% x1=[0 x(3:D:end)];
% x2=[0 x(2:D:end)];



% x3=[0 x(2:D:end)];
% y_poly_dec=filter(h0,1,x0)+filter(h1,1,x1)+filter(h2,1,x2)+filter(h3,1,x3)
% y_poly_dec=filter(h0,1,x0)+filter(h1,1,x1)+filter(h2,1,x2)
% y_poly_dec=filter(h0,1,x0)+filter(h1,1,x1)


% % downsample(conv(x,h),D);  
downsample(filter(h,1,upsample(x,U)),D)
% downsample(filter(h,1,x),D)


%% poly-phase test
% down = cell(3);

% down{1} = [11,12,13];
% down{1} = downsample(x,D);
% down{1}
% down{1} = conv(down{1},h(1:D:end));
% down{1} = conv(h(1:D:end),x);
% down{1}
% down{1} = down{1}(1:end-1);


% down{2} = downsample(x(2:end),D);
% down{2} = conv(down{2},h_flip(2:2));
% down{2} = down{2}(1:end-1);

% down{3} = downsample(x(3:end),D);
% down{3}(end+1:numel(down{1})) = 0;
% down{3} = conv(down{3},h_flip(3:3));
% down{3} = down{3}(1:end-1);

% out_0 = down{1} + down{2} + down{3}

% x_up = upsample(x,U);
