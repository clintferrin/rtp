% matplot = "../report/media/values/poly-" + num2str(f0);
% value =  "../report/media/matplot/poly-" + num2str(f0);
f0 = 5/3

% create and save x[n]
N = 512;
t = 878000;
n = 0:t-1;
freq = linspace(0,1,t);
x = cos(2*pi*f0*n);
[~,I] = max(abs(fft(x,N)));
% write_value((I/N),matplot+".dat
% plot(freq,20*log10(abs(fft(x))/t))
plot(freq,abs(fft(x))/t)


