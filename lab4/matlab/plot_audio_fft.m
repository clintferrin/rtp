function plot_audio_fft(file_in, min, max, bps) % bps = bits per sample
info = audioinfo(file_in);
[x, sample_rate]= audioread(file_in);

i1 = int32(min * sample_rate);         % convert time to index
i2 = int32(max * sample_rate);         % convert time to index

if(nargin <= 3)
   bps = 16;
end  

nfft = 2^bps;                           % FFT size
% freq = ((0:nfft - 1)/nfft - 0.5) * sample_rate; % frequency [Hz]
freq = ((0:nfft - 1)/nfft); % frequency [Hz]

X = fft(x(i1:i2), nfft);                % compute the discrete - Fourier transform

graph_title = sprintf('Fourier Transform  of %s', file_in);
% plot(freq, 20 * log10(abs(fftshift(X)))); % plot with accurately scaled frequency axis
plot(freq, 20 * log10(abs(X))); % plot with accurately scaled frequency axis
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
title(graph_title);


return;