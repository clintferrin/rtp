% Clint Ferrin
% Tue Nov 7, 2017
%% Problem 6
 
% Cutoff (Fp=1807 Fs=1867 h[n]

array_to_bin(h,'../data/h.bin');
audio_to_bin('../data/ghostbustersray.wav','../data/ghostbustersray.bin');

! ../build/sample_conv ../data/ghostbustersray.bin ../data/ghostbustersray_edited.bin ../data/h.bin 3 2
bin_to_audio('../data/ghostbustersray_edited.bin','../data/ghostbustersray_edited.wav');
[y,Fs] = audioread('../data/ghostbustersray_edited.wav');
Fs
sound(y,Fs);