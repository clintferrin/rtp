%% Problem 4
freq = [1/16 1/8 1/4];
N = 512;
t = 878;x
count = 0;

for f0 = freq
    % create and save h[n]
%     array_to_bin(h,'../data/h.bin')
    
    % set file path for saved variables
    value = "../report/media/value/poly-" + num2str(f0);
    matplot =  "../report/media/matplot/poly-" + num2str(f0);

    % create x[n]
    n = 0:t-1;
    freq = linspace(0,1,t);
    x = cos(2*pi*f0*n);
    subplot(1,2,1) 
    plot(x)
    xlim([0,50]);

%     matlab2tikz(char(matplot+"-cos.plot"),'extraAxisOptions','scale=\figurescale');

    % report frequency/plot
    [~,I] = max(abs(fft(x,N)));
    write_value(num2str(round(I/N,3)),value+".val");
    subplot(1,2,2) 
    plot(freq,20*log10(abs(fft(x))/t))
    
    % execute c++ function on data
    array_to_bin(x,'../data/x.bin')

    ! ../sample_conv_full ../data/x.bin ../data/yhat.bin ../data/h.bin 3 2
   
    % reconstruct filtered data and analyze
    y = bin_to_array('../data/yhat.bin');
    
    t = size(y,1);
    n = 0:t-1;
    freq = linspace(0,1,t);
    x = cos(2*pi*f0*n);
    [~,I] = max(abs(fft(y,N)));
    write_value(num2str(round(I/N,3)),value+"-hat.val");
    
    hold on
    plot(freq,20*log10(abs(fft(y)/t)))
    ylim([-60,10])
    hold off
    count = count +1;
    x0=10;
    y0=10;
    width=550;
    height=200;
    set(gcf,'units','points','position',[x0,y0,width,height])
    save_fig(char(matplot+"-mag.pdf"),gcf)

%     matlab2tikz(char(matplot+"-mag.plot"),'extraAxisOptions','scale=\figurescale');

end