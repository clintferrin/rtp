function plot_spectrogram(file_in, min, max, bps)
[x, fs] = audioread(file_in);
info = audioinfo(file_in);

if(nargin ~= 4 )
   bps = info.BitsPerSample;    % use largets bps possible
end   

nfft = 2^bps;                   % fft size
overlap = round(0.8 * nfft);
window = hamming(nfft);
spectrogram(x, window, overlap, nfft, fs, 'yaxis');
if(nargin > 1) 
    ylim([min, max]); 
end

title(['Spectrogram of ' file_in]);
grid on;
return;