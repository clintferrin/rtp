#include <iostream>
#include <vector>
#include <string.h>
#include <cmath>
#include <unistd.h>    

const int IOBUFFSIZE = 1024;

typedef struct {
    int ndim;   // number of dimensions
    int nchan;  // number of channels
    int len;    // length of first dimension
    int fs;     // length of second dimension or sample rate
    int empty;  // length of third dimension
} sound_header;

void print_arr(double arr[], int size){
    for (int i = 0; i < size; i++) {
        std::cout << arr[i] << " ";    
    }
    std::cout << std::endl;
}

FILE* read_binary(char* file){
    FILE *f;
    if (NULL == (f = fopen(file, "rb"))) {
        std::cout << "Cannot find file: " << file << std::endl;
        exit (EXIT_FAILURE);
    }
    return f;
}

FILE* write_binary(char* file){
    FILE *f;
    if (NULL == (f = fopen(file, "wb"))) {
        std::cout << "Cannot open file to write: " << file << std::endl;
        exit (EXIT_FAILURE);
    }
    return f;
}

void down_sample(double x[], double* y, int y_len,  int D){
    for (int i = 0; i <= y_len; i++) {
        y[i] = x[i*(D+1)];
    }
}

void up_sample(double x[], double* y, int x_len, int U){
    std::cout << x_len << std::endl;
    for (int i = 0; i < x_len; i++) {
        y[i*(U+1)] = x[i];
    }
}
void update_h(double *x, int buf_index, double *h, int len, double beta, double e){
    int i;
    for (i = 0; i < len; i++) {
        h[i] = h[i]+beta*e*x[(i+buf_index) % len];
    }
}

int main(int argc, char* argv[]){
    if (argc < 2) {
        std::cout << "file_in file_out file_h"  << std::endl;
    }

    char* file_in = argv[1];
    int noise_only_left = atoi(argv[2]);
    char* file_out = argv[3];
    double beta = atof(argv[4]);

    // read in data files
    FILE *fx, *fy;
    fx = read_binary(file_in);
    fy = write_binary(file_out);

    // read in headers for data files
    sound_header header_x, header_y;
    fread(&header_x, sizeof(header_x), 1, fx);

    // write out header information for y
    memcpy(&header_y, &header_x, sizeof(header_x));
    header_y.nchan = 1;
    fwrite(&header_y, sizeof(header_x), 1, fy);
    
    // initialize variables for sampling
    int x_len, y_len = 0; // indices for input and output buffers
    int buf_index  = 0;   // index for circular data buffer
    int y_down_len = 0;   // new length of down-sampled output
    double long y_n;      // variable for accumulating convolution result
    double e;

    int h_len = 256;
    double *h = (double*)calloc(h_len, sizeof(double));

    h[0] = 1;

    double x[IOBUFFSIZE], y[IOBUFFSIZE];
    double *x_buf = (double*)calloc(h_len, sizeof(double));
    double d;

    x_len = fread(x,sizeof(double),IOBUFFSIZE,fx);// Read in first input samples

    int noise = 1;
    int desired = 0;

    if (noise_only_left == 0) {
        noise = 0;
        desired = 1;
    }

    while(x_len>0) {
        for(int i=0; i<x_len/2; i++) {
            buf_index = (buf_index+h_len-1) % h_len;

            d = x[i*2+noise];
            x_buf[buf_index] = x[i*2+desired];
            
            // Convolution loop
            y_n=0.0;
            for (int n=0; n<h_len; n++) { 
                y_n += h[n] * x_buf[(n+buf_index) % h_len];
            }


            e = d-y_n;

            update_h(x_buf,buf_index,h,h_len,beta,e);
            
            // save result into output buffer
            y[y_len] = e;
            y_len++;

            // save result to output file
            if (y_len == IOBUFFSIZE) {   
                fwrite(y, sizeof(double), y_len, fy);
                y_len = 0;
            }
        }                                       
        x_len = fread(x,sizeof(double),IOBUFFSIZE,fx); // Read in next chunk of input samples
    }


    // write out any remaining data in the buffer
    if(y_len>0) {
        fwrite(y,sizeof(double),y_down_len,fy); // Write the output buffer
        y_len = 0;                               // Reset the index for the output buffer
    }

    fclose(fx);
    fclose(fy);
    free(x_buf);
    return 0;
}
