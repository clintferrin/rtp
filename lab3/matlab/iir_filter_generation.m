Num = [0.0158848293243488,0,-0.0635393172973950,0,0.0953089759460926,0,-0.0635393172973950,0,0.0158848293243488];
Den = [1,-4.16519921294926,8.75269308756112,-12.1372719058494,12.1731377054406,-8.93776989370867,4.68806418875380,-1.61381826372516,0.289942721148074];

[H,W] = freqz(Num,Den);

subplot(2,1,1)
plot(W/(2*pi),20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency')
ylabel('Magnitude (dB)')
ylim([-50 5]);
xlim([0 0.5]);

subplot(2,1,2)
% plot frequency response of filter
[H,W] = freqz(h,1);
plot(W/(2*pi),unwrap(angle(H)));
% xlim([.095,.195]);
title('Phase Response');
xlabel('Frequency');
ylabel('Phase (radians)');

