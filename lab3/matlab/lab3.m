FS = 48000;
SO = [1,0,-1,1,-0.845057100107812,0.925474523703578
      1,0,-1,1,-1.46584306988468,0.944936436225675
      1,0,-1,1,-1.26569787427489,0.855163353611160
      1,0,-1,1,-1.01064799164726,0.836282737599747];
  
Mag =[0.194541995843674
      0.194541995843674
      0.109202904106442
      0.109202904106442
      0.944060876285923];
 
[Num,Den] = sos2tf(SO,Mag);

[H1,W1] = freqz(SO(1,1:3)*Mag(1),SO(1,4:6));
[H2,W2] = freqz(SO(2,1:3)*Mag(2),SO(2,4:6));
[H3,W3] = freqz(SO(3,1:3)*Mag(3),SO(3,4:6));
[H4,W4] = freqz(SO(4,1:3)*Mag(4),SO(4,4:6));

H = H1.*H2.*H3.*H4*Mag(5);

% plot second order stringed filter
subplot(2,1,1)
plot(W1/(2*pi)*FS,20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency (Hz)')
ylabel('Magnitude (dB)')
ylim([-50,10])

subplot(2,1,2)
% plot direct form filter
[H,W] = freqz(Num,Den);
plot(W/(2*pi)*FS,20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency (Hz)')
ylabel('Magnitude (dB)')
ylim([-50,10])
