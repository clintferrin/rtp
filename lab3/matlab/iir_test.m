x = [1 0 0 0 0];

SO = [1,0,-1,1,-0.845057100107812,0.925474523703578];
Mag = 0.194541995843674;

[Num,Den] = sos2tf(SO,Mag);
output1 = filter(Den,Num,x);

Num = [1 0 -1];
Den = [1 -0.845057100107812 0.925474523703578];

output2 = filter(Den,Num*Mag,x);

output1
output2
