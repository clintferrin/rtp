#include <iostream>
#include <unistd.h>    
#include <time.h>

typedef struct lat_ladder_coef {
    float *k;
    float *v;
}lat_ladder_coef;

void print_arr(float *a, int len) {
    for (int i = 0; i < len; i++) {
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;
}

void swap_arr(float **a, float **b) {
    float *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void copy_arr(float *a, float *b,int len) {
     for (int i = 0; i < len; i++) {
         a[i] = b[i];
     }
}

void calc_coefs(lat_ladder_coef *coefs,float Num[],float Den[], int16_t len) {
    int16_t stage;
    float *a_c= (float*)calloc(len,sizeof(float));
    float *a_n= (float*)calloc(len,sizeof(float));
    float *c_c  = (float*)calloc(len,sizeof(float));
    float *c_n  = (float*)calloc(len,sizeof(float));

    copy_arr(a_c,Den,len);
    copy_arr(c_c,Num,len);
     
    stage = len-1;
    while (stage != 0) {
        coefs->k[stage-1] = a_c[stage];
        coefs->v[stage] = c_c[stage];
        for (int i = 0; i < stage; i++) {
            c_n[i] = c_c[i]-coefs->v[stage]*a_c[stage-i];
            a_n[i] = (a_c[i] - coefs->k[stage-1]*a_c[stage-i])/(1-coefs->k[stage-1]*coefs->k[stage-1]);
        }
        swap_arr(&a_n,&a_c);
        swap_arr(&c_n,&c_c);
        stage--;
    }
    coefs->v[stage] = c_c[stage];
}

int main() {
    // float Num[] = { 1,   2,   3,  2};
    // float Den[] = { 1, 0.9,-0.8,0.5};
    float Num[] = { 1,-0.8, 0.15};
    float Den[] = { 1, 0.1,-0.72};
    int16_t len = 3;

    lat_ladder_coef coefs;
    coefs.k = (float*)calloc(len-1,sizeof(float));
    coefs.v = (float*)calloc(len,sizeof(float));

    calc_coefs(&coefs,Num,Den,len);

    std::cout << "K coefficients:" << std::endl;
    print_arr(coefs.k,len-1); std::cout << std::endl;

    std::cout << "V coefficients:" << std::endl;
    print_arr(coefs.v,len);

    return 0;
}
