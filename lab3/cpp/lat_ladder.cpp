#include <iostream>
#include <unistd.h>    
#include <time.h>

typedef struct lat_ladder_coef {
    float *k;
    float *v;
}lat_ladder_coef;

void print_arr(float *a, int len) {
    for (int i = 0; i < len-1; i++) {
        std::cout << a[i] << ", ";
    }
    std::cout << a[len-1] << std::endl;
    std::cout << std::endl;
}

void swap_arr(float **a, float **b) {
    float *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void copy_arr(float *a, float *b,int len) {
    for (int i = 0; i < len; i++) {
        a[i] = b[i];
    }
}

void calc_coefs(lat_ladder_coef *coefs,float num[],float den[], int16_t len) {
    int16_t stage;
    float *a_c= (float*)calloc(len,sizeof(float));
    float *a_n= (float*)calloc(len,sizeof(float));
    float *c_c  = (float*)calloc(len,sizeof(float));
    float *c_n  = (float*)calloc(len,sizeof(float));

    copy_arr(a_c,den,len);
    copy_arr(c_c,num,len);

    stage = len-1;
    while (stage != 0) {
        coefs->k[stage-1] = a_c[stage];
        coefs->v[stage] = c_c[stage];
        for (int i = 0; i < stage; i++) {
            c_n[i] = c_c[i]-coefs->v[stage]*a_c[stage-i];
            a_n[i] = (a_c[i] - coefs->k[stage-1]*a_c[stage-i])/(1-coefs->k[stage-1]*coefs->k[stage-1]);
        }
        swap_arr(&a_n,&a_c);
        swap_arr(&c_n,&c_c);
        stage--;
    }
    coefs->v[stage] = c_c[stage];
}

float lat_iir(float x, float *k, float *v, float *g, int16_t len) {
    int16_t i;
    float y = 0; 

    for (i = 0; i < len; i++) {
        x = x-g[i+1]*k[len-1-i];
        g[i] = x*k[len-1-i]+g[i+1];
        y += g[i]*v[len-i];
    }
    g[len]=x;
    y += x*v[0];
    return y;
}

float direct_2(float x, float *num, float *den, float *g, int16_t len) {
    int16_t i;
    float y = 0; 
    for (i = 0; i < len; i++) {
        x += g[i]*(-den[i+1]);
        y += g[i]*(num[i+1]);
    }

    for (i = 0; i < len-1; i++) {
        g[len-1-i] = g[len-2-i];
    }
    g[0]=x;
    return y+x*num[0];
}

int main() {
    int16_t i;
    // float num[] = { 1,   2,   3,  2};
    // float den[] = { 1, 0.9,-0.8, .5};
    float num[] = { 1, -.8, 0.15};
    float den[] = { 1,  .1, -.72};
    int16_t filt_len = 3;

    float input[] = {1,0,0,0};
    float input_len = 4;
    float x;
    float y;


    lat_ladder_coef coefs;
    coefs.k = (float*)calloc(filt_len-1,sizeof(float));
    coefs.v = (float*)calloc(filt_len,sizeof(float));
    float *g = (float*)calloc(filt_len,sizeof(float));

    calc_coefs(&coefs,num,den,filt_len);

    std::cout << "Lattice Ladder for IIR" << std::endl;
    for (i = 0; i < input_len; i++) {
        x = input[i];
        y = lat_iir(x,coefs.k,coefs.v,g,filt_len-1);
        std::cout << y << std::endl;
    }


    float *g2 = (float*)calloc(filt_len,sizeof(float));
    std::cout << std::endl << "Type II Direct Filter" << std::endl;
    for (i = 0; i < input_len; i++) {
        x = input[i];
        y = direct_2(x, num, den, g2, filt_len);
        std::cout << y << std::endl;
    }


    free(g);
    free(coefs.k);
    free(coefs.v);
    return 0;
}
