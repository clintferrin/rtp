#include <iostream>
#include <unistd.h>    
#include <math.h>
#include <stdio.h>
#define PI 3.1415926535897932384626

void digitrev_index(short *index, int n, int radix);
void bitrev(float *xs, short *index, int n);
void cfftr2_dit(float* x, float* w, short n);

void calc_twid(float twid[], int len);
void find_x_mag(float x[], float x_mag[], float scale, short len);
void set_scale(float scale);

void print_farr(float arr[], int len);
void print_sarr(short arr[], int len);

int main() {
    short N = 8;        // data length
    short radix = 2;    // radix number
    float W[N];         // twiddle factor array
    short i_twid[N/2];  // W index
    short i_data[N];    // input data index
    float x_mag[N];     // mag vector (squared mag) 
    float scale;        // scale factor for x_mag 

	float x[] = {0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0};

    // initialize twiddle factors w/ index
    scale = pow(10,-1);
    calc_twid(W,N);     // must use total N
	digitrev_index(i_twid,N/radix,radix);
	bitrev(W,i_twid,N/radix);

    // calculate fft
    cfftr2_dit(x,W,N);

    // recompose output data 
	digitrev_index(i_data,N,radix);
	bitrev(x,i_data,N);

    // find magnitude squared
    find_x_mag(x,x_mag,scale,N);

    // output results to verify
    print_farr(x,N*2);
    print_farr(x_mag,N);
    
     
    return 0;
}

void calc_twid(float twid[], int len) {
	int i;
	for(i = 0; i < len/2; i++) {
		twid[i*2] = cos(2*PI/len*i);		// real
		twid[i*2+1] = sin(2*PI/len*i);	// image
	}
}

void digitrev_index(short *index, int n, int radix) {
    int		i,j,k;
    short	nbits, nbot, ntop, ndiff, n2, raddiv2; 

    nbits = 0;
    i = n;	
    while (i > 1){
        i = i >> 1;
        nbits++;
    }

    raddiv2	= radix >> 1;
    nbot	= nbits >> raddiv2;
    nbot	= nbot << raddiv2 - 1;
    ndiff	= nbits & raddiv2;
    ntop	= nbot + ndiff;
    n2		= 1 << ntop;

    index[0] = 0;
    for ( i = 1, j = n2/radix + 1; i < n2 - 1; i++){
        index[i] = j - 1;
        for (k = n2/radix; k*(radix-1) < j; k /= radix)
                j -= k*(radix-1);
        j += k;
    }
    index[n2 - 1] = n2 - 1;
}
void bitrev(float *xs, short *index, int n) {
    int    i;
    short  i0, i1, i2, i3;
    short  j0, j1, j2, j3;
    double xi0, xi1, xi2, xi3;
    double xj0, xj1, xj2, xj3;
    short  t;
    int    a, b, ia, ib, ibs;
    int    mask;
    int    nbits, nbot, ntop, ndiff, n2, halfn;
    double *x ;
    x = (double *)xs ;
    
    nbits = 0;
    i = n;
    while (i > 1)
    {
       i = i >> 1;
       nbits++;
    }
    
    nbot    = nbits >> 1;
    ndiff   = nbits & 1;
    ntop    = nbot + ndiff;
    n2      = 1 << ntop;
    mask    = n2 - 1;
    halfn   = n >> 1;
    for (i0 = 0; i0 < halfn; i0 += 2)
    {
        b       = i0 & mask;
        a       = i0 >> nbot;
        if (!b) ia = index[a];
        ib      = index[b];
        ibs     = ib << nbot;
    
        j0      = ibs + ia;
        t       = i0 < j0;
        xi0     = x[i0];
        xj0     = x[j0];
    
        if (t)
        {
          x[i0] = xj0;
          x[j0] = xi0;
        }
    
        i1      = i0 + 1;
        j1      = j0 + halfn;
        xi1     = x[i1];
        xj1     = x[j1];
        x[i1] = xj1;
        x[j1] = xi1;
    
        i3      = i1 + halfn;
        j3      = j1 + 1;
        xi3     = x[i3];
        xj3     = x[j3];
        if (t)
        {
          x[i3] = xj3;
          x[j3] = xi3;
        }
      }
}

void cfftr2_dit(float* x, float* w, short n) {
    short n2, ie, ia, i, j, k, m;
    float rtemp, itemp, c, s;

    n2 = n;
    ie = 1;

    for(k=n; k > 1; k >>= 1)
    {
        n2 >>= 1;
        ia = 0;
        for(j=0; j < ie; j++)
        {
            c = w[2*j];
            s = w[2*j+1];
            for(i=0; i < n2; i++)
            {
                m = ia + n2;
                rtemp     = c * x[2*m]   + s * x[2*m+1];
                itemp     = c * x[2*m+1] - s * x[2*m];
                x[2*m]    = x[2*ia]   - rtemp;
                x[2*m+1]  = x[2*ia+1] - itemp;
                x[2*ia]   = x[2*ia]   + rtemp;
                x[2*ia+1] = x[2*ia+1] + itemp;
                ia++;
            }
            ia += n2;
        }
        ie <<= 1;
    }
}

void find_x_mag(float x[], float x_mag[], float scale, short len) {
    for (int i = 0; i < len; i++) {
        x_mag[i] = x[i*2]*x[i*2];
        x_mag[i] += x[i*2+1]*x[i*2+1];
        x_mag[i] *= scale;
    }
}

void print_farr(float arr[], int len) {
	int i;
	for(i = 0; i < len-1; i++) {
		printf("%.2f\n",arr[i]);
	}
    printf("%.2f\n\n",arr[len-1]);
}

void print_sarr(short arr[], int len) { 
	int i;
	for(i = 0; i < len-1; i++) {
		printf("%u\n",arr[i]);
	}
    printf("%u\n\n",arr[len-1]);
}
