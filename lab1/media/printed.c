int main(void)
{
    int16_t sample = 0;				// Sample index into the buffer array
    float cosDataPrev;       			// Cos Sample
    float delDataPrev;       			// Sine Sample
    float amplitude = AMPLITUDE;	// Amplitude of the generated sin wave

    EVMOMAPL138_init();				// Initialize the board
    EVMOMAPL138_initRAM();			// Set up the RAM
    EVMOMAPL138_enableDsp();			// Wake up the DSP

    // init the i2c for all to use.
    USTIMER_init();					// General use timers
    I2C_init(I2C0, I2C_CLK_400K);	// I2C initialization

    SetGpio();						// Configure the General Purpose I/O
    McASP_Init();					// Initialize McASP
    AIC3106_Init();					// Initialize AIC3106
    McASP_Start();					// Start McASP



    float w_0 = 2.0*PI*SIN_FREQ/(SAMP_FREQ);
    float cosData = cos(w_0);       			// Cos Sample
    float delData = -sin(w_0);       			// Sine Sample


    // Infinite loop:  	Each loop reads/writes one sample to the left and right channels.
    while (1){
        cosDataPrev = cosData;
        delDataPrev = delData;

        // Calculate the next sine wave sample...
        cosData = cos(w_0) * cosDataPrev - sin(w_0) * delDataPrev;
        delData = sin(w_0) * cosDataPrev + cos(w_0) * delDataPrev;

        // Store the new sample in the buffer, for viewing...
        sample = (sample+1)%BUFF_SIZE;      // Update the sample indx

        // wait for xmit ready and send a sample to the left channel.
        while (!CHKBIT(MCASP->SRCTL11, XRDY)) {}
        MCASP->XBUF11 = (int16_t) (amplitude * cosData);

        // wait for xmit ready and send a sample to the right channel.
        while (!CHKBIT(MCASP->SRCTL11, XRDY)) {}
        MCASP->XBUF11 = (int16_t) (amplitude * delData);
    }
}
