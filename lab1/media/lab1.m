w_0 = 2*pi*3000/(24000*8);
amp = 20000;
cosData = cos(w_0);
sinData = -sin(w_0);

plotSize = 100;
outputCos = zeros(1,plotSize);
outputSin = zeros(1,plotSize);
for i=1:plotSize,
    cosDataPrev = cosData;
    sinDataPrev = sinData;
    
    cosData = cos(w_0) * cosDataPrev - sin(w_0) * sinDataPrev;
    sinData = sin(w_0) * cosDataPrev + cos(w_0) * sinDataPrev;
    
    outputCos(i) = cosData;
    outputSin(i) = sinData;
end

plot(outputCos)
hold on
plot(outputSin)

