#include "template1.h"

typedef struct
{
	float left; 	// red wire (ch. 1)
	float right;	// white wire (ch. 2)
} IO_audio;

#define BUFF_SIZE  	(256)

// Function Prototypes
void SetGpio(void);
void sendReceiveAudio(IO_audio *audio);
void swapArr(float **a, float **b);
void copyArr(float *a, float *b,int len);
void sendReceiveAudio(IO_audio *audio);
void findK(float *k,float h[],int16_t h_len);
void initBoard();

int main(void) {
	IO_audio audio;
	int16_t i;
	int16_t k_len = 32;

	float f;
    float g_next;
    float g_tmp;
	float *g = (float*)calloc(k_len,sizeof(float));
	float *k = (float*)calloc(k_len,sizeof(float));

	float h[] = { 0.00777994199607835,	-0.00716782026080922, 	 0.0584905067087291,	 0.0158904578279974,
				  -0.0402787755670438,	-0.0800015202919924,	-0.0692642564326274,	 0.000140785658104214,
				   0.0817127037203227,	 0.104124826767230,	 	 0.0359218251985976,	-0.0768983443593579,
				  -0.141830547483461,	-0.0979854246578476,	 0.0262004175815326, 	 0.131749649651920,
				   0.131749649651920,	 0.0262004175815326,	-0.0979854246578476,	-0.141830547483461,
				  -0.0768983443593579,	 0.0359218251985976,	 0.104124826767230,		 0.0817127037203227,
				   0.000140785658104214,-0.0692642564326274,	-0.0800015202919924,	-0.0402787755670438,
				   0.0158904578279974,	 0.0584905067087291,	-0.00716782026080922,	 0.00777994199607835};

	findK(k,h,k_len);

	initBoard();

	while (1){
		sendReceiveAudio(&audio);
		f = audio.left;
		g_next = f;
		for (i = 0; i < k_len; i++) {
			g_tmp = g_next;
			g_next = k[i]*f+g[i];
			f = f + g[i]*k[i];
			g[i] = g_tmp;
		}
		f = f-audio.left;
		audio.left = f;
		audio.right = f;
	}
}


// sends and receives audio
void sendReceiveAudio(IO_audio *audio){
	int16_t left = (int16_t)audio->left;
	int16_t right = (int16_t)audio->right;

	while (!CHKBIT(MCASP->SRCTL11, XRDY)) {}
    MCASP->XBUF11 = left;
    left = MCASP->XBUF12;	// Read the right channel input samples.

    while (!CHKBIT(MCASP->SRCTL11, XRDY)) {}
    MCASP->XBUF11 = right;
    right = MCASP->XBUF12;	// Read the right channel input samples.

    audio->left = (float)left;
    audio->right = (float)right;
}

void SetGpio(void) {
	EVMOMAPL138_pinmuxConfig(PINMUX_MCASP_REG_17,PINMUX_MCASP_MASK_17,PINMUX_MCASP_VAL_17);
	GPIO_setDir(GPIO_BANK7,GPIO_PIN7,GPIO_OUTPUT);
}

void swapArr(float **a, float **b){
    float *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void findK(float *k,float h[],int16_t h_len){
    int16_t stage;
    int16_t i;
    float *tmp;
    float *a_c= (float*)calloc(h_len,sizeof(float));
    float *a_n= (float*)calloc(h_len,sizeof(float));
    copyArr(a_c,h,h_len);

    stage = h_len-1;
    while (stage != 0) {
        k[stage-1] = a_c[stage];
        for (i = 0; i < stage; i++) {
            a_n[i] = (a_c[i] - k[stage-1]*a_c[stage-i])/(1-k[stage-1]*k[stage-1]);
        }
        tmp = a_n;
        a_n = a_c;
        a_c = tmp;
        stage--;
    }
}

void copyArr(float *a, float *b,int len){
    int16_t i;
	for (i = 0; i < len; i++) {
         a[i] = b[i];
     }
}

void initBoard(){
	EVMOMAPL138_init();				// Initialize the board
		EVMOMAPL138_initRAM();			// Set up the RAM
		EVMOMAPL138_enableDsp();			// Wake up the DSP

		// init the i2c for all to use.
		USTIMER_init();					// General use timers
		I2C_init(I2C0, I2C_CLK_400K);	// I2C initialization

		// set gpio output
		SetGpio();						// Configure the General Purpose I/O
		McASP_Init();					// Initialize McASP
		AIC3106_Init();					// Initialize AIC3106
		McASP_Start();					// Start McASP
}

