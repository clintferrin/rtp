#include <iostream>
#include <unistd.h>    
#include <time.h>

void print_arr(float *a, int len){
    for (int i = 0; i < len; i++) {
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;
}

void swap_arr(float **a, float **b){
    float *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void copy_arr(float *a, float *b,int len){
     for (int i = 0; i < len; i++) {
         a[i] = b[i];
     }
}

void calc_k(float *k,float h[],int16_t h_len){
    int16_t stage;
    float *a_c= (float*)calloc(h_len,sizeof(float));
    float *a_n= (float*)calloc(h_len,sizeof(float));
    copy_arr(a_c,h,h_len);
     
    stage = h_len-1;
    while (stage != 0) {
        k[stage-1] = a_c[stage];
        for (int i = 0; i < stage; i++) {
            a_n[i] = (a_c[i] - k[stage-1]*a_c[stage-i])/(1-k[stage-1]*k[stage-1]);
        }
        swap_arr(&a_n,&a_c);
        stage--;
    }

}

int main(int argc, const char *argv[]) {
    int16_t h_len = 3;
    float h[] = {1,1,-.5};
    float *k= (float*)calloc(h_len-1,sizeof(float));
    calc_k(k,h,h_len);
    print_arr(k,h_len-1);
    return 0;
}
