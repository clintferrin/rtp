#include "template1.h"

typedef struct
{
	float left; 	// red wire (ch. 1)
	float right;	// white wire (ch. 2)
} IO_audio;

#define BUFF_SIZE  	(256)

// Function Prototypes
void SetGpio(void);
void sendReceiveAudio(IO_audio *audio);

int main(void) {
	EVMOMAPL138_init();				// Initialize the board
	EVMOMAPL138_initRAM();			// Set up the RAM
	EVMOMAPL138_enableDsp();			// Wake up the DSP

	// init the i2c for all to use.
	USTIMER_init();					// General use timers
	I2C_init(I2C0, I2C_CLK_400K);	// I2C initialization

	// set gpio output
	SetGpio();						// Configure the General Purpose I/O
	McASP_Init();					// Initialize McASP
	AIC3106_Init();					// Initialize AIC3106
	McASP_Start();					// Start McASP

	// audio read/write structure
	IO_audio audio;

	// convolution variables
	int16_t buf_index = 0;
	int16_t buf_step;
	float x_buf[32] = {0};
	float sum;

	// taps and length for filter
	int16_t h_len = 32;
	float h[] = { 0.00777994199607835,	-0.00716782026080922, 	 0.0584905067087291,	 0.0158904578279974,
				  -0.0402787755670438,	-0.0800015202919924,	-0.0692642564326274,	 0.000140785658104214,
				   0.0817127037203227,	 0.104124826767230,	 	 0.0359218251985976,	-0.0768983443593579,
				  -0.141830547483461,	-0.0979854246578476,	 0.0262004175815326, 	 0.131749649651920,
				   0.131749649651920,	 0.0262004175815326,	-0.0979854246578476,	-0.141830547483461,
				  -0.0768983443593579,	 0.0359218251985976,	 0.104124826767230,		 0.0817127037203227,
				   0.000140785658104214,-0.0692642564326274,	-0.0800015202919924,	-0.0402787755670438,
				   0.0158904578279974,	 0.0584905067087291,	-0.00716782026080922,	 0.00777994199607835};

	while (1){
		sendReceiveAudio(&audio);

		buf_index = (buf_index+h_len-1) & 0x1F;
		x_buf[buf_index] = audio.left;

		// convolution loop
		sum = 0;
		for (buf_step=0; buf_step<h_len; buf_step++) {
			sum += h[buf_step] * x_buf[(buf_index+buf_step) & 0x1F];
		}
		audio.left = sum;
		audio.right = sum;
     }
}

// sends and receives audio
void sendReceiveAudio(IO_audio *audio){
	int16_t left = (int16_t)audio->left;
	int16_t right = (int16_t)audio->right;

	while (!CHKBIT(MCASP->SRCTL11, XRDY)) {}
    MCASP->XBUF11 = left;
    left = MCASP->XBUF12;	// Read the right channel input samples.

    while (!CHKBIT(MCASP->SRCTL11, XRDY)) {}
    MCASP->XBUF11 = right;
    right = MCASP->XBUF12;	// Read the right channel input samples.

    audio->left = (float)left;
    audio->right = (float)right;
}

void SetGpio(void) {
	EVMOMAPL138_pinmuxConfig(PINMUX_MCASP_REG_17,PINMUX_MCASP_MASK_17,PINMUX_MCASP_VAL_17);
	GPIO_setDir(GPIO_BANK7,GPIO_PIN7,GPIO_OUTPUT);
}
