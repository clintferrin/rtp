#include <iostream>
#include <unistd.h>    
#include <time.h>

void print_arr(float *a, int len){
    for (int i = 0; i < len; i++) {
        std::cout << a[i] << " ";
    }
    std::cout << std::endl;
}

void swap_arr(float **a, float **b){
    float *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

int main(int argc, const char *argv[]) {
    int16_t num_input = 13;
    float input[] = {1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1};
    float *tmp;
    float g_next;
    float g_tmp;
    float fn;

    int16_t num_k = 32;
    float k[] = {0.204188879039756,-0.0920743988837364,-0.315052987942725,-0.402515342307166,-0.148624074564412,0.478005825178504,1.22431765797786,-2.77928488505660,-1.40297531419977,1.70245011261198,0.394838492487271,-0.370886786418264,-0.491506258667436,-0.305101728991462,0.0229773096323893,0.243435960021679,0.236867755213559,0.0636574665374851,-0.124652802158330,-0.185747792295337,-0.104423847343343,0.0350669104295802,0.117274428949113,0.0937199483819225,0.00412566454687358,-0.0714243675982277,-0.0830251815982685,-0.0398002899838212,0.0149492813671656,0.0586095245426892,-0.00722878529855538,0.00777994199607835};

    float *g = (float*)calloc(num_k+1,sizeof(float));
    float f;


    for (int j = 0; j < num_input; j++) {
        f = input[j];
        g_next = input[j]; 
        for (int i = 0; i < num_k; i++) { 
            g_tmp = g_next;
            g_next = k[i]*f+g[i];
            f = f + g[i]*k[i];
            g[i] = g_tmp;
        }
        std::cout << f << std::endl;
    }

    return 0;
}
