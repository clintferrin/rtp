h = [0.00777994199607835,-0.00716782026080922,0.0584905067087291,0.0158904578279974,-0.0402787755670438,-0.0800015202919924,-0.0692642564326274,0.000140785658104214,0.0817127037203227,0.104124826767230,0.0359218251985976,-0.0768983443593579,-0.141830547483461,-0.0979854246578476,0.0262004175815326,0.131749649651920,0.131749649651920,0.0262004175815326,-0.0979854246578476,-0.141830547483461,-0.0768983443593579,0.0359218251985976,0.104124826767230,0.0817127037203227,0.000140785658104214,-0.0692642564326274,-0.0800015202919924,-0.0402787755670438,0.0158904578279974,0.0584905067087291,-0.00716782026080922,0.00777994199607835];
Fs = 48000;

% plot filter in time domain
% plot(h);
% title('Impulse Response of FIR Filter')
% xlabel('Bin Number')
% ylabel('Magnitude')
% xlim([289,589]);

% save_fig('../report/media/matplot/h-impulse-response.pdf',gcf)

% plot magnitude response of filter (frequency domain)
[H,W] = freqz(h,1);
subplot(2,1,1)
plot(W/(2*pi),20*log10(abs(H)));
title('Magnitude Response (dB)')
xlabel('Frequency')
ylabel('Magnitude (dB)')
ylim([-50 5]);
xlim([0 0.5]);

subplot(2,1,2)
% plot frequency response of filter
[H,W] = freqz(h,1);
plot(W/(2*pi),unwrap(angle(H)));
xlim([.095,.195]);
title('Phase Response');
xlabel('Frequency');
ylabel('Phase (radians)');

%% Filter using lattice structure
