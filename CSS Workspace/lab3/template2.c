#include "L138_aic3106_init.h"
// direct form II variables
int16_t direct_filt_len = 3;
float SO_Mag[4][6] =
	 {{0.194541995843674,0,-0.194541995843674,1,-0.845057100107812,0.925474523703578},
	  {0.194541995843674,0,-0.194541995843674,1,-1.46584306988468, 0.944936436225675},
	  {0.109202904106442,0,-0.109202904106442,1,-1.26569787427489, 0.855163353611160},
	  {0.103094189343695,0,-0.103094189343695,1,-1.01064799164726, 0.836282737599747}};

float y;
float xn;
float g0[3] = {0};
float g1[3] = {0};
float g2[3] = {0};
float g3[3] = {0};

// lattice ladder variables
typedef struct lat_ladder_coef {
    float *k;
    float *v;
}lat_ladder_coef;

lat_ladder_coef coefs;

int16_t i;
int16_t lat_filt_len = 9;
float num[] = {0.000426083886163017,0,-0.00170433554465207,0,
               0.00255650331697810,0,-0.00170433554465207,0,0.000426083886163017};

float den[] = {1,-4.58724603591464,11.3401612081794,-18.0201447864243,20.1748218860508,
               -16.0235039483826,8.96514202126333,-3.22272941467147,0.625416417528689 };

float g[9] = {0};

// This data structure allows for outputting to both channels.
AIC31_data_type codec_data;

void copy_arr(float *a, float *b,int len) {
    int16_t i;
	for (i = 0; i < len; i++) {
        a[i] = b[i];
    }
}

void swap_arr(float **a, float **b) {
    float *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void calc_coefs(lat_ladder_coef *coefs,float num[],float den[], int16_t len) {
    int16_t stage;
    int16_t i;
    float *a_c= (float*)calloc(len,sizeof(float));
    float *a_n= (float*)calloc(len,sizeof(float));
    float *c_c  = (float*)calloc(len,sizeof(float));
    float *c_n  = (float*)calloc(len,sizeof(float));

    copy_arr(a_c,den,len);
    copy_arr(c_c,num,len);

    stage = len-1;
    while (stage != 0) {
        coefs->k[stage-1] = a_c[stage];
        coefs->v[stage] = c_c[stage];
        for (i = 0; i < stage; i++) {
            c_n[i] = c_c[i]-coefs->v[stage]*a_c[stage-i];
            a_n[i] = (a_c[i] - coefs->k[stage-1]*a_c[stage-i])
/(1-coefs->k[stage-1]*coefs->k[stage-1]);
        }
        swap_arr(&a_n,&a_c);
        swap_arr(&c_n,&c_c);
        stage--;
    }
    coefs->v[stage] = c_c[stage];
}


float direct_2(float x, float num[], float den[], float g[], int16_t len) {
    int16_t i;
    float y = 0;
    for (i = 0; i < len-1; i++) {
        x += g[i]*(-den[i+1]);
        y += g[i]*(num[i+1]);
    }

    for (i = 0; i < len-1; i++) {
        g[len-1-i] = g[len-2-i];
    }
    g[0]=x;
    return y+x*num[0];
}



float lat_iir(float x, float *k, float *v, float *g, int16_t len) {
    int16_t i;
    float y = 0;

    for (i = 0; i < len; i++) {
        x = x-g[i+1]*k[len-1-i];
        g[i] = x*k[len-1-i]+g[i+1];
        y += g[i]*v[len-i];
    }
    g[len]=x;
    y += x*v[0];
    return y;
}

float ASM_direct_2(float x, float *num, float *den, float *g, int16_t len);
float ASM_lat_iir(float x, float *num, float *den, float *g, int16_t len);
float ASM_test(float x, float *num, float *den, float *g);

interrupt void interrupt4(void)
{
	// Get the next sample of the input.
	y = (float)(input_left_sample()); // input from ADC
	xn = (float)(input_right_sample()); // input from ADC

	y = ASM_direct_2(y, SO_Mag[0],SO_Mag[0]+3,g0,direct_filt_len);
	y = ASM_direct_2(y, SO_Mag[1],SO_Mag[1]+3,g1,direct_filt_len);
	y = ASM_direct_2(y, SO_Mag[2],SO_Mag[2]+3,g2,direct_filt_len);
	y = ASM_direct_2(y, SO_Mag[3],SO_Mag[3]+3,g3,direct_filt_len);

//	y = direct_2(y, SO_Mag[0],SO_Mag[0]+3,g0,direct_filt_len);
//	y = direct_2(y, SO_Mag[1],SO_Mag[1]+3,g1,direct_filt_len);
//  y = direct_2(y, SO_Mag[2],SO_Mag[2]+3,g2,direct_filt_len);
//  y = direct_2(y, SO_Mag[3],SO_Mag[3]+3,g3,direct_filt_len);

//	y =  ASM_lat_iir(y,coefs.k,coefs.v,g,lat_filt_len-1);
//	y = lat_iir(y,coefs.k,coefs.v,g,lat_filt_len-1);

	// output to BOTH right and left channels...
	codec_data.channel[LEFT] = (uint16_t)(y);
	codec_data.channel[RIGHT] = (uint16_t)(y);
	output_sample(codec_data.uint);  // output to L and R DAC

	return;
}


int main(void)
{
	coefs.k = (float*)calloc(lat_filt_len-1,sizeof(float));
	coefs.v = (float*)calloc(lat_filt_len,sizeof(float));
	float *g = (float*)calloc(lat_filt_len,sizeof(float));
	calc_coefs(&coefs,num,den,lat_filt_len);

	L138_initialise_intr(FS_48000_HZ,ADC_GAIN_0DB,DAC_ATTEN_0DB);

	while(true);
}
