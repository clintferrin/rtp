;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Wed Feb 21 16:53:59 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=on --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:dwarf 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Disabled                                             *
;*   Optimizing for    : Compile time, Ease of Development                    *
;*                       Based on options: no -o, no -ms                      *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../template2.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\CSS Workspace\lab3\Debug")
;*****************************************************************************
;* CINIT RECORDS                                                             *
;*****************************************************************************
	.sect	".cinit"
	.align	8
	.field  	2,32
	.field  	_direct_filt_len+0,32
	.bits	3,16			; _direct_filt_len @ 0

	.sect	".cinit"
	.align	8
	.field  	$C$IR_1,32
	.field  	_SO_Mag+0,32
	.word	03e473604h		; _SO_Mag[0][0] @ 0
	.word	000000000h		; _SO_Mag[0][1] @ 32
	.word	0be473604h		; _SO_Mag[0][2] @ 64
	.word	03f800000h		; _SO_Mag[0][3] @ 96
	.word	0bf5855aah		; _SO_Mag[0][4] @ 128
	.word	03f6cebe6h		; _SO_Mag[0][5] @ 160
	.word	03e473604h		; _SO_Mag[1][0] @ 192
	.word	000000000h		; _SO_Mag[1][1] @ 224
	.word	0be473604h		; _SO_Mag[1][2] @ 256
	.word	03f800000h		; _SO_Mag[1][3] @ 288
	.word	0bfbba0bfh		; _SO_Mag[1][4] @ 320
	.word	03f71e75bh		; _SO_Mag[1][5] @ 352
	.word	03ddfa5c6h		; _SO_Mag[2][0] @ 384
	.word	000000000h		; _SO_Mag[2][1] @ 416
	.word	0bddfa5c6h		; _SO_Mag[2][2] @ 448
	.word	03f800000h		; _SO_Mag[2][3] @ 480
	.word	0bfa20263h		; _SO_Mag[2][4] @ 512
	.word	03f5aebfch		; _SO_Mag[2][5] @ 544
	.word	03dd3230ch		; _SO_Mag[3][0] @ 576
	.word	000000000h		; _SO_Mag[3][1] @ 608
	.word	0bdd3230ch		; _SO_Mag[3][2] @ 640
	.word	03f800000h		; _SO_Mag[3][3] @ 672
	.word	0bf815ceah		; _SO_Mag[3][4] @ 704
	.word	03f5616a0h		; _SO_Mag[3][5] @ 736
$C$IR_1:	.set	96

	.sect	".cinit"
	.align	8
	.field  	$C$IR_2,32
	.field  	_g0+0,32
	.word	000000000h		; _g0[0] @ 0
$C$IR_2:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_3,32
	.field  	_g1+0,32
	.word	000000000h		; _g1[0] @ 0
$C$IR_3:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_4,32
	.field  	_g2+0,32
	.word	000000000h		; _g2[0] @ 0
$C$IR_4:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_5,32
	.field  	_g3+0,32
	.word	000000000h		; _g3[0] @ 0
$C$IR_5:	.set	4

	.sect	".cinit"
	.align	8
	.field  	2,32
	.field  	_lat_filt_len+0,32
	.bits	9,16			; _lat_filt_len @ 0

	.sect	".cinit"
	.align	8
	.field  	$C$IR_6,32
	.field  	_num+0,32
	.word	039df6403h		; _num[0] @ 0
	.word	000000000h		; _num[1] @ 32
	.word	0badf6403h		; _num[2] @ 64
	.word	000000000h		; _num[3] @ 96
	.word	03b278b02h		; _num[4] @ 128
	.word	000000000h		; _num[5] @ 160
	.word	0badf6403h		; _num[6] @ 192
	.word	000000000h		; _num[7] @ 224
	.word	039df6403h		; _num[8] @ 256
$C$IR_6:	.set	36

	.sect	".cinit"
	.align	8
	.field  	$C$IR_7,32
	.field  	_den+0,32
	.word	03f800000h		; _den[0] @ 0
	.word	0c092cab8h		; _den[1] @ 32
	.word	04135714dh		; _den[2] @ 64
	.word	0c1902942h		; _den[3] @ 96
	.word	041a16609h		; _den[4] @ 128
	.word	0c1803023h		; _den[5] @ 160
	.word	0410f7139h		; _den[6] @ 192
	.word	0c04e4133h		; _den[7] @ 224
	.word	03f201b4ah		; _den[8] @ 256
$C$IR_7:	.set	36

	.sect	".cinit"
	.align	8
	.field  	$C$IR_8,32
	.field  	_g+0,32
	.word	000000000h		; _g[0] @ 0
$C$IR_8:	.set	4


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("calloc")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_calloc")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$25)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$25)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("L138_initialise_intr")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_L138_initialise_intr")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$28)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$29)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$29)
	.dwendtag $C$DW$4


$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("input_right_sample")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_input_right_sample")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("input_left_sample")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_input_left_sample")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("output_sample")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_output_sample")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$28)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ASM_direct_2")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ASM_direct_2")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$16)
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$22)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$22)
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$22)
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$29)
	.dwendtag $C$DW$12

	.global	_direct_filt_len
	.bss	_direct_filt_len,2,2
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("direct_filt_len")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_direct_filt_len")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_addr _direct_filt_len]
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$18, DW_AT_external
	.global	_SO_Mag
_SO_Mag:	.usect	".far",96,8
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("SO_Mag")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_SO_Mag")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_addr _SO_Mag]
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$19, DW_AT_external
	.global	_y
	.bss	_y,4,4
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("y")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_addr _y]
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$20, DW_AT_external
	.global	_xn
	.bss	_xn,4,4
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("xn")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_xn")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_addr _xn]
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$21, DW_AT_external
	.global	_g0
_g0:	.usect	".far",12,8
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("g0")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_g0")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr _g0]
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$22, DW_AT_external
	.global	_g1
_g1:	.usect	".far",12,8
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("g1")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_g1")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_addr _g1]
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$23, DW_AT_external
	.global	_g2
_g2:	.usect	".far",12,8
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("g2")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_g2")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_addr _g2]
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$24, DW_AT_external
	.global	_g3
_g3:	.usect	".far",12,8
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("g3")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_g3")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_addr _g3]
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$25, DW_AT_external
	.global	_coefs
_coefs:	.usect	".far",8,4
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("coefs")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_coefs")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _coefs]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$26, DW_AT_external
	.global	_i
	.bss	_i,2,2
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_addr _i]
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$27, DW_AT_external
	.global	_lat_filt_len
	.bss	_lat_filt_len,2,2
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("lat_filt_len")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_lat_filt_len")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_addr _lat_filt_len]
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$28, DW_AT_external
	.global	_num
_num:	.usect	".far",36,8
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("num")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_addr _num]
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$29, DW_AT_external
	.global	_den
_den:	.usect	".far",36,8
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("den")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_addr _den]
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$30, DW_AT_external
	.global	_g
_g:	.usect	".far",36,8
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("g")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_addr _g]
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$31, DW_AT_external
	.global	_codec_data
_codec_data:	.usect	".far",4,4
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("codec_data")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_codec_data")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_addr _codec_data]
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$32, DW_AT_external
;	C:\ccsv5\tools\compiler\c6000_7.4.4\bin\acp6x.exe -@C:\\Users\\A02179~1\\AppData\\Local\\Temp\\1131214 
	.sect	".text"
	.clink
	.global	_copy_arr

$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("copy_arr")
	.dwattr $C$DW$33, DW_AT_low_pc(_copy_arr)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_copy_arr")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x2f)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(0x10)
	.dwpsn	file "../template2.c",line 47,column 43,is_stmt,address _copy_arr

	.dwfde $C$DW$CIE, _copy_arr
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("a")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_a")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg4]
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("b")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_b")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg20]
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg6]

;******************************************************************************
;* FUNCTION NAME: copy_arr                                                    *
;*                                                                            *
;*   Regs Modified     : B0,B4,B5,B6,B7,SP                                    *
;*   Regs Used         : A4,A6,B0,B3,B4,B5,B6,B7,SP                           *
;*   Local Frame Size  : 0 Args + 16 Auto + 0 Save = 16 byte                  *
;******************************************************************************
_copy_arr:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           SUB     .D2     SP,16,SP          ; |47| 
	.dwcfi	cfa_offset, 16
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("a")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_a")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg31 4]
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("b")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_b")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg31 8]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg31 12]
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg31 16]
           STW     .D2T1   A6,*+SP(12)       ; |47| 
           STW     .D2T2   B4,*+SP(8)        ; |47| 
           STW     .D2T1   A4,*+SP(4)        ; |47| 
	.dwpsn	file "../template2.c",line 49,column 7,is_stmt
           ZERO    .L2     B4                ; |49| 
           STH     .D2T2   B4,*+SP(16)       ; |49| 
	.dwpsn	file "../template2.c",line 49,column 14,is_stmt
           LDH     .D2T2   *+SP(16),B5       ; |49| 
           NOP             4
           CMPLT   .L2X    B5,A6,B0          ; |49| 
   [!B0]   BNOP    .S1     $C$L2,5           ; |49| 
           ; BRANCHCC OCCURS {$C$L2}         ; |49| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L1:    
$C$DW$L$_copy_arr$2$B:
	.dwpsn	file "../template2.c",line 50,column 9,is_stmt
           LDH     .D2T2   *+SP(16),B4       ; |50| 
           LDW     .D2T2   *+SP(8),B7        ; |50| 
           LDH     .D2T2   *+SP(16),B5       ; |50| 
           LDW     .D2T2   *+SP(4),B6        ; |50| 
           NOP             2
           LDW     .D2T2   *+B7[B4],B4       ; |50| 
           NOP             4
           STW     .D2T2   B4,*+B6[B5]       ; |50| 
	.dwpsn	file "../template2.c",line 49,column 23,is_stmt
           LDH     .D2T2   *+SP(16),B4       ; |49| 
           NOP             4
           ADD     .L2     1,B4,B4           ; |49| 
           STH     .D2T2   B4,*+SP(16)       ; |49| 
	.dwpsn	file "../template2.c",line 49,column 14,is_stmt
           LDW     .D2T2   *+SP(12),B4       ; |49| 
           LDH     .D2T2   *+SP(16),B5       ; |49| 
           NOP             4
           CMPLT   .L2     B5,B4,B0          ; |49| 
   [ B0]   BNOP    .S1     $C$L1,5           ; |49| 
           ; BRANCHCC OCCURS {$C$L1}         ; |49| 
$C$DW$L$_copy_arr$2$E:
;** --------------------------------------------------------------------------*
	.dwpsn	file "../template2.c",line 52,column 1,is_stmt
;** --------------------------------------------------------------------------*
$C$L2:    
           ADDK    .S2     16,SP             ; |52| 
	.dwcfi	cfa_offset, 0
	.dwcfi	cfa_offset, 0
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |52| 
           ; BRANCH OCCURS {B3}              ; |52| 

$C$DW$42	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$42, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L1:1:1519257239")
	.dwattr $C$DW$42, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$42, DW_AT_TI_begin_line(0x31)
	.dwattr $C$DW$42, DW_AT_TI_end_line(0x33)
$C$DW$43	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$43, DW_AT_low_pc($C$DW$L$_copy_arr$2$B)
	.dwattr $C$DW$43, DW_AT_high_pc($C$DW$L$_copy_arr$2$E)
	.dwendtag $C$DW$42

	.dwattr $C$DW$33, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x34)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text"
	.clink
	.global	_swap_arr

$C$DW$44	.dwtag  DW_TAG_subprogram, DW_AT_name("swap_arr")
	.dwattr $C$DW$44, DW_AT_low_pc(_swap_arr)
	.dwattr $C$DW$44, DW_AT_high_pc(0x00)
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_swap_arr")
	.dwattr $C$DW$44, DW_AT_external
	.dwattr $C$DW$44, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$44, DW_AT_TI_begin_line(0x36)
	.dwattr $C$DW$44, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$44, DW_AT_TI_max_frame_size(0x10)
	.dwpsn	file "../template2.c",line 54,column 37,is_stmt,address _swap_arr

	.dwfde $C$DW$CIE, _swap_arr
$C$DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_name("a")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_a")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg4]
$C$DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_name("b")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_b")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: swap_arr                                                    *
;*                                                                            *
;*   Regs Modified     : A3,B4,B5,SP                                          *
;*   Regs Used         : A3,A4,B3,B4,B5,SP                                    *
;*   Local Frame Size  : 0 Args + 12 Auto + 0 Save = 12 byte                  *
;******************************************************************************
_swap_arr:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           SUB     .D2     SP,16,SP          ; |54| 
	.dwcfi	cfa_offset, 16
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("a")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_a")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg31 4]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("b")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_b")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg31 8]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg31 12]
           STW     .D2T2   B4,*+SP(8)        ; |54| 
           STW     .D2T1   A4,*+SP(4)        ; |54| 
	.dwpsn	file "../template2.c",line 56,column 5,is_stmt
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |56| 
           NOP             4
           STW     .D2T1   A3,*+SP(12)       ; |56| 
	.dwpsn	file "../template2.c",line 57,column 5,is_stmt
           LDW     .D2T2   *B4,B4            ; |57| 
           MV      .L2X    A4,B5
           NOP             3
           STW     .D2T2   B4,*B5            ; |57| 
	.dwpsn	file "../template2.c",line 58,column 5,is_stmt
           LDW     .D2T2   *+SP(12),B4       ; |58| 
           LDW     .D2T1   *+SP(8),A3        ; |58| 
           NOP             4
           STW     .D1T2   B4,*A3            ; |58| 
	.dwpsn	file "../template2.c",line 59,column 1,is_stmt
           ADDK    .S2     16,SP             ; |59| 
	.dwcfi	cfa_offset, 0
	.dwcfi	cfa_offset, 0
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |59| 
           ; BRANCH OCCURS {B3}              ; |59| 
	.dwattr $C$DW$44, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$44, DW_AT_TI_end_line(0x3b)
	.dwattr $C$DW$44, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$44

	.sect	".text"
	.clink
	.global	_calc_coefs

$C$DW$51	.dwtag  DW_TAG_subprogram, DW_AT_name("calc_coefs")
	.dwattr $C$DW$51, DW_AT_low_pc(_calc_coefs)
	.dwattr $C$DW$51, DW_AT_high_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_calc_coefs")
	.dwattr $C$DW$51, DW_AT_external
	.dwattr $C$DW$51, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$51, DW_AT_TI_begin_line(0x3d)
	.dwattr $C$DW$51, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$51, DW_AT_TI_max_frame_size(0x28)
	.dwpsn	file "../template2.c",line 61,column 78,is_stmt,address _calc_coefs

	.dwfde $C$DW$CIE, _calc_coefs
$C$DW$52	.dwtag  DW_TAG_formal_parameter, DW_AT_name("coefs")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_coefs")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg4]
$C$DW$53	.dwtag  DW_TAG_formal_parameter, DW_AT_name("num")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg20]
$C$DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_name("den")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg6]
$C$DW$55	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg22]

;******************************************************************************
;* FUNCTION NAME: calc_coefs                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 36 Auto + 4 Save = 40 byte                  *
;******************************************************************************
_calc_coefs:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           STW     .D2T2   B3,*SP--(40)      ; |61| 
	.dwcfi	cfa_offset, 40
	.dwcfi	save_reg_to_mem, 19, 0
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("coefs")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_coefs")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg31 4]
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("num")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg31 8]
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("den")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg31 12]
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg31 16]
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("stage")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_stage")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg31 18]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg31 20]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("a_c")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_a_c")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg31 24]
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("a_n")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_a_n")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg31 28]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("c_c")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_c_c")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg31 32]
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("c_n")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_c_n")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg31 36]
           STW     .D2T1   A6,*+SP(12)       ; |61| 

           MV      .L1X    B6,A3             ; |61| 
||         STW     .D2T1   A4,*+SP(4)        ; |61| 

           MV      .L1X    B4,A5             ; |61| 
||         STH     .D2T1   A3,*+SP(16)       ; |61| 

           STW     .D2T1   A5,*+SP(8)        ; |61| 
	.dwpsn	file "../template2.c",line 64,column 12,is_stmt
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("_calloc")
	.dwattr $C$DW$66, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+SP(16),A4       ; |64| 
||         MVK     .L2     0x4,B4            ; |64| 

$C$RL0:    ; CALL OCCURS {_calloc} {0}       ; |64| 
           STW     .D2T1   A4,*+SP(24)       ; |64| 
	.dwpsn	file "../template2.c",line 65,column 12,is_stmt
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_calloc")
	.dwattr $C$DW$67, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+SP(16),A4       ; |65| 
||         MVK     .L2     0x4,B4            ; |65| 

$C$RL1:    ; CALL OCCURS {_calloc} {0}       ; |65| 
           STW     .D2T1   A4,*+SP(28)       ; |65| 
	.dwpsn	file "../template2.c",line 66,column 12,is_stmt
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("_calloc")
	.dwattr $C$DW$68, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+SP(16),A4       ; |66| 
||         MVK     .L2     0x4,B4            ; |66| 

$C$RL2:    ; CALL OCCURS {_calloc} {0}       ; |66| 
           STW     .D2T1   A4,*+SP(32)       ; |66| 
	.dwpsn	file "../template2.c",line 67,column 12,is_stmt
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_calloc")
	.dwattr $C$DW$69, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+SP(16),A4       ; |67| 
||         MVK     .L2     0x4,B4            ; |67| 

$C$RL3:    ; CALL OCCURS {_calloc} {0}       ; |67| 
           STW     .D2T1   A4,*+SP(36)       ; |67| 
	.dwpsn	file "../template2.c",line 69,column 5,is_stmt
           LDW     .D2T1   *+SP(24),A4       ; |69| 
           LDW     .D2T2   *+SP(12),B4       ; |69| 
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_name("_copy_arr")
	.dwattr $C$DW$70, DW_AT_TI_call

           CALLP   .S2     _copy_arr,B3
||         LDH     .D2T1   *+SP(16),A6       ; |69| 

$C$RL4:    ; CALL OCCURS {_copy_arr} {0}     ; |69| 
	.dwpsn	file "../template2.c",line 70,column 5,is_stmt
           LDW     .D2T2   *+SP(8),B4        ; |70| 
           LDW     .D2T1   *+SP(32),A4       ; |70| 
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("_copy_arr")
	.dwattr $C$DW$71, DW_AT_TI_call

           CALLP   .S2     _copy_arr,B3
||         LDH     .D2T1   *+SP(16),A6       ; |70| 

$C$RL5:    ; CALL OCCURS {_copy_arr} {0}     ; |70| 
	.dwpsn	file "../template2.c",line 72,column 5,is_stmt
           LDH     .D2T2   *+SP(16),B4       ; |72| 
           NOP             4
           SUB     .L2     B4,1,B4           ; |72| 
           STH     .D2T2   B4,*+SP(18)       ; |72| 
	.dwpsn	file "../template2.c",line 73,column 12,is_stmt
           LDH     .D2T2   *+SP(18),B0       ; |73| 
           NOP             4
   [!B0]   BNOP    .S1     $C$L6,5           ; |73| 
           ; BRANCHCC OCCURS {$C$L6}         ; |73| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L3
;** --------------------------------------------------------------------------*
$C$L3:    
$C$DW$L$_calc_coefs$2$B:
	.dwpsn	file "../template2.c",line 74,column 9,is_stmt
           LDW     .D2T1   *+SP(4),A5        ; |74| 
           LDH     .D2T1   *+SP(18),A3       ; |74| 
           LDW     .D2T1   *+SP(24),A4       ; |74| 
           LDH     .D2T2   *+SP(18),B4       ; |74| 
           NOP             2
           LDW     .D1T1   *A5,A5            ; |74| 
           LDW     .D1T1   *+A4[A3],A3       ; |74| 
           SUB     .L1X    B4,1,A4           ; |74| 
           NOP             3
           STW     .D1T1   A3,*+A5[A4]       ; |74| 
	.dwpsn	file "../template2.c",line 75,column 9,is_stmt
           LDW     .D2T2   *+SP(4),B5        ; |75| 
           LDW     .D2T1   *+SP(32),A4       ; |75| 
           LDH     .D2T1   *+SP(18),A3       ; |75| 
           LDH     .D2T2   *+SP(18),B4       ; |75| 
           NOP             3

           LDW     .D2T2   *+B5(4),B5        ; |75| 
||         LDW     .D1T1   *+A4[A3],A3       ; |75| 

           NOP             4
           STW     .D2T1   A3,*+B5[B4]       ; |75| 
	.dwpsn	file "../template2.c",line 76,column 14,is_stmt
           ZERO    .L2     B4                ; |76| 
           STH     .D2T2   B4,*+SP(20)       ; |76| 
	.dwpsn	file "../template2.c",line 76,column 21,is_stmt
           LDH     .D2T2   *+SP(18),B4       ; |76| 
           LDH     .D2T2   *+SP(20),B5       ; |76| 
           NOP             4
           CMPLT   .L2     B5,B4,B0          ; |76| 
   [!B0]   BNOP    .S1     $C$L5,5           ; |76| 
           ; BRANCHCC OCCURS {$C$L5}         ; |76| 
$C$DW$L$_calc_coefs$2$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*      Disqualified loop: Loop contains a call
;*      Disqualified loop: Loop contains non-pipelinable instructions
;*----------------------------------------------------------------------------*
$C$L4:    
$C$DW$L$_calc_coefs$3$B:
	.dwpsn	file "../template2.c",line 77,column 13,is_stmt
           LDW     .D2T1   *+SP(4),A4        ; |77| 
           LDH     .D2T1   *+SP(18),A5       ; |77| 
           LDH     .D2T1   *+SP(18),A3       ; |77| 
           LDH     .D2T2   *+SP(20),B4       ; |77| 
           LDW     .D2T1   *+SP(24),A8       ; |77| 
           LDW     .D1T1   *+A4(4),A4        ; |77| 
           LDH     .D2T1   *+SP(20),A6       ; |77| 
           LDW     .D2T1   *+SP(32),A7       ; |77| 
           SUB     .L1X    A5,B4,A31         ; |77| 
           LDW     .D2T2   *+SP(36),B5       ; |77| 
           LDW     .D1T1   *+A4[A3],A3       ; |77| 
           LDW     .D1T1   *+A8[A31],A4      ; |77| 
           LDW     .D1T1   *+A7[A6],A30      ; |77| 
           LDH     .D2T2   *+SP(20),B31      ; |77| 
           NOP             2
           MPYSP   .M1     A4,A3,A3          ; |77| 
           NOP             3
           SUBSP   .L1     A30,A3,A3         ; |77| 
           NOP             3
           STW     .D2T1   A3,*+B5[B31]      ; |77| 
	.dwpsn	file "../template2.c",line 78,column 13,is_stmt
           LDW     .D2T2   *+SP(4),B5        ; |78| 
           LDH     .D2T2   *+SP(18),B8       ; |78| 
           LDH     .D2T2   *+SP(18),B7       ; |78| 
           LDW     .D2T1   *+SP(24),A29      ; |78| 
           LDH     .D2T1   *+SP(18),A7       ; |78| 
           MV      .L1X    B5,A4             ; |78| 

           LDW     .D1T1   *A4,A6            ; |78| 
||         LDW     .D2T2   *B5,B6            ; |78| 

           MV      .L1X    B5,A3             ; |78| 
           LDH     .D2T2   *+SP(20),B5       ; |78| 
           LDH     .D2T2   *+SP(18),B4       ; |78| 

           LDW     .D1T1   *A3,A8            ; |78| 
||         SUB     .L1X    B7,1,A9           ; |78| 
||         SUB     .L2     B8,1,B7           ; |78| 

           LDW     .D1T1   *+A6[A9],A9       ; |78| 
||         LDW     .D2T2   *+B6[B7],B30      ; |78| 

           LDH     .D2T1   *+SP(20),A28      ; |78| 
           SUB     .L1X    A7,B5,A7          ; |78| 
           SUB     .L1X    B4,1,A27          ; |78| 
           LDW     .D1T1   *+A29[A7],A7      ; |78| 

           LDW     .D1T1   *+A8[A27],A6      ; |78| 
||         MPYSP   .M1X    A9,B30,A3         ; |78| 

           ZERO    .L1     A26
           MV      .L1     A29,A5            ; |78| 
           SET     .S1     A26,0x17,0x1d,A8

           LDW     .D1T1   *+A5[A28],A5      ; |78| 
||         SUBSP   .L1     A8,A3,A3          ; |78| 

           MPYSP   .M1     A7,A6,A4          ; |78| 
           NOP             3
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_name("__divf")
	.dwattr $C$DW$72, DW_AT_TI_call

           CALLP   .S2     __divf,B3
||         SUBSP   .L1     A5,A4,A4          ; |78| 
||         MV      .L2X    A3,B4             ; |78| 

$C$RL6:    ; CALL OCCURS {__divf} {0}        ; |78| 
           LDW     .D2T2   *+SP(28),B5       ; |78| 
           LDH     .D2T2   *+SP(20),B4       ; |78| 
           NOP             4
           STW     .D2T1   A4,*+B5[B4]       ; |78| 
	.dwpsn	file "../template2.c",line 76,column 32,is_stmt
           LDH     .D2T2   *+SP(20),B4       ; |76| 
           NOP             4
           ADD     .L2     1,B4,B4           ; |76| 
           STH     .D2T2   B4,*+SP(20)       ; |76| 
	.dwpsn	file "../template2.c",line 76,column 21,is_stmt
           LDH     .D2T2   *+SP(18),B4       ; |76| 
           LDH     .D2T2   *+SP(20),B5       ; |76| 
           NOP             4
           CMPLT   .L2     B5,B4,B0          ; |76| 
   [ B0]   BNOP    .S1     $C$L4,5           ; |76| 
           ; BRANCHCC OCCURS {$C$L4}         ; |76| 
$C$DW$L$_calc_coefs$3$E:
;** --------------------------------------------------------------------------*
$C$L5:    
$C$DW$L$_calc_coefs$4$B:
	.dwpsn	file "../template2.c",line 81,column 9,is_stmt
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("_swap_arr")
	.dwattr $C$DW$73, DW_AT_TI_call

           CALLP   .S2     _swap_arr,B3
||         ADDAW   .D1X    SP,7,A4           ; |81| 
||         ADDAW   .D2     SP,6,B4           ; |81| 

$C$RL7:    ; CALL OCCURS {_swap_arr} {0}     ; |81| 
	.dwpsn	file "../template2.c",line 82,column 9,is_stmt
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_swap_arr")
	.dwattr $C$DW$74, DW_AT_TI_call

           CALLP   .S2     _swap_arr,B3
||         ADDAW   .D1X    SP,9,A4           ; |82| 
||         ADDAW   .D2     SP,8,B4           ; |82| 

$C$RL8:    ; CALL OCCURS {_swap_arr} {0}     ; |82| 
	.dwpsn	file "../template2.c",line 83,column 9,is_stmt
           LDH     .D2T2   *+SP(18),B4       ; |83| 
           NOP             4
           SUB     .L2     B4,1,B4           ; |83| 
           STH     .D2T2   B4,*+SP(18)       ; |83| 
	.dwpsn	file "../template2.c",line 73,column 12,is_stmt
           LDH     .D2T2   *+SP(18),B0       ; |73| 
           NOP             4
   [ B0]   BNOP    .S1     $C$L3,5           ; |73| 
           ; BRANCHCC OCCURS {$C$L3}         ; |73| 
$C$DW$L$_calc_coefs$4$E:
;** --------------------------------------------------------------------------*
$C$L6:    
	.dwpsn	file "../template2.c",line 85,column 5,is_stmt
           LDW     .D2T2   *+SP(4),B5        ; |85| 
           LDW     .D2T1   *+SP(32),A3       ; |85| 
           LDH     .D2T1   *+SP(18),A4       ; |85| 
           LDH     .D2T2   *+SP(18),B4       ; |85| 
           NOP             3

           LDW     .D2T2   *+B5(4),B5        ; |85| 
||         LDW     .D1T1   *+A3[A4],A3       ; |85| 

           NOP             4
           STW     .D2T1   A3,*+B5[B4]       ; |85| 
	.dwpsn	file "../template2.c",line 86,column 1,is_stmt
           LDW     .D2T2   *++SP(40),B3      ; |86| 
           NOP             4
	.dwcfi	cfa_offset, 0
	.dwcfi	restore_reg, 19
	.dwcfi	cfa_offset, 0
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |86| 
           ; BRANCH OCCURS {B3}              ; |86| 

$C$DW$76	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$76, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L3:1:1519257239")
	.dwattr $C$DW$76, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$76, DW_AT_TI_begin_line(0x49)
	.dwattr $C$DW$76, DW_AT_TI_end_line(0x54)
$C$DW$77	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$77, DW_AT_low_pc($C$DW$L$_calc_coefs$2$B)
	.dwattr $C$DW$77, DW_AT_high_pc($C$DW$L$_calc_coefs$2$E)
$C$DW$78	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$78, DW_AT_low_pc($C$DW$L$_calc_coefs$4$B)
	.dwattr $C$DW$78, DW_AT_high_pc($C$DW$L$_calc_coefs$4$E)

$C$DW$79	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$79, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L4:2:1519257239")
	.dwattr $C$DW$79, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$79, DW_AT_TI_begin_line(0x4c)
	.dwattr $C$DW$79, DW_AT_TI_end_line(0x50)
$C$DW$80	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$80, DW_AT_low_pc($C$DW$L$_calc_coefs$3$B)
	.dwattr $C$DW$80, DW_AT_high_pc($C$DW$L$_calc_coefs$3$E)
	.dwendtag $C$DW$79

	.dwendtag $C$DW$76

	.dwattr $C$DW$51, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$51, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$51, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$51

	.sect	".text"
	.clink
	.global	_direct_2

$C$DW$81	.dwtag  DW_TAG_subprogram, DW_AT_name("direct_2")
	.dwattr $C$DW$81, DW_AT_low_pc(_direct_2)
	.dwattr $C$DW$81, DW_AT_high_pc(0x00)
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_direct_2")
	.dwattr $C$DW$81, DW_AT_external
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$81, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$81, DW_AT_TI_begin_line(0x59)
	.dwattr $C$DW$81, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$81, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "../template2.c",line 89,column 75,is_stmt,address _direct_2

	.dwfde $C$DW$CIE, _direct_2
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg4]
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("num")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg20]
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("den")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg6]
$C$DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("g")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg22]
$C$DW$86	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg8]

;******************************************************************************
;* FUNCTION NAME: direct_2                                                    *
;*                                                                            *
;*   Regs Modified     : A3,A4,A5,B0,B4,B5,B6,B7,B8,SP,B30,B31                *
;*   Regs Used         : A3,A4,A5,A6,A8,B0,B3,B4,B5,B6,B7,B8,SP,B30,B31       *
;*   Local Frame Size  : 0 Args + 24 Auto + 0 Save = 24 byte                  *
;******************************************************************************
_direct_2:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           SUB     .D2     SP,24,SP          ; |89| 
	.dwcfi	cfa_offset, 24
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("x")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg31 4]
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("num")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg31 8]
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("den")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg31 12]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("g")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg31 16]
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg31 20]
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg31 22]
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("y")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg31 24]
           STH     .D2T1   A8,*+SP(20)       ; |89| 
           STW     .D2T2   B6,*+SP(16)       ; |89| 
           STW     .D2T1   A6,*+SP(12)       ; |89| 

           MV      .L1X    B4,A3             ; |89| 
||         STW     .D2T1   A4,*+SP(4)        ; |89| 

           STW     .D2T1   A3,*+SP(8)        ; |89| 
	.dwpsn	file "../template2.c",line 91,column 11,is_stmt
           ZERO    .L2     B4                ; |91| 
           STW     .D2T2   B4,*+SP(24)       ; |91| 
	.dwpsn	file "../template2.c",line 92,column 10,is_stmt
           STH     .D2T2   B4,*+SP(22)       ; |92| 
	.dwpsn	file "../template2.c",line 92,column 17,is_stmt
           LDH     .D2T2   *+SP(20),B4       ; |92| 
           LDH     .D2T2   *+SP(22),B5       ; |92| 
           NOP             3
           SUB     .L2     B4,1,B4           ; |92| 
           CMPLT   .L2     B5,B4,B0          ; |92| 
   [!B0]   BNOP    .S1     $C$L8,5           ; |92| 
           ; BRANCHCC OCCURS {$C$L8}         ; |92| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L7:    
$C$DW$L$_direct_2$2$B:
	.dwpsn	file "../template2.c",line 93,column 9,is_stmt
           LDH     .D2T2   *+SP(22),B5       ; |93| 
           LDH     .D2T2   *+SP(22),B4       ; |93| 
           LDW     .D2T2   *+SP(16),B7       ; |93| 
           MV      .L2X    A6,B6             ; |93| 
           ZERO    .L1     A3                ; |93| 
           ADD     .L2     1,B5,B5           ; |93| 
           LDW     .D2T2   *+B6[B5],B5       ; |93| 
           LDW     .D2T2   *+B7[B4],B4       ; |93| 
           SET     .S1     A3,31,31,A3       ; |93| 
           LDW     .D2T2   *+SP(4),B31       ; |93| 
           NOP             1
           XOR     .L1X    B5,A3,A3          ; |93| 
           MPYSP   .M1X    A3,B4,A3          ; |93| 
           NOP             4
           ADDSP   .L2X    A3,B31,B4         ; |93| 
           NOP             3
           STW     .D2T2   B4,*+SP(4)        ; |93| 
	.dwpsn	file "../template2.c",line 94,column 9,is_stmt
           LDH     .D2T1   *+SP(22),A3       ; |94| 
           LDH     .D2T2   *+SP(22),B4       ; |94| 
           LDW     .D2T1   *+SP(8),A5        ; |94| 
           MV      .L1X    B7,A4             ; |94| 
           LDW     .D2T2   *+SP(24),B30      ; |94| 
           LDW     .D1T1   *+A4[A3],A3       ; |94| 
           ADD     .L1X    1,B4,A4           ; |94| 
           LDW     .D1T1   *+A5[A4],A4       ; |94| 
           NOP             4
           MPYSP   .M1     A4,A3,A3          ; |94| 
           NOP             4
           ADDSP   .L2X    A3,B30,B4         ; |94| 
           NOP             3
           STW     .D2T2   B4,*+SP(24)       ; |94| 
	.dwpsn	file "../template2.c",line 92,column 28,is_stmt
           LDH     .D2T2   *+SP(22),B4       ; |92| 
           NOP             4
           ADD     .L2     1,B4,B4           ; |92| 
           STH     .D2T2   B4,*+SP(22)       ; |92| 
	.dwpsn	file "../template2.c",line 92,column 17,is_stmt
           LDH     .D2T2   *+SP(20),B4       ; |92| 
           LDH     .D2T2   *+SP(22),B5       ; |92| 
           NOP             3
           SUB     .L2     B4,1,B4           ; |92| 
           CMPLT   .L2     B5,B4,B0          ; |92| 
   [ B0]   BNOP    .S1     $C$L7,5           ; |92| 
           ; BRANCHCC OCCURS {$C$L7}         ; |92| 
$C$DW$L$_direct_2$2$E:
;** --------------------------------------------------------------------------*
$C$L8:    
	.dwpsn	file "../template2.c",line 97,column 10,is_stmt
           ZERO    .L2     B4                ; |97| 
           STH     .D2T2   B4,*+SP(22)       ; |97| 
	.dwpsn	file "../template2.c",line 97,column 17,is_stmt
           LDH     .D2T2   *+SP(20),B4       ; |97| 
           LDH     .D2T2   *+SP(22),B5       ; |97| 
           NOP             3
           SUB     .L2     B4,1,B4           ; |97| 
           CMPLT   .L2     B5,B4,B0          ; |97| 
   [!B0]   BNOP    .S1     $C$L10,5          ; |97| 
           ; BRANCHCC OCCURS {$C$L10}        ; |97| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L9:    
$C$DW$L$_direct_2$4$B:
	.dwpsn	file "../template2.c",line 98,column 9,is_stmt
           LDH     .D2T2   *+SP(20),B6       ; |98| 
           LDH     .D2T2   *+SP(22),B4       ; |98| 
           LDW     .D2T2   *+SP(16),B7       ; |98| 
           LDH     .D2T2   *+SP(20),B5       ; |98| 
           LDH     .D2T2   *+SP(22),B31      ; |98| 
           NOP             1
           SUB     .L2     B6,B4,B4          ; |98| 
           SUB     .L2     B4,2,B4           ; |98| 

           MV      .L2     B7,B8             ; |98| 
||         LDW     .D2T2   *+B7[B4],B7       ; |98| 

           SUB     .L2     B5,B31,B4         ; |98| 
           SUB     .L2     B4,1,B4           ; |98| 
           NOP             2
           STW     .D2T2   B7,*+B8[B4]       ; |98| 
	.dwpsn	file "../template2.c",line 97,column 28,is_stmt
           LDH     .D2T2   *+SP(22),B4       ; |97| 
           NOP             4
           ADD     .L2     1,B4,B4           ; |97| 
           STH     .D2T2   B4,*+SP(22)       ; |97| 
	.dwpsn	file "../template2.c",line 97,column 17,is_stmt
           LDH     .D2T2   *+SP(20),B4       ; |97| 
           LDH     .D2T2   *+SP(22),B5       ; |97| 
           NOP             3
           SUB     .L2     B4,1,B4           ; |97| 
           CMPLT   .L2     B5,B4,B0          ; |97| 
   [ B0]   BNOP    .S1     $C$L9,5           ; |97| 
           ; BRANCHCC OCCURS {$C$L9}         ; |97| 
$C$DW$L$_direct_2$4$E:
;** --------------------------------------------------------------------------*
$C$L10:    
	.dwpsn	file "../template2.c",line 100,column 5,is_stmt
           LDW     .D2T2   *+SP(4),B4        ; |100| 
           LDW     .D2T1   *+SP(16),A3       ; |100| 
           NOP             4
           STW     .D1T2   B4,*A3            ; |100| 
	.dwpsn	file "../template2.c",line 101,column 5,is_stmt
           LDW     .D2T2   *+SP(8),B5        ; |101| 
           LDW     .D2T2   *+SP(4),B4        ; |101| 
           LDW     .D2T1   *+SP(24),A3       ; |101| 
           NOP             2
           LDW     .D2T2   *B5,B5            ; |101| 
           NOP             4
           MPYSP   .M2     B5,B4,B4          ; |101| 
           NOP             4
           ADDSP   .L1X    B4,A3,A4          ; |101| 
           NOP             3
	.dwpsn	file "../template2.c",line 102,column 1,is_stmt
           ADDK    .S2     24,SP             ; |102| 
	.dwcfi	cfa_offset, 0
	.dwcfi	cfa_offset, 0
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |102| 
           ; BRANCH OCCURS {B3}              ; |102| 

$C$DW$95	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$95, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L9:1:1519257239")
	.dwattr $C$DW$95, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$95, DW_AT_TI_begin_line(0x61)
	.dwattr $C$DW$95, DW_AT_TI_end_line(0x63)
$C$DW$96	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$96, DW_AT_low_pc($C$DW$L$_direct_2$4$B)
	.dwattr $C$DW$96, DW_AT_high_pc($C$DW$L$_direct_2$4$E)
	.dwendtag $C$DW$95


$C$DW$97	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$97, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L7:1:1519257239")
	.dwattr $C$DW$97, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$97, DW_AT_TI_begin_line(0x5c)
	.dwattr $C$DW$97, DW_AT_TI_end_line(0x5f)
$C$DW$98	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$98, DW_AT_low_pc($C$DW$L$_direct_2$2$B)
	.dwattr $C$DW$98, DW_AT_high_pc($C$DW$L$_direct_2$2$E)
	.dwendtag $C$DW$97

	.dwattr $C$DW$81, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$81, DW_AT_TI_end_line(0x66)
	.dwattr $C$DW$81, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$81

	.sect	".text"
	.clink
	.global	_lat_iir

$C$DW$99	.dwtag  DW_TAG_subprogram, DW_AT_name("lat_iir")
	.dwattr $C$DW$99, DW_AT_low_pc(_lat_iir)
	.dwattr $C$DW$99, DW_AT_high_pc(0x00)
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_lat_iir")
	.dwattr $C$DW$99, DW_AT_external
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$99, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$99, DW_AT_TI_begin_line(0x6a)
	.dwattr $C$DW$99, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$99, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "../template2.c",line 106,column 67,is_stmt,address _lat_iir

	.dwfde $C$DW$CIE, _lat_iir
$C$DW$100	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg4]
$C$DW$101	.dwtag  DW_TAG_formal_parameter, DW_AT_name("k")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_reg20]
$C$DW$102	.dwtag  DW_TAG_formal_parameter, DW_AT_name("v")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_v")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg6]
$C$DW$103	.dwtag  DW_TAG_formal_parameter, DW_AT_name("g")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_reg22]
$C$DW$104	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg8]

;******************************************************************************
;* FUNCTION NAME: lat_iir                                                     *
;*                                                                            *
;*   Regs Modified     : A3,A4,A5,A6,B0,B4,B5,B6,B7,B8,SP,B28,B29,B30,B31     *
;*   Regs Used         : A3,A4,A5,A6,A8,B0,B3,B4,B5,B6,B7,B8,SP,B28,B29,B30,  *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 24 Auto + 0 Save = 24 byte                  *
;******************************************************************************
_lat_iir:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           SUB     .D2     SP,24,SP          ; |106| 
	.dwcfi	cfa_offset, 24
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("x")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg31 4]
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("k")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_breg31 8]
$C$DW$107	.dwtag  DW_TAG_variable, DW_AT_name("v")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_v")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_breg31 12]
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("g")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_breg31 16]
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("len")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_breg31 20]
$C$DW$110	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg31 22]
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("y")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_breg31 24]
           STH     .D2T1   A8,*+SP(20)       ; |106| 
           STW     .D2T2   B6,*+SP(16)       ; |106| 
           STW     .D2T1   A6,*+SP(12)       ; |106| 

           MV      .L1X    B4,A3             ; |106| 
||         STW     .D2T1   A4,*+SP(4)        ; |106| 

           STW     .D2T1   A3,*+SP(8)        ; |106| 
	.dwpsn	file "../template2.c",line 108,column 11,is_stmt
           ZERO    .L2     B4                ; |108| 
           STW     .D2T2   B4,*+SP(24)       ; |108| 
	.dwpsn	file "../template2.c",line 110,column 10,is_stmt
           STH     .D2T2   B4,*+SP(22)       ; |110| 
	.dwpsn	file "../template2.c",line 110,column 17,is_stmt
           LDH     .D2T2   *+SP(22),B5       ; |110| 
           LDH     .D2T2   *+SP(20),B4       ; |110| 
           NOP             4
           CMPLT   .L2     B5,B4,B0          ; |110| 
   [!B0]   BNOP    .S1     $C$L12,5          ; |110| 
           ; BRANCHCC OCCURS {$C$L12}        ; |110| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L11:    
$C$DW$L$_lat_iir$2$B:
	.dwpsn	file "../template2.c",line 111,column 9,is_stmt
           LDH     .D2T2   *+SP(22),B4       ; |111| 
           LDH     .D2T2   *+SP(20),B5       ; |111| 
           LDH     .D2T2   *+SP(22),B6       ; |111| 
           LDW     .D2T1   *+SP(16),A4       ; |111| 
           LDW     .D2T1   *+SP(8),A5        ; |111| 
           LDW     .D2T2   *+SP(4),B31       ; |111| 
           SUB     .L2     B5,B4,B4          ; |111| 
           ADD     .L1X    1,B6,A3           ; |111| 

           LDW     .D1T1   *+A4[A3],A3       ; |111| 
||         SUB     .L1X    B4,1,A4           ; |111| 

           LDW     .D1T1   *+A5[A4],A4       ; |111| 
           NOP             4
           MPYSP   .M1     A4,A3,A3          ; |111| 
           NOP             4
           SUBSP   .L2X    B31,A3,B4         ; |111| 
           NOP             3
           STW     .D2T2   B4,*+SP(4)        ; |111| 
	.dwpsn	file "../template2.c",line 112,column 9,is_stmt
           LDH     .D2T2   *+SP(22),B4       ; |112| 
           LDH     .D2T2   *+SP(20),B5       ; |112| 
           LDW     .D2T2   *+SP(16),B7       ; |112| 
           LDH     .D2T2   *+SP(22),B6       ; |112| 
           MV      .L2X    A5,B8             ; |112| 
           LDW     .D2T2   *+SP(4),B30       ; |112| 
           SUB     .L2     B5,B4,B4          ; |112| 
           SUB     .L2     B4,1,B5           ; |112| 
           LDW     .D2T2   *+B8[B5],B5       ; |112| 
           ADD     .L2     1,B6,B6           ; |112| 
           LDW     .D2T2   *+B7[B6],B6       ; |112| 
           LDH     .D2T2   *+SP(22),B29      ; |112| 
           NOP             1
           MPYSP   .M2     B5,B30,B4         ; |112| 
           NOP             3
           ADDSP   .L2     B6,B4,B4          ; |112| 
           NOP             3
           STW     .D2T2   B4,*+B7[B29]      ; |112| 
	.dwpsn	file "../template2.c",line 113,column 9,is_stmt
           LDH     .D2T1   *+SP(22),A3       ; |113| 
           LDH     .D2T2   *+SP(22),B4       ; |113| 
           LDW     .D2T1   *+SP(16),A4       ; |113| 
           LDH     .D2T1   *+SP(20),A5       ; |113| 
           LDW     .D2T1   *+SP(12),A6       ; |113| 
           LDW     .D2T2   *+SP(24),B28      ; |113| 
           NOP             2

           LDW     .D1T1   *+A4[A3],A3       ; |113| 
||         SUB     .L1X    A5,B4,A4          ; |113| 

           LDW     .D1T1   *+A6[A4],A4       ; |113| 
           NOP             4
           MPYSP   .M1     A4,A3,A3          ; |113| 
           NOP             4
           ADDSP   .L2X    A3,B28,B4         ; |113| 
           NOP             3
           STW     .D2T2   B4,*+SP(24)       ; |113| 
	.dwpsn	file "../template2.c",line 110,column 26,is_stmt
           LDH     .D2T2   *+SP(22),B4       ; |110| 
           NOP             4
           ADD     .L2     1,B4,B4           ; |110| 
           STH     .D2T2   B4,*+SP(22)       ; |110| 
	.dwpsn	file "../template2.c",line 110,column 17,is_stmt
           LDH     .D2T2   *+SP(20),B5       ; |110| 
           LDH     .D2T2   *+SP(22),B4       ; |110| 
           NOP             4
           CMPLT   .L2     B4,B5,B0          ; |110| 
   [ B0]   BNOP    .S1     $C$L11,5          ; |110| 
           ; BRANCHCC OCCURS {$C$L11}        ; |110| 
$C$DW$L$_lat_iir$2$E:
;** --------------------------------------------------------------------------*
$C$L12:    
	.dwpsn	file "../template2.c",line 115,column 5,is_stmt
           LDH     .D2T1   *+SP(20),A3       ; |115| 
           LDW     .D2T1   *+SP(16),A4       ; |115| 
           LDW     .D2T2   *+SP(4),B4        ; |115| 
           NOP             4
           STW     .D1T2   B4,*+A4[A3]       ; |115| 
	.dwpsn	file "../template2.c",line 116,column 5,is_stmt
           LDW     .D2T2   *+SP(12),B5       ; |116| 
           LDW     .D2T2   *+SP(4),B4        ; |116| 
           LDW     .D2T2   *+SP(24),B6       ; |116| 
           NOP             2
           LDW     .D2T2   *B5,B5            ; |116| 
           NOP             4
           MPYSP   .M2     B5,B4,B4          ; |116| 
           NOP             3
           ADDSP   .L2     B4,B6,B4          ; |116| 
           NOP             3
           STW     .D2T2   B4,*+SP(24)       ; |116| 
	.dwpsn	file "../template2.c",line 117,column 5,is_stmt
           MV      .L1X    B4,A4
	.dwpsn	file "../template2.c",line 118,column 1,is_stmt
           ADDK    .S2     24,SP             ; |118| 
	.dwcfi	cfa_offset, 0
	.dwcfi	cfa_offset, 0
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |118| 
           ; BRANCH OCCURS {B3}              ; |118| 

$C$DW$113	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$113, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L11:1:1519257239")
	.dwattr $C$DW$113, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$113, DW_AT_TI_begin_line(0x6e)
	.dwattr $C$DW$113, DW_AT_TI_end_line(0x72)
$C$DW$114	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$114, DW_AT_low_pc($C$DW$L$_lat_iir$2$B)
	.dwattr $C$DW$114, DW_AT_high_pc($C$DW$L$_lat_iir$2$E)
	.dwendtag $C$DW$113

	.dwattr $C$DW$99, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$99, DW_AT_TI_end_line(0x76)
	.dwattr $C$DW$99, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$99

	.sect	".text:retain"
	.retain
	.retainrefs
	.global	_interrupt4

$C$DW$115	.dwtag  DW_TAG_subprogram, DW_AT_name("interrupt4")
	.dwattr $C$DW$115, DW_AT_low_pc(_interrupt4)
	.dwattr $C$DW$115, DW_AT_high_pc(0x00)
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_interrupt4")
	.dwattr $C$DW$115, DW_AT_external
	.dwattr $C$DW$115, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$115, DW_AT_TI_begin_line(0x7c)
	.dwattr $C$DW$115, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$115, DW_AT_TI_interrupt
	.dwattr $C$DW$115, DW_AT_TI_max_frame_size(0xe0)
	.dwpsn	file "../template2.c",line 125,column 1,is_stmt,address _interrupt4

	.dwfde $C$DW$CIE, _interrupt4

;******************************************************************************
;* FUNCTION NAME: interrupt4                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 0 Auto + 220 Save = 220 byte                *
;******************************************************************************
_interrupt4:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           ADDK    .S2     -224,SP           ; |125| 
	.dwcfi	cfa_offset, 224
           STW     .D2T2   B31,*+SP(224)     ; |125| 
	.dwcfi	save_reg_to_mem, 68, 0
           STW     .D2T2   B30,*+SP(220)     ; |125| 
	.dwcfi	save_reg_to_mem, 67, -4
           STW     .D2T2   B29,*+SP(216)     ; |125| 
	.dwcfi	save_reg_to_mem, 66, -8
           STW     .D2T2   B28,*+SP(212)     ; |125| 
	.dwcfi	save_reg_to_mem, 65, -12
           STW     .D2T2   B27,*+SP(208)     ; |125| 
	.dwcfi	save_reg_to_mem, 64, -16
           STW     .D2T2   B26,*+SP(204)     ; |125| 
	.dwcfi	save_reg_to_mem, 63, -20
           STW     .D2T2   B25,*+SP(200)     ; |125| 
	.dwcfi	save_reg_to_mem, 62, -24
           STW     .D2T2   B24,*+SP(196)     ; |125| 
	.dwcfi	save_reg_to_mem, 61, -28
           STW     .D2T2   B23,*+SP(192)     ; |125| 
	.dwcfi	save_reg_to_mem, 60, -32
           STW     .D2T2   B22,*+SP(188)     ; |125| 
	.dwcfi	save_reg_to_mem, 59, -36
           STW     .D2T2   B21,*+SP(184)     ; |125| 
	.dwcfi	save_reg_to_mem, 58, -40
           STW     .D2T2   B20,*+SP(180)     ; |125| 
	.dwcfi	save_reg_to_mem, 57, -44
           STW     .D2T2   B19,*+SP(176)     ; |125| 
	.dwcfi	save_reg_to_mem, 56, -48
           STW     .D2T2   B18,*+SP(172)     ; |125| 
	.dwcfi	save_reg_to_mem, 55, -52
           STW     .D2T2   B17,*+SP(168)     ; |125| 
	.dwcfi	save_reg_to_mem, 54, -56
           STW     .D2T2   B16,*+SP(164)     ; |125| 
	.dwcfi	save_reg_to_mem, 53, -60
           STW     .D2T1   A31,*+SP(160)     ; |125| 
	.dwcfi	save_reg_to_mem, 52, -64
           STW     .D2T1   A30,*+SP(156)     ; |125| 
	.dwcfi	save_reg_to_mem, 51, -68
           STW     .D2T1   A29,*+SP(152)     ; |125| 
	.dwcfi	save_reg_to_mem, 50, -72
           STW     .D2T1   A28,*+SP(148)     ; |125| 
	.dwcfi	save_reg_to_mem, 49, -76
           STW     .D2T1   A27,*+SP(144)     ; |125| 
	.dwcfi	save_reg_to_mem, 48, -80
           STW     .D2T1   A26,*+SP(140)     ; |125| 
	.dwcfi	save_reg_to_mem, 47, -84
           STW     .D2T1   A25,*+SP(136)     ; |125| 
	.dwcfi	save_reg_to_mem, 46, -88
           STW     .D2T1   A24,*+SP(132)     ; |125| 
	.dwcfi	save_reg_to_mem, 45, -92
           STW     .D2T1   A23,*+SP(128)     ; |125| 
	.dwcfi	save_reg_to_mem, 44, -96
           STW     .D2T1   A22,*+SP(124)     ; |125| 
	.dwcfi	save_reg_to_mem, 43, -100
           STW     .D2T1   A21,*+SP(120)     ; |125| 
	.dwcfi	save_reg_to_mem, 42, -104
           STW     .D2T1   A20,*+SP(116)     ; |125| 
	.dwcfi	save_reg_to_mem, 41, -108
           STW     .D2T1   A19,*+SP(112)     ; |125| 
	.dwcfi	save_reg_to_mem, 40, -112
           STW     .D2T1   A18,*+SP(108)     ; |125| 
	.dwcfi	save_reg_to_mem, 39, -116
           STW     .D2T1   A17,*+SP(104)     ; |125| 
	.dwcfi	save_reg_to_mem, 38, -120
           STW     .D2T1   A16,*+SP(100)     ; |125| 
	.dwcfi	save_reg_to_mem, 37, -124
           STW     .D2T2   B9,*+SP(96)       ; |125| 
	.dwcfi	save_reg_to_mem, 25, -128
           STW     .D2T2   B8,*+SP(92)       ; |125| 
	.dwcfi	save_reg_to_mem, 24, -132
           STW     .D2T2   B7,*+SP(88)       ; |125| 
	.dwcfi	save_reg_to_mem, 23, -136
           STW     .D2T2   B6,*+SP(84)       ; |125| 
	.dwcfi	save_reg_to_mem, 22, -140
           STW     .D2T2   B5,*+SP(80)       ; |125| 
	.dwcfi	save_reg_to_mem, 21, -144
           STW     .D2T2   B4,*+SP(76)       ; |125| 
	.dwcfi	save_reg_to_mem, 20, -148
           STW     .D2T2   B3,*+SP(72)       ; |125| 
	.dwcfi	save_reg_to_mem, 19, -152
           STW     .D2T2   B2,*+SP(68)       ; |125| 
	.dwcfi	save_reg_to_mem, 18, -156
           STW     .D2T2   B1,*+SP(64)       ; |125| 
	.dwcfi	save_reg_to_mem, 17, -160
           STW     .D2T2   B0,*+SP(60)       ; |125| 
	.dwcfi	save_reg_to_mem, 16, -164
           STW     .D2T1   A9,*+SP(56)       ; |125| 
	.dwcfi	save_reg_to_mem, 9, -168
           STW     .D2T1   A8,*+SP(52)       ; |125| 
	.dwcfi	save_reg_to_mem, 8, -172
           STW     .D2T1   A7,*+SP(48)       ; |125| 
	.dwcfi	save_reg_to_mem, 7, -176
           STW     .D2T1   A6,*+SP(44)       ; |125| 
	.dwcfi	save_reg_to_mem, 6, -180
           STW     .D2T1   A5,*+SP(40)       ; |125| 
	.dwcfi	save_reg_to_mem, 5, -184
           STW     .D2T1   A4,*+SP(36)       ; |125| 
	.dwcfi	save_reg_to_mem, 4, -188
           STW     .D2T1   A3,*+SP(32)       ; |125| 
	.dwcfi	save_reg_to_mem, 3, -192
           STW     .D2T1   A2,*+SP(28)       ; |125| 
	.dwcfi	save_reg_to_mem, 2, -196
           STW     .D2T1   A1,*+SP(24)       ; |125| 
	.dwcfi	save_reg_to_mem, 1, -200
           STW     .D2T1   A0,*+SP(20)       ; |125| 
	.dwcfi	save_reg_to_mem, 0, -204
           MVC     .S2     ILC,B0            ; |125| 
           STW     .D2T2   B0,*+SP(16)       ; |125| 
	.dwcfi	save_reg_to_mem, 88, -208
           MVC     .S2     RILC,B0           ; |125| 
           STW     .D2T2   B0,*+SP(12)       ; |125| 
	.dwcfi	save_reg_to_mem, 89, -208
           MVC     .S2     ITSR,B0           ; |125| 
           STW     .D2T2   B0,*+SP(8)        ; |125| 
	.dwcfi	save_reg_to_mem, 95, -208
	.dwpsn	file "../template2.c",line 127,column 2,is_stmt
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_input_left_sample")
	.dwattr $C$DW$116, DW_AT_TI_call
           CALLP   .S2     _input_left_sample,B3
$C$RL9:    ; CALL OCCURS {_input_left_sample} {0}  ; |127| 
           INTSP   .L2X    A4,B4             ; |127| 
           NOP             3
           STW     .D2T2   B4,*+DP(_y)       ; |127| 
	.dwpsn	file "../template2.c",line 128,column 2,is_stmt
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("_input_right_sample")
	.dwattr $C$DW$117, DW_AT_TI_call
           CALLP   .S2     _input_right_sample,B3
$C$RL10:   ; CALL OCCURS {_input_right_sample} {0}  ; |128| 
           INTSP   .L1     A4,A3             ; |128| 
           NOP             3
           STW     .D2T1   A3,*+DP(_xn)      ; |128| 
	.dwpsn	file "../template2.c",line 135,column 2,is_stmt
           MVKL    .S1     _SO_Mag+12,A6
           MVKL    .S1     _g0,A3
           MVKH    .S1     _SO_Mag+12,A6

           MVKH    .S1     _g0,A3
||         MVK     .S2     12,B4

           LDW     .D2T1   *+DP(_y),A4       ; |135| 
||         SUB     .L2X    A6,B4,B4

$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$118, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |135| 
||         MV      .L2X    A3,B6             ; |135| 

$C$RL11:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |135| 
           STW     .D2T1   A4,*+DP(_y)       ; |135| 
	.dwpsn	file "../template2.c",line 136,column 2,is_stmt
           MVKL    .S2     _SO_Mag+36,B4
           MVKH    .S2     _SO_Mag+36,B4
           MVK     .S1     12,A3

           SUB     .L1X    B4,A3,A3
||         MVKL    .S2     _g1,B6

           MVKH    .S2     _g1,B6
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$119, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |136| 
||         MV      .L1X    B4,A6             ; |136| 
||         MV      .L2X    A3,B4             ; |136| 

$C$RL12:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |136| 
           STW     .D2T1   A4,*+DP(_y)       ; |136| 
	.dwpsn	file "../template2.c",line 137,column 2,is_stmt
           MVKL    .S1     _SO_Mag+60,A6
           MVKL    .S1     _g2,A3
           MVKH    .S1     _SO_Mag+60,A6

           MVKH    .S1     _g2,A3
||         MVK     .S2     12,B4

           SUB     .L2X    A6,B4,B4
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$120, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |137| 
||         MV      .L2X    A3,B6             ; |137| 

$C$RL13:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |137| 
           STW     .D2T1   A4,*+DP(_y)       ; |137| 
	.dwpsn	file "../template2.c",line 138,column 2,is_stmt
           MVKL    .S1     _SO_Mag+84,A6
           MVKH    .S1     _SO_Mag+84,A6

           SUBAW   .D1     A6,3,A3
||         MVKL    .S2     _g3,B6

           MVKH    .S2     _g3,B6
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$121, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |138| 
||         MV      .L2X    A3,B4             ; |138| 

$C$RL14:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |138| 
           STW     .D2T1   A4,*+DP(_y)       ; |138| 
	.dwpsn	file "../template2.c",line 149,column 2,is_stmt
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("__fixfu")
	.dwattr $C$DW$122, DW_AT_TI_call
           CALLP   .S2     __fixfu,B3
$C$RL15:   ; CALL OCCURS {__fixfu} {0}       ; |149| 
           MVKL    .S2     _codec_data,B4

           MVKH    .S2     _codec_data,B4
||         MV      .L2X    A4,B5             ; |149| 

           STH     .D2T2   B5,*B4            ; |149| 
	.dwpsn	file "../template2.c",line 150,column 2,is_stmt
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("__fixfu")
	.dwattr $C$DW$123, DW_AT_TI_call

           CALLP   .S2     __fixfu,B3
||         LDW     .D2T1   *+DP(_y),A4       ; |150| 

$C$RL16:   ; CALL OCCURS {__fixfu} {0}       ; |150| 
           MVKL    .S2     _codec_data+2,B4
           MVKH    .S2     _codec_data+2,B4
           STH     .D2T1   A4,*B4            ; |150| 
	.dwpsn	file "../template2.c",line 151,column 2,is_stmt
           ADD     .L1X    -2,B4,A3
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("_output_sample")
	.dwattr $C$DW$124, DW_AT_TI_call

           CALLP   .S2     _output_sample,B3
||         LDW     .D1T1   *A3,A4            ; |151| 

$C$RL17:   ; CALL OCCURS {_output_sample} {0}  ; |151| 
	.dwpsn	file "../template2.c",line 153,column 2,is_stmt
	.dwpsn	file "../template2.c",line 154,column 1,is_stmt
           LDW     .D2T2   *+SP(16),B0       ; |154| 
           NOP             4
           MVC     .S2     B0,ILC            ; |154| 
           NOP             3
	.dwcfi	restore_reg, 88
           LDW     .D2T2   *+SP(12),B0       ; |154| 
           NOP             4
           MVC     .S2     B0,RILC           ; |154| 
           NOP             3
	.dwcfi	restore_reg, 89
           LDW     .D2T2   *+SP(8),B0        ; |154| 
           NOP             4
           MVC     .S2     B0,ITSR           ; |154| 
	.dwcfi	restore_reg, 95
           LDW     .D2T2   *+SP(224),B31     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 68
           LDW     .D2T2   *+SP(220),B30     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 67
           LDW     .D2T2   *+SP(216),B29     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 66
           LDW     .D2T2   *+SP(212),B28     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 65
           LDW     .D2T2   *+SP(208),B27     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 64
           LDW     .D2T2   *+SP(204),B26     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 63
           LDW     .D2T2   *+SP(200),B25     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 62
           LDW     .D2T2   *+SP(196),B24     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 61
           LDW     .D2T2   *+SP(192),B23     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 60
           LDW     .D2T2   *+SP(188),B22     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 59
           LDW     .D2T2   *+SP(184),B21     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 58
           LDW     .D2T2   *+SP(180),B20     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 57
           LDW     .D2T2   *+SP(176),B19     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 56
           LDW     .D2T2   *+SP(172),B18     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 55
           LDW     .D2T2   *+SP(168),B17     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 54
           LDW     .D2T2   *+SP(164),B16     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 53
           LDW     .D2T1   *+SP(160),A31     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 52
           LDW     .D2T1   *+SP(156),A30     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 51
           LDW     .D2T1   *+SP(152),A29     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 50
           LDW     .D2T1   *+SP(148),A28     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 49
           LDW     .D2T1   *+SP(144),A27     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 48
           LDW     .D2T1   *+SP(140),A26     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 47
           LDW     .D2T1   *+SP(136),A25     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 46
           LDW     .D2T1   *+SP(132),A24     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 45
           LDW     .D2T1   *+SP(128),A23     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 44
           LDW     .D2T1   *+SP(124),A22     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 43
           LDW     .D2T1   *+SP(120),A21     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 42
           LDW     .D2T1   *+SP(116),A20     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 41
           LDW     .D2T1   *+SP(112),A19     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 40
           LDW     .D2T1   *+SP(108),A18     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 39
           LDW     .D2T1   *+SP(104),A17     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 38
           LDW     .D2T1   *+SP(100),A16     ; |154| 
           NOP             4
	.dwcfi	restore_reg, 37
           LDW     .D2T2   *+SP(96),B9       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 25
           LDW     .D2T2   *+SP(92),B8       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 24
           LDW     .D2T2   *+SP(88),B7       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 23
           LDW     .D2T2   *+SP(84),B6       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 22
           LDW     .D2T2   *+SP(80),B5       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 21
           LDW     .D2T2   *+SP(76),B4       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 20
           LDW     .D2T2   *+SP(72),B3       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 19
           LDW     .D2T2   *+SP(68),B2       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 18
           LDW     .D2T2   *+SP(64),B1       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 17
           LDW     .D2T2   *+SP(60),B0       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 16
           LDW     .D2T1   *+SP(56),A9       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 9
           LDW     .D2T1   *+SP(52),A8       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 8
           LDW     .D2T1   *+SP(48),A7       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 7
           LDW     .D2T1   *+SP(44),A6       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 6
           LDW     .D2T1   *+SP(40),A5       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 5
           LDW     .D2T1   *+SP(36),A4       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 4
           LDW     .D2T1   *+SP(32),A3       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 3
           LDW     .D2T1   *+SP(28),A2       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 2
           LDW     .D2T1   *+SP(24),A1       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 1
           LDW     .D2T1   *+SP(20),A0       ; |154| 
           NOP             4
	.dwcfi	restore_reg, 0
           ADDK    .S2     224,SP            ; |154| 
	.dwcfi	cfa_offset, 0
	.dwcfi	cfa_offset, 0
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_TI_return
           RET     .S2     IRP               ; |154| 
           NOP             5
           ; BRANCH OCCURS {IRP}             ; |154| 
	.dwattr $C$DW$115, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$115, DW_AT_TI_end_line(0x9a)
	.dwattr $C$DW$115, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$115

	.sect	".text"
	.clink
	.global	_main

$C$DW$126	.dwtag  DW_TAG_subprogram, DW_AT_name("main")
	.dwattr $C$DW$126, DW_AT_low_pc(_main)
	.dwattr $C$DW$126, DW_AT_high_pc(0x00)
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_main")
	.dwattr $C$DW$126, DW_AT_external
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$126, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$126, DW_AT_TI_begin_line(0x9d)
	.dwattr $C$DW$126, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$126, DW_AT_TI_max_frame_size(0x08)
	.dwpsn	file "../template2.c",line 158,column 1,is_stmt,address _main

	.dwfde $C$DW$CIE, _main

;******************************************************************************
;* FUNCTION NAME: main                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 4 Auto + 4 Save = 8 byte                    *
;******************************************************************************
_main:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           STW     .D2T2   B3,*SP--(8)       ; |158| 
	.dwcfi	cfa_offset, 8
	.dwcfi	save_reg_to_mem, 19, 0
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("g")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg31 4]
	.dwpsn	file "../template2.c",line 159,column 2,is_stmt
           LDH     .D2T2   *+DP(_lat_filt_len),B4 ; |159| 
           NOP             4
$C$DW$128	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$128, DW_AT_low_pc(0x00)
	.dwattr $C$DW$128, DW_AT_name("_calloc")
	.dwattr $C$DW$128, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         SUB     .L1X    B4,1,A4           ; |159| 
||         MVK     .L2     0x4,B4            ; |159| 

$C$RL18:   ; CALL OCCURS {_calloc} {0}       ; |159| 
           MVKL    .S1     _coefs,A3
           MVKH    .S1     _coefs,A3
           STW     .D1T1   A4,*A3            ; |159| 
	.dwpsn	file "../template2.c",line 160,column 2,is_stmt
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_name("_calloc")
	.dwattr $C$DW$129, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+DP(_lat_filt_len),A4 ; |160| 
||         MVK     .L2     0x4,B4            ; |160| 

$C$RL19:   ; CALL OCCURS {_calloc} {0}       ; |160| 
           MVKL    .S2     _coefs+4,B4
           MVKH    .S2     _coefs+4,B4
           STW     .D2T1   A4,*B4            ; |160| 
	.dwpsn	file "../template2.c",line 161,column 9,is_stmt
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_name("_calloc")
	.dwattr $C$DW$130, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+DP(_lat_filt_len),A4 ; |161| 
||         MVK     .L2     0x4,B4            ; |161| 

$C$RL20:   ; CALL OCCURS {_calloc} {0}       ; |161| 
           STW     .D2T1   A4,*+SP(4)        ; |161| 
	.dwpsn	file "../template2.c",line 162,column 2,is_stmt
           MVKL    .S1     _coefs,A4

           MVKL    .S2     _num,B4
||         MVKH    .S1     _coefs,A4

           MVKL    .S1     _den,A6
||         MVKH    .S2     _num,B4

$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_name("_calc_coefs")
	.dwattr $C$DW$131, DW_AT_TI_call

           CALLP   .S2     _calc_coefs,B3
||         LDH     .D2T2   *+DP(_lat_filt_len),B6 ; |162| 
||         MVKH    .S1     _den,A6

$C$RL21:   ; CALL OCCURS {_calc_coefs} {0}   ; |162| 
	.dwpsn	file "../template2.c",line 164,column 2,is_stmt
           MVKL    .S1     0xbb80,A4
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_name("_L138_initialise_intr")
	.dwattr $C$DW$132, DW_AT_TI_call

           CALLP   .S2     _L138_initialise_intr,B3
||         MVKH    .S1     0xbb80,A4
||         ZERO    .L2     B4                ; |164| 
||         ZERO    .L1     A6                ; |164| 

$C$RL22:   ; CALL OCCURS {_L138_initialise_intr} {0}  ; |164| 
	.dwpsn	file "../template2.c",line 166,column 8,is_stmt
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*      Disqualified loop: Bad loop structure
;*----------------------------------------------------------------------------*
$C$L13:    
$C$DW$L$_main$2$B:
           BNOP    .S1     $C$L13,5          ; |166| 
           ; BRANCH OCCURS {$C$L13}          ; |166| 
$C$DW$L$_main$2$E:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	restore_reg, 19
	.dwcfi	cfa_offset, 0

$C$DW$133	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$133, DW_AT_name("D:\CSS Workspace\lab3\Debug\template2.asm:$C$L13:1:1519257239")
	.dwattr $C$DW$133, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$133, DW_AT_TI_begin_line(0xa6)
	.dwattr $C$DW$133, DW_AT_TI_end_line(0xa6)
$C$DW$134	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$134, DW_AT_low_pc($C$DW$L$_main$2$B)
	.dwattr $C$DW$134, DW_AT_high_pc($C$DW$L$_main$2$E)
	.dwendtag $C$DW$133

	.dwattr $C$DW$126, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$126, DW_AT_TI_end_line(0xa7)
	.dwattr $C$DW$126, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$126

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_calloc
	.global	_L138_initialise_intr
	.global	_input_right_sample
	.global	_input_left_sample
	.global	_output_sample
	.global	_ASM_direct_2
	.global	__divf
	.global	__fixfu

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$T$21	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x04)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$135, DW_AT_name("uint")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_uint")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$136, DW_AT_name("channel")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("AIC31_data_type")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x20)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

$C$DW$T$20	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$137	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$137, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$20

$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$22	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$22, DW_AT_address_class(0x20)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x20)

$C$DW$T$52	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x18)
$C$DW$138	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$138, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x60)
$C$DW$139	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$139, DW_AT_upper_bound(0x03)
$C$DW$140	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$140, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$53


$C$DW$T$55	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x0c)
$C$DW$141	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$141, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x24)
$C$DW$142	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$142, DW_AT_upper_bound(0x08)
	.dwendtag $C$DW$T$56

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_name("lat_ladder_coef")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$143, DW_AT_name("k")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$144, DW_AT_name("v")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_v")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("lat_ladder_coef")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$40	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_address_class(0x20)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 228
	.dwcfi	cfa_register, 31
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 4
	.dwcfi	undefined, 5
	.dwcfi	undefined, 6
	.dwcfi	undefined, 7
	.dwcfi	undefined, 8
	.dwcfi	undefined, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	same_value, 26
	.dwcfi	same_value, 27
	.dwcfi	same_value, 28
	.dwcfi	same_value, 29
	.dwcfi	same_value, 30
	.dwcfi	same_value, 31
	.dwcfi	same_value, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 41
	.dwcfi	undefined, 42
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 45
	.dwcfi	undefined, 46
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 49
	.dwcfi	undefined, 50
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 53
	.dwcfi	undefined, 54
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	undefined, 57
	.dwcfi	undefined, 58
	.dwcfi	undefined, 59
	.dwcfi	undefined, 60
	.dwcfi	undefined, 61
	.dwcfi	undefined, 62
	.dwcfi	undefined, 63
	.dwcfi	undefined, 64
	.dwcfi	undefined, 65
	.dwcfi	undefined, 66
	.dwcfi	undefined, 67
	.dwcfi	undefined, 68
	.dwcfi	undefined, 69
	.dwcfi	undefined, 70
	.dwcfi	undefined, 71
	.dwcfi	undefined, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 78
	.dwcfi	undefined, 79
	.dwcfi	undefined, 80
	.dwcfi	undefined, 81
	.dwcfi	undefined, 82
	.dwcfi	undefined, 83
	.dwcfi	undefined, 84
	.dwcfi	undefined, 85
	.dwcfi	undefined, 86
	.dwcfi	undefined, 87
	.dwcfi	undefined, 88
	.dwcfi	undefined, 89
	.dwcfi	undefined, 90
	.dwcfi	undefined, 91
	.dwcfi	undefined, 92
	.dwcfi	undefined, 93
	.dwcfi	undefined, 94
	.dwcfi	undefined, 95
	.dwcfi	undefined, 96
	.dwcfi	undefined, 97
	.dwcfi	undefined, 98
	.dwcfi	undefined, 99
	.dwcfi	undefined, 100
	.dwcfi	undefined, 101
	.dwcfi	undefined, 102
	.dwcfi	undefined, 103
	.dwcfi	undefined, 104
	.dwcfi	undefined, 105
	.dwcfi	undefined, 106
	.dwcfi	undefined, 107
	.dwcfi	undefined, 108
	.dwcfi	undefined, 109
	.dwcfi	undefined, 110
	.dwcfi	undefined, 111
	.dwcfi	undefined, 112
	.dwcfi	undefined, 113
	.dwcfi	undefined, 114
	.dwcfi	undefined, 115
	.dwcfi	undefined, 116
	.dwcfi	undefined, 117
	.dwcfi	undefined, 118
	.dwcfi	undefined, 119
	.dwcfi	undefined, 120
	.dwcfi	undefined, 121
	.dwcfi	undefined, 122
	.dwcfi	undefined, 123
	.dwcfi	undefined, 124
	.dwcfi	undefined, 125
	.dwcfi	undefined, 126
	.dwcfi	undefined, 127
	.dwcfi	undefined, 128
	.dwcfi	undefined, 129
	.dwcfi	undefined, 130
	.dwcfi	undefined, 131
	.dwcfi	undefined, 132
	.dwcfi	undefined, 133
	.dwcfi	undefined, 134
	.dwcfi	undefined, 135
	.dwcfi	undefined, 136
	.dwcfi	undefined, 137
	.dwcfi	undefined, 138
	.dwcfi	undefined, 139
	.dwcfi	undefined, 140
	.dwcfi	undefined, 141
	.dwcfi	undefined, 142
	.dwcfi	undefined, 143
	.dwcfi	undefined, 144
	.dwcfi	undefined, 145
	.dwcfi	undefined, 146
	.dwcfi	undefined, 147
	.dwcfi	undefined, 148
	.dwcfi	undefined, 149
	.dwcfi	undefined, 150
	.dwcfi	undefined, 151
	.dwcfi	undefined, 152
	.dwcfi	undefined, 153
	.dwcfi	undefined, 154
	.dwcfi	undefined, 155
	.dwcfi	undefined, 156
	.dwcfi	undefined, 157
	.dwcfi	undefined, 158
	.dwcfi	undefined, 159
	.dwcfi	undefined, 160
	.dwcfi	undefined, 161
	.dwcfi	undefined, 162
	.dwcfi	undefined, 163
	.dwcfi	undefined, 164
	.dwcfi	undefined, 165
	.dwcfi	undefined, 166
	.dwcfi	undefined, 167
	.dwcfi	undefined, 168
	.dwcfi	undefined, 169
	.dwcfi	undefined, 170
	.dwcfi	undefined, 171
	.dwcfi	undefined, 172
	.dwcfi	undefined, 173
	.dwcfi	undefined, 174
	.dwcfi	undefined, 175
	.dwcfi	undefined, 176
	.dwcfi	undefined, 177
	.dwcfi	undefined, 178
	.dwcfi	undefined, 179
	.dwcfi	undefined, 180
	.dwcfi	undefined, 181
	.dwcfi	undefined, 182
	.dwcfi	undefined, 183
	.dwcfi	undefined, 184
	.dwcfi	undefined, 185
	.dwcfi	undefined, 186
	.dwcfi	undefined, 187
	.dwcfi	undefined, 188
	.dwcfi	undefined, 189
	.dwcfi	undefined, 190
	.dwcfi	undefined, 191
	.dwcfi	undefined, 192
	.dwcfi	undefined, 193
	.dwcfi	undefined, 194
	.dwcfi	undefined, 195
	.dwcfi	undefined, 196
	.dwcfi	undefined, 197
	.dwcfi	undefined, 198
	.dwcfi	undefined, 199
	.dwcfi	undefined, 200
	.dwcfi	undefined, 201
	.dwcfi	undefined, 202
	.dwcfi	undefined, 203
	.dwcfi	undefined, 204
	.dwcfi	undefined, 205
	.dwcfi	undefined, 206
	.dwcfi	undefined, 207
	.dwcfi	undefined, 208
	.dwcfi	undefined, 209
	.dwcfi	undefined, 210
	.dwcfi	undefined, 211
	.dwcfi	undefined, 212
	.dwcfi	undefined, 213
	.dwcfi	undefined, 214
	.dwcfi	undefined, 215
	.dwcfi	undefined, 216
	.dwcfi	undefined, 217
	.dwcfi	undefined, 218
	.dwcfi	undefined, 219
	.dwcfi	undefined, 220
	.dwcfi	undefined, 221
	.dwcfi	undefined, 222
	.dwcfi	undefined, 223
	.dwcfi	undefined, 224
	.dwcfi	undefined, 225
	.dwcfi	undefined, 226
	.dwcfi	undefined, 227
	.dwcfi	undefined, 228
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg0]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg1]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg2]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg3]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg4]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg5]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg6]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg7]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg8]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg9]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg10]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_reg11]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_reg12]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg13]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_reg14]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_reg15]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_reg16]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg17]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_reg18]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg19]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg20]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_reg21]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_reg22]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_reg23]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg24]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg25]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg26]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg27]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg28]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_reg29]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_reg30]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg31]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x20]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x21]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x22]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x23]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x24]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x25]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x26]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x27]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x28]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x29]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_regx 0x30]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x31]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_regx 0x32]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_regx 0x33]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_regx 0x34]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_regx 0x35]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_regx 0x36]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_regx 0x37]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x38]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_regx 0x39]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_regx 0x40]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_regx 0x41]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_regx 0x42]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_regx 0x43]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_regx 0x44]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x45]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_regx 0x46]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_regx 0x47]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_regx 0x48]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_regx 0x49]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_regx 0x50]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_regx 0x51]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_regx 0x52]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_regx 0x53]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_regx 0x54]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_regx 0x55]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_regx 0x56]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_regx 0x57]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_regx 0x58]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_regx 0x59]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x60]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x61]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x62]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x63]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x64]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x65]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x66]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x67]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x68]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x69]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$253	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$253, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$254	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$254, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$255	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$255, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$256	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$256, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$257	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_regx 0x70]
$C$DW$258	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$258, DW_AT_location[DW_OP_regx 0x71]
$C$DW$259	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$259, DW_AT_location[DW_OP_regx 0x72]
$C$DW$260	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$260, DW_AT_location[DW_OP_regx 0x73]
$C$DW$261	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$261, DW_AT_location[DW_OP_regx 0x74]
$C$DW$262	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$262, DW_AT_location[DW_OP_regx 0x75]
$C$DW$263	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$263, DW_AT_location[DW_OP_regx 0x76]
$C$DW$264	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$264, DW_AT_location[DW_OP_regx 0x77]
$C$DW$265	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$265, DW_AT_location[DW_OP_regx 0x78]
$C$DW$266	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$266, DW_AT_location[DW_OP_regx 0x79]
$C$DW$267	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$268	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$269	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$270	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$271	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$272	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$273	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

