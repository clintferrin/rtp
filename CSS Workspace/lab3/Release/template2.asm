;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Wed Feb 21 16:52:35 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=on --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Enabled with threshold = 0                           *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../template2.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\CSS Workspace\lab3\Release")
;*****************************************************************************
;* CINIT RECORDS                                                             *
;*****************************************************************************
	.sect	".cinit"
	.align	8
	.field  	2,32
	.field  	_direct_filt_len+0,32
	.bits	3,16			; _direct_filt_len @ 0

	.sect	".cinit"
	.align	8
	.field  	$C$IR_1,32
	.field  	_SO_Mag+0,32
	.word	03e473604h		; _SO_Mag[0][0] @ 0
	.word	000000000h		; _SO_Mag[0][1] @ 32
	.word	0be473604h		; _SO_Mag[0][2] @ 64
	.word	03f800000h		; _SO_Mag[0][3] @ 96
	.word	0bf5855aah		; _SO_Mag[0][4] @ 128
	.word	03f6cebe6h		; _SO_Mag[0][5] @ 160
	.word	03e473604h		; _SO_Mag[1][0] @ 192
	.word	000000000h		; _SO_Mag[1][1] @ 224
	.word	0be473604h		; _SO_Mag[1][2] @ 256
	.word	03f800000h		; _SO_Mag[1][3] @ 288
	.word	0bfbba0bfh		; _SO_Mag[1][4] @ 320
	.word	03f71e75bh		; _SO_Mag[1][5] @ 352
	.word	03ddfa5c6h		; _SO_Mag[2][0] @ 384
	.word	000000000h		; _SO_Mag[2][1] @ 416
	.word	0bddfa5c6h		; _SO_Mag[2][2] @ 448
	.word	03f800000h		; _SO_Mag[2][3] @ 480
	.word	0bfa20263h		; _SO_Mag[2][4] @ 512
	.word	03f5aebfch		; _SO_Mag[2][5] @ 544
	.word	03dd3230ch		; _SO_Mag[3][0] @ 576
	.word	000000000h		; _SO_Mag[3][1] @ 608
	.word	0bdd3230ch		; _SO_Mag[3][2] @ 640
	.word	03f800000h		; _SO_Mag[3][3] @ 672
	.word	0bf815ceah		; _SO_Mag[3][4] @ 704
	.word	03f5616a0h		; _SO_Mag[3][5] @ 736
$C$IR_1:	.set	96

	.sect	".cinit"
	.align	8
	.field  	$C$IR_2,32
	.field  	_g0+0,32
	.word	000000000h		; _g0[0] @ 0
$C$IR_2:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_3,32
	.field  	_g1+0,32
	.word	000000000h		; _g1[0] @ 0
$C$IR_3:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_4,32
	.field  	_g2+0,32
	.word	000000000h		; _g2[0] @ 0
$C$IR_4:	.set	4

	.sect	".cinit"
	.align	8
	.field  	$C$IR_5,32
	.field  	_g3+0,32
	.word	000000000h		; _g3[0] @ 0
$C$IR_5:	.set	4

	.sect	".cinit"
	.align	8
	.field  	2,32
	.field  	_lat_filt_len+0,32
	.bits	9,16			; _lat_filt_len @ 0

	.sect	".cinit"
	.align	8
	.field  	$C$IR_6,32
	.field  	_num+0,32
	.word	039df6403h		; _num[0] @ 0
	.word	000000000h		; _num[1] @ 32
	.word	0badf6403h		; _num[2] @ 64
	.word	000000000h		; _num[3] @ 96
	.word	03b278b02h		; _num[4] @ 128
	.word	000000000h		; _num[5] @ 160
	.word	0badf6403h		; _num[6] @ 192
	.word	000000000h		; _num[7] @ 224
	.word	039df6403h		; _num[8] @ 256
$C$IR_6:	.set	36

	.sect	".cinit"
	.align	8
	.field  	$C$IR_7,32
	.field  	_den+0,32
	.word	03f800000h		; _den[0] @ 0
	.word	0c092cab8h		; _den[1] @ 32
	.word	04135714dh		; _den[2] @ 64
	.word	0c1902942h		; _den[3] @ 96
	.word	041a16609h		; _den[4] @ 128
	.word	0c1803023h		; _den[5] @ 160
	.word	0410f7139h		; _den[6] @ 192
	.word	0c04e4133h		; _den[7] @ 224
	.word	03f201b4ah		; _den[8] @ 256
$C$IR_7:	.set	36

	.sect	".cinit"
	.align	8
	.field  	$C$IR_8,32
	.field  	_g+0,32
	.word	000000000h		; _g[0] @ 0
$C$IR_8:	.set	4


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("calloc")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_calloc")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$26)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$26)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("L138_initialise_intr")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_L138_initialise_intr")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$29)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$30)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$30)
	.dwendtag $C$DW$4


$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("input_right_sample")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_input_right_sample")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("input_left_sample")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_input_left_sample")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("output_sample")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_output_sample")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$29)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ASM_direct_2")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ASM_direct_2")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$16)
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$22)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$22)
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$22)
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$30)
	.dwendtag $C$DW$12

	.global	_direct_filt_len
	.bss	_direct_filt_len,2,2
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("direct_filt_len")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_direct_filt_len")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_addr _direct_filt_len]
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$18, DW_AT_external
	.global	_SO_Mag
_SO_Mag:	.usect	".far",96,8
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("SO_Mag")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_SO_Mag")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_addr _SO_Mag]
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$19, DW_AT_external
	.global	_y
	.bss	_y,4,4
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("y")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_y")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_addr _y]
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$20, DW_AT_external
	.global	_xn
	.bss	_xn,4,4
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("xn")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_xn")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_addr _xn]
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$21, DW_AT_external
	.global	_g0
_g0:	.usect	".far",12,8
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("g0")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_g0")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr _g0]
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$22, DW_AT_external
	.global	_g1
_g1:	.usect	".far",12,8
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("g1")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_g1")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_addr _g1]
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$23, DW_AT_external
	.global	_g2
_g2:	.usect	".far",12,8
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("g2")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_g2")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_addr _g2]
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$24, DW_AT_external
	.global	_g3
_g3:	.usect	".far",12,8
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("g3")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_g3")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_addr _g3]
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$25, DW_AT_external
	.global	_coefs
_coefs:	.usect	".far",8,4
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("coefs")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_coefs")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _coefs]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$26, DW_AT_external
	.global	_i
	.bss	_i,2,2
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_addr _i]
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$27, DW_AT_external
	.global	_lat_filt_len
	.bss	_lat_filt_len,2,2
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("lat_filt_len")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_lat_filt_len")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_addr _lat_filt_len]
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$28, DW_AT_external
	.global	_num
_num:	.usect	".far",36,8
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("num")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_addr _num]
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$29, DW_AT_external
	.global	_den
_den:	.usect	".far",36,8
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("den")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_addr _den]
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$30, DW_AT_external
	.global	_g
_g:	.usect	".far",36,8
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("g")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_addr _g]
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$31, DW_AT_external
	.global	_codec_data
_codec_data:	.usect	".far",4,4
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("codec_data")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_codec_data")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_addr _codec_data]
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$32, DW_AT_external
;	C:\ccsv5\tools\compiler\c6000_7.4.4\bin\opt6x.exe C:\\Users\\A02179~1\\AppData\\Local\\Temp\\097002 C:\\Users\\A02179~1\\AppData\\Local\\Temp\\097004 
	.sect	".text"
	.clink
	.global	_swap_arr

$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("swap_arr")
	.dwattr $C$DW$33, DW_AT_low_pc(_swap_arr)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_swap_arr")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x36)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$33, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$33, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 54,column 37,is_stmt,address _swap_arr
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("a")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_a")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg4]
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("b")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_b")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: swap_arr                                                    *
;*                                                                            *
;*   Regs Modified     : A3,B5                                                *
;*   Regs Used         : A3,A4,B3,B4,B5                                       *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_swap_arr:
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *B4,B5            ; |57| 
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return

           RETNOP  .S2     B3,3              ; |59| 
||         LDW     .D1T1   *A4,A3            ; |56| 

           STW     .D1T2   B5,*A4            ; |57| 
	.dwpsn	file "../template2.c",line 59,column 1,is_stmt
           STW     .D2T1   A3,*B4            ; |58| 
           ; BRANCH OCCURS {B3}              ; |59| 
	.dwattr $C$DW$33, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x3b)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$33

	.sect	".text"
	.clink
	.global	_copy_arr

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("copy_arr")
	.dwattr $C$DW$37, DW_AT_low_pc(_copy_arr)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_copy_arr")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0x2f)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$37, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$37, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 47,column 43,is_stmt,address _copy_arr
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("a")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_a")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg4]
$C$DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("b")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_b")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg20]
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg6]

;******************************************************************************
;* FUNCTION NAME: copy_arr                                                    *
;*                                                                            *
;*   Regs Modified     : A0,A3,A4,A5,B0,B4,B5,B6,B7                           *
;*   Regs Used         : A0,A3,A4,A5,A6,B0,B3,B4,B5,B6,B7                     *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_copy_arr:
;** --------------------------------------------------------------------------*

           CMPGT   .L1     A6,0,A0           ; |49| 
||         SHL     .S1     A6,2,A5           ; |50| 
||         SUB     .L2X    B4,A4,B5          ; |50| 

   [!A0]   B       .S1     $C$L7             ; |49| 
||         SUB     .L1X    A4,B4,A3          ; |50| 
|| [ A0]   MV      .L2X    A6,B7
|| [ A0]   MV      .S2     B4,B6

           CMPGT   .L1     A5,A3,A3          ; |50| 
           CMPGT   .L2X    A5,B5,B5          ; |50| 
           AND     .L2X    B5,A3,B0          ; |50| 

   [!A0]   MVK     .L2     0x1,B0            ; nullify predicate
|| [ A0]   MV      .S2X    A4,B5

   [!B0]   B       .S1     $C$L4             ; |50| 
           ; BRANCHCC OCCURS {$C$L7}         ; |49| 
;** --------------------------------------------------------------------------*
   [!B0]   MVC     .S2X    A6,ILC
   [ B0]   SUB     .L2     B7,1,B7
           NOP             3
           ; BRANCHCC OCCURS {$C$L4}         ; |50| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../template2.c
;*      Loop source line                 : 49
;*      Loop opening brace source line   : 49
;*      Loop closing brace source line   : 51
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 32768                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 6
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     0        2*    
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             0        2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        0     
;*      Bound(.L .S .D .LS .LSD)     0        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 6  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L1:    ; PIPED LOOP PROLOG
	.dwpsn	file "../template2.c",line 49,column 0,is_stmt

           SPLOOPD 6       ;12               ; (P) 
||         MVC     .S2     B7,ILC

;** --------------------------------------------------------------------------*
$C$L2:    ; PIPED LOOP KERNEL
$C$DW$L$_copy_arr$4$B:
           LDW     .D2T2   *B6++,B4          ; |50| (P) <0,0>  ^ 
           NOP             4
           STW     .D2T2   B4,*B5++          ; |50| (P) <0,5>  ^ 
	.dwpsn	file "../template2.c",line 51,column 0,is_stmt
           SPKERNEL 0,0
$C$DW$L$_copy_arr$4$E:
;** --------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP EPILOG
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |52| 
           ; BRANCH OCCURS {B3}              ; |52| 
;** --------------------------------------------------------------------------*
           MVC     .S2X    A6,ILC
           NOP             3
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../template2.c
;*      Loop source line                 : 49
;*      Loop opening brace source line   : 49
;*      Loop closing brace source line   : 51
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 32768                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1*       1*    
;*      .M units                     0        0     
;*      .X cross paths               0        1*    
;*      .T address paths             1*       1*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        0     
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 7 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L4:    ; PIPED LOOP PROLOG
	.dwpsn	file "../template2.c",line 49,column 0,is_stmt

           SPLOOP  1       ;7                ; (P) 
||         MV      .L1X    B4,A4

;** --------------------------------------------------------------------------*
$C$L5:    ; PIPED LOOP KERNEL
$C$DW$L$_copy_arr$8$B:
           LDW     .D1T1   *A4++,A3          ; |50| (P) <0,0> 
           NOP             4
           MV      .L2X    A3,B4             ; |50| (P) <0,5> Define a twin register
	.dwpsn	file "../template2.c",line 51,column 0,is_stmt

           SPKERNEL 6,0
||         STW     .D2T2   B4,*B5++          ; |50| <0,6> 

$C$DW$L$_copy_arr$8$E:
;** --------------------------------------------------------------------------*
$C$L6:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L7:    
	.dwpsn	file "../template2.c",line 52,column 1,is_stmt
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |52| 
           ; BRANCH OCCURS {B3}              ; |52| 

$C$DW$43	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$43, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L5:1:1519257155")
	.dwattr $C$DW$43, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$43, DW_AT_TI_begin_line(0x31)
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x33)
$C$DW$44	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$44, DW_AT_low_pc($C$DW$L$_copy_arr$8$B)
	.dwattr $C$DW$44, DW_AT_high_pc($C$DW$L$_copy_arr$8$E)
	.dwendtag $C$DW$43


$C$DW$45	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$45, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L2:1:1519257155")
	.dwattr $C$DW$45, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$45, DW_AT_TI_begin_line(0x31)
	.dwattr $C$DW$45, DW_AT_TI_end_line(0x33)
$C$DW$46	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$46, DW_AT_low_pc($C$DW$L$_copy_arr$4$B)
	.dwattr $C$DW$46, DW_AT_high_pc($C$DW$L$_copy_arr$4$E)
	.dwendtag $C$DW$45

	.dwattr $C$DW$37, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0x34)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$37

	.sect	".text"
	.clink
	.global	_calc_coefs

$C$DW$47	.dwtag  DW_TAG_subprogram, DW_AT_name("calc_coefs")
	.dwattr $C$DW$47, DW_AT_low_pc(_calc_coefs)
	.dwattr $C$DW$47, DW_AT_high_pc(0x00)
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_calc_coefs")
	.dwattr $C$DW$47, DW_AT_external
	.dwattr $C$DW$47, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$47, DW_AT_TI_begin_line(0x3d)
	.dwattr $C$DW$47, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$47, DW_AT_TI_max_frame_size(0x50)
	.dwattr $C$DW$47, DW_AT_frame_base[DW_OP_breg31 80]
	.dwattr $C$DW$47, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 61,column 78,is_stmt,address _calc_coefs
$C$DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_name("coefs")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_coefs")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg4]
$C$DW$49	.dwtag  DW_TAG_formal_parameter, DW_AT_name("num")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg20]
$C$DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_name("den")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg6]
$C$DW$51	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg22]

;******************************************************************************
;* FUNCTION NAME: calc_coefs                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,  *
;*                           A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20,B21, *
;*                           B22,B23,B24,B25,B26,B27,B28,B29,B30,B31          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24,   *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Local Frame Size  : 0 Args + 20 Auto + 56 Save = 76 byte                 *
;******************************************************************************
_calc_coefs:
;** --------------------------------------------------------------------------*
           STW     .D2T1   A11,*SP--(8)      ; |61| 
           STW     .D2T1   A10,*SP--(8)      ; |61| 
           STDW    .D2T2   B13:B12,*SP--     ; |61| 
           STDW    .D2T2   B11:B10,*SP--     ; |61| 
           STDW    .D2T1   A15:A14,*SP--     ; |61| 

           STDW    .D2T1   A13:A12,*SP--     ; |61| 
||         MV      .L2     B6,B10            ; |61| 
||         MV      .L1     A4,A10            ; |61| 
||         MV      .S1X    B6,A4             ; |61| 

$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x04)
	.dwattr $C$DW$52, DW_AT_name("_calloc")
	.dwattr $C$DW$52, DW_AT_TI_call

           STW     .D2T2   B3,*SP--(32)      ; |61| 
||         CALLP   .S2     _calloc,B3
||         MV      .L1X    B4,A12            ; |61| 
||         MV      .S1     A6,A13            ; |61| 
||         MVK     .L2     0x4,B4            ; |64| 

$C$RL0:    ; CALL OCCURS {_calloc} {0}       ; |64| 
;** --------------------------------------------------------------------------*
           MV      .L1X    B10,A11           ; |61| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_calloc")
	.dwattr $C$DW$53, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         MVK     .L2     0x4,B4            ; |65| 
||         STW     .D2T1   A4,*+SP(4)        ; |64| 
||         MV      .L1X    B10,A4            ; |61| 

$C$RL1:    ; CALL OCCURS {_calloc} {0}       ; |65| 
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("_calloc")
	.dwattr $C$DW$54, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         STW     .D2T1   A4,*+SP(8)        ; |65| 
||         MV      .L1     A11,A4            ; |66| 
||         MVK     .L2     0x4,B4            ; |66| 

$C$RL2:    ; CALL OCCURS {_calloc} {0}       ; |66| 
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_name("_calloc")
	.dwattr $C$DW$55, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         STW     .D2T1   A4,*+SP(12)       ; |66| 
||         MV      .L1     A11,A4            ; |67| 
||         MVK     .L2     0x4,B4            ; |67| 

$C$RL3:    ; CALL OCCURS {_calloc} {0}       ; |67| 
           STW     .D2T1   A4,*+SP(16)       ; |67| 
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_name("_copy_arr")
	.dwattr $C$DW$56, DW_AT_TI_call

           CALLP   .S2     _copy_arr,B3
||         LDW     .D2T1   *+SP(4),A4        ; |69| 
||         MV      .L1X    B10,A6            ; |61| 
||         MV      .L2X    A13,B4            ; |61| 

$C$RL4:    ; CALL OCCURS {_copy_arr} {0}     ; |69| 
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("_copy_arr")
	.dwattr $C$DW$57, DW_AT_TI_call

           CALLP   .S2     _copy_arr,B3
||         LDW     .D2T1   *+SP(12),A4       ; |70| 
||         MV      .L2X    A12,B4            ; |61| 

$C$RL5:    ; CALL OCCURS {_copy_arr} {0}     ; |70| 
;** --------------------------------------------------------------------------*

           SUB     .L2     B10,1,B4          ; |72| 
||         LDW     .D2T1   *+SP(4),A3        ; |74| 

           EXT     .S2     B4,16,16,B0       ; |72| 

   [!B0]   B       .S1     $C$L11            ; |72| 
||         EXT     .S2     B4,16,14,B12      ; |72| 
|| [ B0]   LDW     .D1T2   *A10,B5           ; |74| 
||         MV      .L2     B0,B11            ; |72| 

   [ B0]   LDW     .D2T2   *+SP(12),B4       ; |75| 
   [ B0]   CMPGT   .L1X    B11,0,A0          ; |76| 
           ADD     .L1X    B12,A3,A3         ; |74| 
   [ B0]   LDW     .D1T1   *A3,A4            ; |74| 
   [ B0]   ADD     .L2     B12,B5,B5         ; |74| 
           ; BRANCHCC OCCURS {$C$L11}        ; |72| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L8
;** --------------------------------------------------------------------------*
$C$L8:    
$C$DW$L$_calc_coefs$4$B:
	.dwpsn	file "../template2.c",line 73,column 0,is_stmt
           ADD     .L2     B12,B4,B4         ; |75| 
           SUB     .L2     B5,4,B5           ; |74| 
           NOP             2
           STW     .D2T1   A4,*B5            ; |74| 

           LDW     .D1T1   *+A10(4),A4       ; |75| 
||         LDW     .D2T2   *B4,B6            ; |75| 

   [!A0]   BNOP    .S1     $C$L10,3          ; |76| 
           ADD     .L2X    B12,A4,B4         ; |75| 
           STW     .D2T2   B6,*B4            ; |75| 
           ; BRANCHCC OCCURS {$C$L10}        ; |76| 
$C$DW$L$_calc_coefs$4$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_calc_coefs$5$B:

           MV      .L1X    B11,A4
||         LDW     .D2T1   *+SP(12),A11
||         MV      .L2X    A3,B10
||         ZERO    .S1     A30

           STW     .D2T1   A4,*+SP(20)
||         MV      .L1X    B4,A12

           LDW     .D2T2   *B10,B4           ; |77| 
||         LDW     .D1T1   *A12,A3           ; |77| 
||         MV      .L1X    B5,A14

           LDW     .D2T1   *+SP(8),A15
           LDW     .D2T2   *+SP(4),B13
           LDW     .D1T1   *A11++,A31        ; |77| 
	.dwpsn	file "../template2.c",line 76,column 0,is_stmt
           LDW     .D2T1   *+SP(16),A13
$C$DW$L$_calc_coefs$5$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*      Disqualified loop: Loop contains non-pipelinable instructions
;*----------------------------------------------------------------------------*
$C$L9:    
$C$DW$L$_calc_coefs$6$B:
           MPYSP   .M2X    B4,A3,B4          ; |77| 
           NOP             4
           SUBSP   .L1X    A31,B4,A3         ; |77| 
           NOP             3
           STW     .D1T1   A3,*A13++         ; |77| 

           LDW     .D2T2   *B10--,B4         ; |78| 
||         LDW     .D1T1   *A14,A4           ; |78| 

           LDW     .D2T2   *B13++,B5         ; |78| 
           NOP             3

           MPYSP   .M1     A4,A4,A3          ; |78| 
||         MPYSP   .M2X    A4,B4,B4          ; |78| 

           SET     .S1     A30,0x17,0x1d,A4
           NOP             2
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x08)
	.dwattr $C$DW$58, DW_AT_name("__divf")
	.dwattr $C$DW$58, DW_AT_TI_call

           SUBSP   .L2     B5,B4,B5          ; |78| 
||         SUBSP   .L1     A4,A3,A3          ; |78| 
||         CALL    .S1     __divf            ; |78| 

           ADDKPC  .S2     $C$RL6,B3,3       ; |78| 

           MV      .L2X    A3,B4             ; |78| 
||         MV      .L1X    B5,A4             ; |78| 

$C$RL6:    ; CALL OCCURS {__divf} {0}        ; |78| 
$C$DW$L$_calc_coefs$6$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_calc_coefs$7$B:
           LDW     .D2T2   *+SP(20),B4       ; |78| 
           STW     .D1T1   A4,*A15++         ; |78| 
           NOP             3

           SUB     .L1X    B4,1,A0           ; |76| 
||         SUB     .L2     B4,1,B4           ; |76| 

   [ A0]   B       .S1     $C$L9             ; |76| 
|| [ A0]   LDW     .D1T1   *A12,A3           ; |77| 
||         STW     .D2T2   B4,*+SP(20)       ; |78| 

   [ A0]   LDW     .D2T2   *B10,B4           ; |77| 
   [ A0]   ZERO    .L1     A30
   [ A0]   LDW     .D1T1   *A11++,A31        ; |77| 
	.dwpsn	file "../template2.c",line 80,column 0,is_stmt
           NOP             2
           ; BRANCHCC OCCURS {$C$L9}         ; |76| 
$C$DW$L$_calc_coefs$7$E:
;** --------------------------------------------------------------------------*
$C$L10:    
$C$DW$L$_calc_coefs$8$B:
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("_swap_arr")
	.dwattr $C$DW$59, DW_AT_TI_call

           CALLP   .S2     _swap_arr,B3
||         ADD     .L2     4,SP,B4           ; |81| 
||         ADD     .L1X    8,SP,A4           ; |81| 

$C$RL7:    ; CALL OCCURS {_swap_arr} {0}     ; |81| 
$C$DW$L$_calc_coefs$8$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_calc_coefs$9$B:
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_name("_swap_arr")
	.dwattr $C$DW$60, DW_AT_TI_call

           CALLP   .S2     _swap_arr,B3
||         ADDAW   .D1X    SP,4,A4           ; |82| 
||         ADD     .L2     12,SP,B4          ; |82| 

$C$RL8:    ; CALL OCCURS {_swap_arr} {0}     ; |82| 
$C$DW$L$_calc_coefs$9$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_calc_coefs$10$B:
           SUB     .L1X    B11,1,A3          ; |73| 

           EXT     .S1     A3,16,16,A0       ; |73| 
||         LDW     .D2T1   *+SP(4),A3        ; |74| 

   [ A0]   LDW     .D1T2   *A10,B5           ; |74| 
|| [ A0]   B       .S1     $C$L8             ; |73| 
||         MV      .L1     A0,A1             ; guard predicate rewrite

   [ A1]   LDW     .D2T2   *+SP(12),B4       ; |75| 
||         MV      .L2X    A0,B11            ; |73| 

           SUB     .L2     B12,4,B12         ; |73| 
   [ A0]   CMPGT   .L1X    B11,0,A0          ; |76| 
   [ A1]   ADD     .L1X    B12,A3,A3         ; |74| 
	.dwpsn	file "../template2.c",line 84,column 0,is_stmt

   [ A1]   ADD     .L2     B12,B5,B5         ; |74| 
|| [ A1]   LDW     .D1T1   *A3,A4            ; |74| 

           ; BRANCHCC OCCURS {$C$L8}         ; |73| 
$C$DW$L$_calc_coefs$10$E:
;** --------------------------------------------------------------------------*
$C$L11:    
           LDW     .D2T1   *+SP(12),A3       ; |85| 
           MV      .L1X    B11,A5            ; |85| 
           LDW     .D1T1   *+A10(4),A4       ; |85| 
           NOP             2
           LDW     .D1T1   *+A3[A5],A3       ; |85| 
           NOP             4
           STW     .D1T1   A3,*+A4[A5]       ; |85| 
           LDW     .D2T2   *++SP(32),B3      ; |86| 
           LDDW    .D2T1   *++SP,A13:A12     ; |86| 
           LDDW    .D2T1   *++SP,A15:A14     ; |86| 
           LDDW    .D2T2   *++SP,B11:B10     ; |86| 
           LDDW    .D2T2   *++SP,B13:B12     ; |86| 
$C$DW$61	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$61, DW_AT_low_pc(0x04)
	.dwattr $C$DW$61, DW_AT_TI_return

           LDW     .D2T1   *++SP(8),A10      ; |86| 
||         RET     .S2     B3                ; |86| 

           LDW     .D2T1   *++SP(8),A11      ; |86| 
	.dwpsn	file "../template2.c",line 86,column 1,is_stmt
           NOP             4
           ; BRANCH OCCURS {B3}              ; |86| 

$C$DW$62	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$62, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L8:1:1519257155")
	.dwattr $C$DW$62, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$62, DW_AT_TI_begin_line(0x49)
	.dwattr $C$DW$62, DW_AT_TI_end_line(0x54)
$C$DW$63	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$63, DW_AT_low_pc($C$DW$L$_calc_coefs$4$B)
	.dwattr $C$DW$63, DW_AT_high_pc($C$DW$L$_calc_coefs$4$E)
$C$DW$64	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$64, DW_AT_low_pc($C$DW$L$_calc_coefs$5$B)
	.dwattr $C$DW$64, DW_AT_high_pc($C$DW$L$_calc_coefs$5$E)
$C$DW$65	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$65, DW_AT_low_pc($C$DW$L$_calc_coefs$8$B)
	.dwattr $C$DW$65, DW_AT_high_pc($C$DW$L$_calc_coefs$8$E)
$C$DW$66	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$66, DW_AT_low_pc($C$DW$L$_calc_coefs$9$B)
	.dwattr $C$DW$66, DW_AT_high_pc($C$DW$L$_calc_coefs$9$E)
$C$DW$67	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$67, DW_AT_low_pc($C$DW$L$_calc_coefs$10$B)
	.dwattr $C$DW$67, DW_AT_high_pc($C$DW$L$_calc_coefs$10$E)

$C$DW$68	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$68, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L9:2:1519257155")
	.dwattr $C$DW$68, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$68, DW_AT_TI_begin_line(0x4c)
	.dwattr $C$DW$68, DW_AT_TI_end_line(0x50)
$C$DW$69	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$69, DW_AT_low_pc($C$DW$L$_calc_coefs$6$B)
	.dwattr $C$DW$69, DW_AT_high_pc($C$DW$L$_calc_coefs$6$E)
$C$DW$70	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$70, DW_AT_low_pc($C$DW$L$_calc_coefs$7$B)
	.dwattr $C$DW$70, DW_AT_high_pc($C$DW$L$_calc_coefs$7$E)
	.dwendtag $C$DW$68

	.dwendtag $C$DW$62

	.dwattr $C$DW$47, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$47, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$47, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$47

	.sect	".text"
	.clink
	.global	_main

$C$DW$71	.dwtag  DW_TAG_subprogram, DW_AT_name("main")
	.dwattr $C$DW$71, DW_AT_low_pc(_main)
	.dwattr $C$DW$71, DW_AT_high_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_main")
	.dwattr $C$DW$71, DW_AT_external
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$71, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$71, DW_AT_TI_begin_line(0x9d)
	.dwattr $C$DW$71, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$71, DW_AT_TI_max_frame_size(0x08)
	.dwattr $C$DW$71, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr $C$DW$71, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 158,column 1,is_stmt,address _main

;******************************************************************************
;* FUNCTION NAME: main                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,B0,B1,B2,B3,B4,*
;*                           B5,B6,B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,*
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 8 Save = 8 byte                    *
;******************************************************************************
_main:
;** --------------------------------------------------------------------------*
           STW     .D2T1   A11,*SP--(8)      ; |158| 
           LDH     .D2T2   *+DP(_lat_filt_len),B4 ; |159| 
           STW     .D2T1   A10,*+SP(4)       ; |158| 
           NOP             3
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_name("_calloc")
	.dwattr $C$DW$72, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         SUB     .L1X    B4,1,A4           ; |159| 
||         MVK     .L2     0x4,B4            ; |159| 

$C$RL9:    ; CALL OCCURS {_calloc} {0}       ; |159| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     _coefs,A10
           MVKH    .S1     _coefs,A10
           STW     .D1T1   A4,*A10           ; |159| 
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("_calloc")
	.dwattr $C$DW$73, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+DP(_lat_filt_len),A4 ; |160| 
||         MVK     .L2     0x4,B4            ; |160| 

$C$RL10:   ; CALL OCCURS {_calloc} {0}       ; |160| 
           STW     .D1T1   A4,*+A10(4)       ; |160| 
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_calloc")
	.dwattr $C$DW$74, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         LDH     .D2T1   *+DP(_lat_filt_len),A4 ; |161| 
||         MVK     .L2     0x4,B4            ; |161| 

$C$RL11:   ; CALL OCCURS {_calloc} {0}       ; |161| 

           MVKL    .S1     _num,A3
||         MVKL    .S2     _den,B5

           MVKH    .S1     _num,A3
||         MVKH    .S2     _den,B5

           LDH     .D2T2   *+DP(_lat_filt_len),B6 ; |162| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x0c)
	.dwattr $C$DW$75, DW_AT_name("_calc_coefs")
	.dwattr $C$DW$75, DW_AT_TI_call

           MV      .L1X    B5,A6             ; |162| 
||         MV      .L2X    A3,B4             ; |162| 
||         MV      .S1     A10,A4            ; |162| 
||         CALLP   .S2     _calc_coefs,B3

$C$RL12:   ; CALL OCCURS {_calc_coefs} {0}   ; |162| 
           MVKL    .S1     0xbb80,A4
	.dwpsn	file "../template2.c",line 166,column 0,is_stmt
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_L138_initialise_intr")
	.dwattr $C$DW$76, DW_AT_TI_call

           CALLP   .S2     _L138_initialise_intr,B3
||         MVKH    .S1     0xbb80,A4
||         ZERO    .L2     B4                ; |164| 
||         ZERO    .L1     A6                ; |164| 

$C$RL13:   ; CALL OCCURS {_L138_initialise_intr} {0}  ; |164| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Bad loop structure
;*----------------------------------------------------------------------------*
$C$L12:    
$C$DW$L$_main$3$B:
	.dwpsn	file "../template2.c",line 167,column 1,is_stmt
           BNOP    .S1     $C$L12,5          ; |166| 
           ; BRANCH OCCURS {$C$L12}          ; |166| 
$C$DW$L$_main$3$E:

$C$DW$77	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$77, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L12:1:1519257155")
	.dwattr $C$DW$77, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$77, DW_AT_TI_begin_line(0xa6)
	.dwattr $C$DW$77, DW_AT_TI_end_line(0xa7)
$C$DW$78	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$78, DW_AT_low_pc($C$DW$L$_main$3$B)
	.dwattr $C$DW$78, DW_AT_high_pc($C$DW$L$_main$3$E)
	.dwendtag $C$DW$77

	.dwattr $C$DW$71, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$71, DW_AT_TI_end_line(0xa7)
	.dwattr $C$DW$71, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$71

	.sect	".text"
	.clink
	.global	_lat_iir

$C$DW$79	.dwtag  DW_TAG_subprogram, DW_AT_name("lat_iir")
	.dwattr $C$DW$79, DW_AT_low_pc(_lat_iir)
	.dwattr $C$DW$79, DW_AT_high_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_lat_iir")
	.dwattr $C$DW$79, DW_AT_external
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$79, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$79, DW_AT_TI_begin_line(0x6a)
	.dwattr $C$DW$79, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$79, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$79, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$79, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 106,column 67,is_stmt,address _lat_iir
$C$DW$80	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg4]
$C$DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_name("k")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg20]
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("v")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_v")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg6]
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("g")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg22]
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg8]

;******************************************************************************
;* FUNCTION NAME: lat_iir                                                     *
;*                                                                            *
;*   Regs Modified     : A0,A1,A3,A4,A5,A6,A7,A8,A9,B0,B1,B4,B5,B6,B7,B8,B9,  *
;*                           A16,A17,A18,B16,B17                              *
;*   Regs Used         : A0,A1,A3,A4,A5,A6,A7,A8,A9,B0,B1,B3,B4,B5,B6,B7,B8,  *
;*                           B9,DP,SP,A16,A17,A18,B16,B17                     *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_lat_iir:
;** --------------------------------------------------------------------------*

           MV      .L1     A8,A16            ; |106| 
||         SUB     .S2     B4,B6,B5          ; |111| 
||         SUB     .S1X    B6,A6,A5          ; |111| 
||         SUB     .D2     B6,B4,B7          ; |111| 
||         CMPGT   .L2X    A8,0,B1           ; |110| 
||         MV      .D1     A6,A17            ; |106| 

           SHL     .S1     A16,2,A3          ; |111| 
||         SUB     .L1     A5,4,A5           ; |111| 
||         MV      .D1X    B6,A18            ; |106| 

           CMPGT   .L1X    A3,B5,A9          ; |111| 
||         SUB     .L2X    A6,B6,B5          ; |111| 
||         ZERO    .S2     B6                ; |108| 

           ADD     .L2     4,B5,B5           ; |111| 
||         CMPGT   .L1     A3,A5,A5          ; |111| 
|| [!B1]   B       .S1     $C$L21            ; |110| 

           CMPGT   .L2X    A3,B5,B5          ; |111| 
||         CMPGT   .L1X    A3,B7,A7          ; |111| 

           AND     .L2X    B5,A5,B5
||         AND     .L1     A7,A9,A5
||         MV      .S1     A4,A9             ; |106| 

           XOR     .L1     1,A5,A5
           XOR     .L2     1,B5,B5
           AND     .L2X    A5,B5,B0          ; |111| 
           ; BRANCHCC OCCURS {$C$L21}        ; |110| 
;** --------------------------------------------------------------------------*

   [ B0]   B       .S2     $C$L18            ; |111| 
||         ADD     .L1X    A3,B4,A4
||         ADD     .L2X    A3,B4,B7
||         MV      .S1     A16,A0
||         ADD     .D1     A3,A17,A5

           SUB     .S1     A4,4,A4
||         MV      .L2X    A17,B4
||         SUB     .S2     B7,4,B7
||         CMPGT   .L1     A0,1,A1
||         MV      .D1     A18,A6

   [ B0]   SUB     .L1     A16,1,A8
           MV      .L1     A18,A7
           ADD     .L2X    A3,B4,B8
   [ B0]   MV      .L2X    A4,B5
           ; BRANCHCC OCCURS {$C$L18}        ; |111| 
;** --------------------------------------------------------------------------*

   [ A1]   MV      .L1X    B6,A5
|| [ A1]   MV      .L2X    A6,B6
|| [ A1]   B       .S1     $C$L14
|| [ A1]   LDW     .D2T2   *B7--,B9          ; |111| (P) <0,2>  ^ 

   [ A1]   MV      .L1X    B8,A4
|| [ A1]   LDW     .D2T2   *+B6(4),B8        ; |111| (P) <0,0> 

   [!A1]   LDW     .D1T1   *+A6(4),A3        ; |111| 
   [!A1]   LDW     .D2T2   *B7--,B4          ; |111| 
           NOP             2
           ; BRANCHCC OCCURS {$C$L14} {0} 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L13
;** --------------------------------------------------------------------------*
$C$L13:    
$C$DW$L$_lat_iir$4$B:
	.dwpsn	file "../template2.c",line 110,column 0,is_stmt
           NOP             4
$C$DW$L$_lat_iir$4$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_lat_iir$5$B:
           MPYSP   .M1X    B4,A3,A4          ; |111| 
           SUB     .L1     A0,1,A0           ; |110| 
           NOP             2
           SUBSP   .L1     A9,A4,A9          ; |111| 
           NOP             4
           MPYSP   .M2X    A9,B4,B4          ; |112| 
           NOP             3
           ADDSP   .L2X    A3,B4,B5          ; |112| 
           NOP             3
           STW     .D1T2   B5,*A6++          ; |112| 
           LDW     .D2T2   *B8--,B4          ; |113| 
   [ A0]   LDW     .D1T1   *+A6(4),A3        ; |111| 
           NOP             2
   [ A0]   B       .S1     $C$L13            ; |110| 

   [!A0]   BNOP    .S1     $C$L22,3
||         MPYSP   .M2     B5,B4,B4          ; |113| 

	.dwpsn	file "../template2.c",line 114,column 0,is_stmt

           ADDSP   .L2     B4,B6,B6          ; |113| 
|| [ A0]   LDW     .D2T2   *B7--,B4          ; |111| 

           ; BRANCHCC OCCURS {$C$L13}        ; |110| 
$C$DW$L$_lat_iir$5$E:
;** --------------------------------------------------------------------------*
           STW     .D1T1   A9,*+A18[A16]     ; |115| 
           ; BRANCH OCCURS {$C$L22}  
;** --------------------------------------------------------------------------*
$C$L14:    
           MPYSP   .M2     B9,B8,B5          ; |111| (P) <0,7>  ^ 
           SUB     .L1     A0,2,A0
           DINT                              ; interrupts off
           NOP             1
           SUBSP   .L2X    A9,B5,B4          ; |111| 
           NOP             3
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../template2.c
;*      Loop source line                 : 110
;*      Loop opening brace source line   : 110
;*      Loop closing brace source line   : 114
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 65536                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 22
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 3
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1        0     
;*      .D units                     1        3*    
;*      .M units                     1        2     
;*      .X cross paths               1        0     
;*      .T address paths             1        3*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           1        2     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        1     
;*      Bound(.L .S .D .LS .LSD)     1        2     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 22 Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Epilog not removed
;*      Collapsed epilog stages       : 0
;*
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh4
;*
;*      Minimum safe trip count       : 2
;*----------------------------------------------------------------------------*
$C$L15:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
$C$L16:    ; PIPED LOOP KERNEL
$C$DW$L$_lat_iir$9$B:
	.dwpsn	file "../template2.c",line 110,column 0,is_stmt
           MPYSP   .M2     B4,B9,B5          ; |112| <0,15>  ^ 
           NOP             3
           ADDSP   .L2     B8,B5,B5          ; |112| <0,19>  ^ 
           NOP             2
           LDW     .D2T2   *+B6(8),B8        ; |111| <1,0> 
           STW     .D2T2   B5,*B6++          ; |112| <0,23>  ^ 

           LDW     .D1T1   *A4--,A3          ; |113| <0,24> 
||         LDW     .D2T2   *B7--,B9          ; |111| <1,2>  ^ 

           NOP             4

           MPYSP   .M1X    B5,A3,A3          ; |113| <0,29> 
||         MPYSP   .M2     B9,B8,B5          ; |111| <1,7>  ^ 

           NOP             1
   [ A0]   BDEC    .S1     $C$L16,A0         ; |110| <0,31> 
           NOP             1

           ADDSP   .L1     A3,A5,A5          ; |113| <0,33> 
||         SUBSP   .L2     B4,B5,B4          ; |111| <1,11>  ^ 

	.dwpsn	file "../template2.c",line 114,column 0,is_stmt
           NOP             3
$C$DW$L$_lat_iir$9$E:
;** --------------------------------------------------------------------------*
$C$L17:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
           MPYSP   .M2     B4,B9,B5          ; |112| (E) <1,15>  ^ 
           MV      .L1X    B4,A9
           RINT                              ; interrupts on
           NOP             1
           ADDSP   .L2     B8,B5,B4          ; |112| (E) <1,19>  ^ 
           NOP             3
           STW     .D2T2   B4,*B6++          ; |112| (E) <1,23>  ^ 
           LDW     .D1T1   *A4--,A3          ; |113| (E) <1,24> 
           STW     .D1T1   A9,*+A18[A16]     ; |115| 
           NOP             3
           MPYSP   .M1X    B4,A3,A3          ; |113| (E) <1,29> 
           NOP             3

           BNOP    .S1     $C$L23,4          ; |110| 
||         ADDSP   .L1     A3,A5,A3          ; |113| (E) <1,33> 

           MV      .L2X    A3,B6
||         LDW     .D1T1   *A17,A3           ; |116| 

           ; BRANCH OCCURS {$C$L23}          ; |110| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../template2.c
;*      Loop source line                 : 110
;*      Loop opening brace source line   : 110
;*      Loop closing brace source line   : 114
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 65536                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 4
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     2*       2*    
;*      .M units                     2*       1     
;*      .X cross paths               3*       0     
;*      .T address paths             2*       2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           3        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             2*       0     
;*      Bound(.L .S .D .LS .LSD)     2*       1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 4  Schedule found with 7 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh4
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L18:    ; PIPED LOOP PROLOG
	.dwpsn	file "../template2.c",line 110,column 0,is_stmt

           SPLOOPD 4       ;28               ; (P) 
||         ADD     .L1     4,A7,A8
||         MVC     .S2X    A8,ILC

;** --------------------------------------------------------------------------*
$C$L19:    ; PIPED LOOP KERNEL
$C$DW$L$_lat_iir$13$B:

           LDW     .D2T2   *B5--,B9          ; |111| (P) <0,0> 
||         LDW     .D1T1   *A8++,A6          ; |111| (P) <0,0> 

           NOP             4

           SPMASK          L2
||         MV      .L2X    A5,B8
||         MPYSP   .M1X    B9,A6,A5          ; |111| (P) <0,5> 

           MVD     .M1     A6,A4             ; |111| (P) <0,6> Split a long life
           NOP             1
           MVD     .M2     B9,B4             ; |111| (P) <0,8> Split a long life
           SUBSP   .L1     A9,A5,A9          ; |111| (P) <0,9>  ^ 
           NOP             2
           MVD     .M1     A4,A3             ; |111| (P) <0,12> Split a long life
           NOP             1
           MPYSP   .M2X    A9,B4,B17         ; |112| (P) <0,14> 
           NOP             3
           LDW     .D2T2   *B8--,B16         ; |113| (P) <0,18> 
           ADDSP   .L2X    A3,B17,B7         ; |112| (P) <0,19> 
           NOP             3
           MPYSP   .M2     B7,B16,B4         ; |113| (P) <0,23> 
           MV      .S1X    B7,A4             ; |112| <0,24> Define a twin register
           STW     .D1T1   A4,*A7++          ; |112| <0,25> 
           NOP             1
	.dwpsn	file "../template2.c",line 114,column 0,is_stmt

           SPKERNEL 6,0
||         ADDSP   .S2     B4,B6,B6          ; |113| <0,27>  ^ 

$C$DW$L$_lat_iir$13$E:
;** --------------------------------------------------------------------------*
$C$L20:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L21:    
           STW     .D1T1   A9,*+A18[A16]     ; |115| 
;** --------------------------------------------------------------------------*
$C$L22:    
           LDW     .D1T1   *A17,A3           ; |116| 
;** --------------------------------------------------------------------------*
$C$L23:    
           NOP             4
           MPYSP   .M1     A9,A3,A3          ; |116| 
           NOP             1
$C$DW$85	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$85, DW_AT_low_pc(0x00)
	.dwattr $C$DW$85, DW_AT_TI_return
           RETNOP  .S2     B3,1              ; |118| 
           ADDSP   .L1X    A3,B6,A4          ; |116| 
	.dwpsn	file "../template2.c",line 118,column 1,is_stmt
           NOP             3
           ; BRANCH OCCURS {B3}              ; |118| 

$C$DW$86	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$86, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L19:1:1519257155")
	.dwattr $C$DW$86, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$86, DW_AT_TI_begin_line(0x6e)
	.dwattr $C$DW$86, DW_AT_TI_end_line(0x72)
$C$DW$87	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$87, DW_AT_low_pc($C$DW$L$_lat_iir$13$B)
	.dwattr $C$DW$87, DW_AT_high_pc($C$DW$L$_lat_iir$13$E)
	.dwendtag $C$DW$86


$C$DW$88	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$88, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L16:1:1519257155")
	.dwattr $C$DW$88, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$88, DW_AT_TI_begin_line(0x6e)
	.dwattr $C$DW$88, DW_AT_TI_end_line(0x72)
$C$DW$89	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$89, DW_AT_low_pc($C$DW$L$_lat_iir$9$B)
	.dwattr $C$DW$89, DW_AT_high_pc($C$DW$L$_lat_iir$9$E)
	.dwendtag $C$DW$88


$C$DW$90	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$90, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L13:1:1519257155")
	.dwattr $C$DW$90, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$90, DW_AT_TI_begin_line(0x6e)
	.dwattr $C$DW$90, DW_AT_TI_end_line(0x72)
$C$DW$91	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$91, DW_AT_low_pc($C$DW$L$_lat_iir$4$B)
	.dwattr $C$DW$91, DW_AT_high_pc($C$DW$L$_lat_iir$4$E)
$C$DW$92	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$92, DW_AT_low_pc($C$DW$L$_lat_iir$5$B)
	.dwattr $C$DW$92, DW_AT_high_pc($C$DW$L$_lat_iir$5$E)
	.dwendtag $C$DW$90

	.dwattr $C$DW$79, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$79, DW_AT_TI_end_line(0x76)
	.dwattr $C$DW$79, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$79

	.sect	".text:retain"
	.retain
	.retainrefs
	.global	_interrupt4

$C$DW$93	.dwtag  DW_TAG_subprogram, DW_AT_name("interrupt4")
	.dwattr $C$DW$93, DW_AT_low_pc(_interrupt4)
	.dwattr $C$DW$93, DW_AT_high_pc(0x00)
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_interrupt4")
	.dwattr $C$DW$93, DW_AT_external
	.dwattr $C$DW$93, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$93, DW_AT_TI_begin_line(0x7c)
	.dwattr $C$DW$93, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$93, DW_AT_TI_interrupt
	.dwattr $C$DW$93, DW_AT_TI_max_frame_size(0xe0)
	.dwattr $C$DW$93, DW_AT_frame_base[DW_OP_breg31 224]
	.dwattr $C$DW$93, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 125,column 1,is_stmt,address _interrupt4

;******************************************************************************
;* FUNCTION NAME: interrupt4                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,   *
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 224 Save = 224 byte                *
;******************************************************************************
_interrupt4:
;** --------------------------------------------------------------------------*
           ADDK    .S2     -224,SP           ; |125| 
           STDW    .D2T2   B1:B0,*+SP(56)    ; |125| 
           STW     .D2T2   B3,*+SP(216)      ; |125| 
           STDW    .D2T2   B31:B30,*+SP(208) ; |125| 
           STDW    .D2T2   B29:B28,*+SP(200) ; |125| 
           STDW    .D2T2   B27:B26,*+SP(192) ; |125| 
           STDW    .D2T2   B25:B24,*+SP(184) ; |125| 
           STDW    .D2T2   B23:B22,*+SP(176) ; |125| 
           STDW    .D2T2   B21:B20,*+SP(168) ; |125| 
           STDW    .D2T2   B19:B18,*+SP(160) ; |125| 
           STDW    .D2T2   B17:B16,*+SP(152) ; |125| 
           STDW    .D2T1   A31:A30,*+SP(144) ; |125| 
           STDW    .D2T1   A29:A28,*+SP(136) ; |125| 
           STDW    .D2T1   A27:A26,*+SP(128) ; |125| 
           STDW    .D2T1   A25:A24,*+SP(120) ; |125| 
           STDW    .D2T1   A23:A22,*+SP(112) ; |125| 
           STDW    .D2T1   A21:A20,*+SP(104) ; |125| 
           STDW    .D2T1   A19:A18,*+SP(96)  ; |125| 
           STDW    .D2T1   A17:A16,*+SP(88)  ; |125| 
           STDW    .D2T2   B9:B8,*+SP(80)    ; |125| 
           STDW    .D2T2   B7:B6,*+SP(72)    ; |125| 
           STDW    .D2T2   B5:B4,*+SP(64)    ; |125| 
           STW     .D2T2   B2,*+SP(224)      ; |125| 
           STDW    .D2T1   A9:A8,*+SP(48)    ; |125| 
           STDW    .D2T1   A7:A6,*+SP(40)    ; |125| 
           STDW    .D2T1   A5:A4,*+SP(32)    ; |125| 
           STDW    .D2T1   A3:A2,*+SP(24)    ; |125| 
           STDW    .D2T1   A1:A0,*+SP(16)    ; |125| 

           MVC     .S2     ILC,B0            ; |125| 
||         STW     .D2T1   A10,*+SP(220)     ; |125| 

           STW     .D2T2   B0,*+SP(12)       ; |125| 
||         MVC     .S2     RILC,B0           ; |125| 

           STW     .D2T2   B0,*+SP(8)        ; |125| 
||         MVC     .S2     ITSR,B0           ; |125| 

$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_input_left_sample")
	.dwattr $C$DW$94, DW_AT_TI_call

           CALLP   .S2     _input_left_sample,B3
||         STW     .D2T2   B0,*+SP(4)        ; |125| 

$C$RL14:   ; CALL OCCURS {_input_left_sample} {0}  ; |127| 
;** --------------------------------------------------------------------------*
           MV      .L2X    A4,B4             ; |127| 
           INTSP   .L2     B4,B4             ; |127| 
           NOP             3
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_input_right_sample")
	.dwattr $C$DW$95, DW_AT_TI_call

           CALLP   .S2     _input_right_sample,B3
||         STW     .D2T2   B4,*+DP(_y)       ; |127| 

$C$RL15:   ; CALL OCCURS {_input_right_sample} {0}  ; |128| 
           INTSP   .L1     A4,A3             ; |128| 
           MVKL    .S1     _SO_Mag,A10

           LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |135| 
||         MVKH    .S1     _SO_Mag,A10
||         MVKL    .S2     _g0,B6

           LDW     .D2T1   *+DP(_y),A4       ; |135| 
||         MVKH    .S2     _g0,B6

$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$96, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         STW     .D2T1   A3,*+DP(_xn)      ; |128| 
||         MV      .L2X    A10,B4            ; |135| 
||         ADD     .L1     12,A10,A6         ; |135| 

$C$RL16:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |135| 

           ADD     .D1     A10,24,A3         ; |136| 
||         MVKL    .S2     _g1,B6

           MVKH    .S2     _g1,B6
||         STW     .D2T1   A4,*+DP(_y)       ; |135| 

$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$97, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |136| 
||         MV      .L2X    A3,B4             ; |136| 
||         ADDAW   .D1     A10,9,A6          ; |136| 

$C$RL17:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |136| 

           MVKL    .S1     _g2,A5
||         ADDAD   .D1     A10,6,A3          ; |137| 

           MVKH    .S1     _g2,A5

           MV      .L2X    A3,B4             ; |137| 
||         STW     .D2T1   A4,*+DP(_y)       ; |136| 

$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$98, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |137| 
||         MV      .L2X    A5,B6             ; |137| 
||         ADDAW   .D1     A10,15,A6         ; |137| 

$C$RL18:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |137| 

           ADDAD   .D1     A10,9,A3          ; |138| 
||         MVKL    .S2     _g3,B6

           MVKH    .S2     _g3,B6
||         STW     .D2T1   A4,*+DP(_y)       ; |137| 

$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("_ASM_direct_2")
	.dwattr $C$DW$99, DW_AT_TI_call

           CALLP   .S2     _ASM_direct_2,B3
||         LDH     .D2T1   *+DP(_direct_filt_len),A8 ; |138| 
||         MV      .L2X    A3,B4             ; |138| 
||         ADDAW   .D1     A10,21,A6         ; |138| 

$C$RL19:   ; CALL OCCURS {_ASM_direct_2} {0}  ; |138| 
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_name("__fixfu")
	.dwattr $C$DW$100, DW_AT_TI_call

           CALLP   .S2     __fixfu,B3
||         STW     .D2T1   A4,*+DP(_y)       ; |138| 

$C$RL20:   ; CALL OCCURS {__fixfu} {0}       ; |150| 
           MVKL    .S1     _codec_data,A3
           MVKH    .S1     _codec_data,A3
           PACK2   .L1     A4,A4,A4          ; |150| 

           MV      .L2X    A3,B4             ; |150| 
||         STW     .D1T1   A4,*A3            ; |150| 

$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_name("_output_sample")
	.dwattr $C$DW$101, DW_AT_TI_call

           CALLP   .S2     _output_sample,B3
||         LDW     .D2T1   *B4,A4            ; |151| 

$C$RL21:   ; CALL OCCURS {_output_sample} {0}  ; |151| 
;** --------------------------------------------------------------------------*
           LDW     .D2T2   *+SP(12),B0       ; |154| 
           LDDW    .D2T1   *+SP(24),A3:A2    ; |154| 
           LDDW    .D2T1   *+SP(32),A5:A4    ; |154| 
           LDDW    .D2T1   *+SP(40),A7:A6    ; |154| 
           LDDW    .D2T1   *+SP(48),A9:A8    ; |154| 

           MVC     .S2     B0,ILC            ; |154| 
||         LDW     .D2T2   *+SP(8),B0        ; |154| 

           LDDW    .D2T2   *+SP(64),B5:B4    ; |154| 
           LDDW    .D2T2   *+SP(72),B7:B6    ; |154| 
           LDDW    .D2T2   *+SP(80),B9:B8    ; |154| 
           LDDW    .D2T1   *+SP(88),A17:A16  ; |154| 
           LDDW    .D2T1   *+SP(96),A19:A18  ; |154| 
           LDDW    .D2T1   *+SP(104),A21:A20 ; |154| 
           LDDW    .D2T1   *+SP(112),A23:A22 ; |154| 
           LDDW    .D2T1   *+SP(120),A25:A24 ; |154| 
           LDDW    .D2T1   *+SP(128),A27:A26 ; |154| 
           LDDW    .D2T1   *+SP(136),A29:A28 ; |154| 
           LDDW    .D2T1   *+SP(144),A31:A30 ; |154| 
           LDDW    .D2T2   *+SP(152),B17:B16 ; |154| 
           LDDW    .D2T2   *+SP(160),B19:B18 ; |154| 
           LDDW    .D2T2   *+SP(168),B21:B20 ; |154| 
           LDDW    .D2T2   *+SP(176),B23:B22 ; |154| 
           LDDW    .D2T2   *+SP(184),B25:B24 ; |154| 
           LDDW    .D2T2   *+SP(192),B27:B26 ; |154| 
           LDDW    .D2T2   *+SP(200),B29:B28 ; |154| 
           LDDW    .D2T2   *+SP(208),B31:B30 ; |154| 
           MVC     .S2     B0,RILC           ; |154| 
           LDW     .D2T2   *+SP(4),B0        ; |154| 
           LDW     .D2T2   *+SP(216),B3      ; |154| 
           LDW     .D2T1   *+SP(220),A10     ; |154| 
           LDW     .D2T2   *+SP(224),B2      ; |154| 
           LDDW    .D2T1   *+SP(16),A1:A0    ; |154| 
           MVC     .S2     B0,ITSR           ; |154| 
           LDDW    .D2T2   *+SP(56),B1:B0    ; |154| 
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_TI_return
           RET     .S2     IRP               ; |154| 
           ADDK    .S2     224,SP            ; |154| 
	.dwpsn	file "../template2.c",line 154,column 1,is_stmt
           NOP             4
           ; BRANCH OCCURS {IRP}             ; |154| 
	.dwattr $C$DW$93, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$93, DW_AT_TI_end_line(0x9a)
	.dwattr $C$DW$93, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$93

	.sect	".text"
	.clink
	.global	_direct_2

$C$DW$103	.dwtag  DW_TAG_subprogram, DW_AT_name("direct_2")
	.dwattr $C$DW$103, DW_AT_low_pc(_direct_2)
	.dwattr $C$DW$103, DW_AT_high_pc(0x00)
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_direct_2")
	.dwattr $C$DW$103, DW_AT_external
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$103, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$103, DW_AT_TI_begin_line(0x59)
	.dwattr $C$DW$103, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$103, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$103, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$103, DW_AT_TI_skeletal
	.dwpsn	file "../template2.c",line 89,column 75,is_stmt,address _direct_2
$C$DW$104	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg4]
$C$DW$105	.dwtag  DW_TAG_formal_parameter, DW_AT_name("num")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_num")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_reg20]
$C$DW$106	.dwtag  DW_TAG_formal_parameter, DW_AT_name("den")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_den")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg6]
$C$DW$107	.dwtag  DW_TAG_formal_parameter, DW_AT_name("g")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_g")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg22]
$C$DW$108	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg8]

;******************************************************************************
;* FUNCTION NAME: direct_2                                                    *
;*                                                                            *
;*   Regs Modified     : A0,A3,A4,A5,A6,A7,A8,A9,B0,B4,B5,B6,B7,B8,B9,A16,A17,*
;*                           B16                                              *
;*   Regs Used         : A0,A3,A4,A5,A6,A7,A8,A9,B0,B3,B4,B5,B6,B7,B8,B9,A16, *
;*                           A17,B16                                          *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_direct_2:
;** --------------------------------------------------------------------------*

           CMPLT   .L1     A8,2,A0           ; |92| 
||         SUB     .L2X    A8,1,B16          ; |92| 
||         MV      .S1     A8,A17            ; |89| 
||         ZERO    .D1     A8                ; |91| 
||         ADD     .S2     4,B4,B8

   [ A0]   BNOP    .S2     $C$L27,4          ; |92| 
|| [!A0]   SUB     .L2     B16,1,B7
||         MV      .L1     A4,A7             ; |89| 
||         ADD     .S1     4,A6,A6
||         MV      .D1X    B6,A9             ; |89| 

           MV      .L1X    B4,A16            ; |89| 
           ; BRANCHCC OCCURS {$C$L27}        ; |92| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../template2.c
;*      Loop source line                 : 92
;*      Loop opening brace source line   : 92
;*      Loop closing brace source line   : 95
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 32766                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 4
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 3
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        1     
;*      .D units                     1        2     
;*      .M units                     1        1     
;*      .X cross paths               3*       0     
;*      .T address paths             1        2     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        0     (.L or .S unit)
;*      Addition ops (.LSD)          2        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        1     
;*      Bound(.L .S .D .LS .LSD)     2        2     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 4  Schedule found with 4 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh4
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L24:    ; PIPED LOOP PROLOG
	.dwpsn	file "../template2.c",line 92,column 0,is_stmt

           SPLOOPD 4       ;16               ; (P) 
||         MV      .L2     B6,B7
||         MVC     .S2     B7,ILC

;** --------------------------------------------------------------------------*
$C$L25:    ; PIPED LOOP KERNEL
$C$DW$L$_direct_2$3$B:
           LDW     .D2T2   *B7++,B4          ; |93| (P) <0,0> 
           LDW     .D2T2   *B8++,B6          ; |94| (P) <0,1> 
           LDW     .D1T1   *A6++,A3          ; |93| (P) <0,2> 
           NOP             1
           ZERO    .L2     B6                ; |93| (P) <0,4> 

           MV      .L2     B4,B5             ; |93| (P) <0,5> Split a long life
||         SET     .S2     B6,31,31,B9       ; |93| (P) <0,5> 

           MPYSP   .M2     B5,B6,B4          ; |94| (P) <0,6> 
           XOR     .L1X    A3,B9,A5          ; |93| (P) <0,7> 
           NOP             1
           MPYSP   .M1X    B5,A5,A4          ; |93| (P) <0,9> 
           NOP             2
           MV      .D1X    B4,A3             ; |94| <0,12> Define a twin register
	.dwpsn	file "../template2.c",line 95,column 0,is_stmt

           SPKERNEL 2,0
||         ADDSP   .S1     A4,A7,A7          ; |93| <0,13>  ^ 
||         ADDSP   .L1     A3,A8,A8          ; |94| <0,13>  ^ 

$C$DW$L$_direct_2$3$E:
;** --------------------------------------------------------------------------*
$C$L26:    ; PIPED LOOP EPILOG
           NOP             2
;** --------------------------------------------------------------------------*
$C$L27:    

           CMPGT   .L2     B16,0,B0          ; |97| 
||         ADDAW   .D1     A9,A17,A3

   [!B0]   BNOP    .S1     $C$L31,5          ; |97| 
|| [ B0]   MVC     .S2     B16,ILC
||         SUB     .L1     A3,4,A4

           ; BRANCHCC OCCURS {$C$L31}        ; |97| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../template2.c
;*      Loop source line                 : 97
;*      Loop opening brace source line   : 97
;*      Loop closing brace source line   : 99
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 32766                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1*       1*    
;*      .M units                     0        0     
;*      .X cross paths               0        1*    
;*      .T address paths             1*       1*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        0     
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 7 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh4
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L28:    ; PIPED LOOP PROLOG
	.dwpsn	file "../template2.c",line 97,column 0,is_stmt

           SPLOOP  1       ;7                ; (P) 
||         MV      .L2X    A4,B5
||         SUB     .L1     A4,4,A4

;** --------------------------------------------------------------------------*
$C$L29:    ; PIPED LOOP KERNEL
$C$DW$L$_direct_2$7$B:
           LDW     .D1T1   *A4--,A3          ; |98| (P) <0,0> 
           NOP             4
           MV      .L2X    A3,B4             ; |98| (P) <0,5> Define a twin register
	.dwpsn	file "../template2.c",line 99,column 0,is_stmt

           SPKERNEL 6,0
||         STW     .D2T2   B4,*B5--          ; |98| <0,6> 

$C$DW$L$_direct_2$7$E:
;** --------------------------------------------------------------------------*
$C$L30:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L31:    
           STW     .D1T1   A7,*A9            ; |100| 
           LDW     .D1T1   *A16,A3           ; |101| 
           NOP             4
           MPYSP   .M1     A7,A3,A3          ; |101| 
           NOP             1
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_TI_return
           RETNOP  .S2     B3,1              ; |102| 
           ADDSP   .L1     A3,A8,A4          ; |101| 
	.dwpsn	file "../template2.c",line 102,column 1,is_stmt
           NOP             3
           ; BRANCH OCCURS {B3}              ; |102| 

$C$DW$110	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$110, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L29:1:1519257155")
	.dwattr $C$DW$110, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$110, DW_AT_TI_begin_line(0x61)
	.dwattr $C$DW$110, DW_AT_TI_end_line(0x63)
$C$DW$111	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$111, DW_AT_low_pc($C$DW$L$_direct_2$7$B)
	.dwattr $C$DW$111, DW_AT_high_pc($C$DW$L$_direct_2$7$E)
	.dwendtag $C$DW$110


$C$DW$112	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$112, DW_AT_name("D:\CSS Workspace\lab3\Release\template2.asm:$C$L25:1:1519257155")
	.dwattr $C$DW$112, DW_AT_TI_begin_file("../template2.c")
	.dwattr $C$DW$112, DW_AT_TI_begin_line(0x5c)
	.dwattr $C$DW$112, DW_AT_TI_end_line(0x5f)
$C$DW$113	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$113, DW_AT_low_pc($C$DW$L$_direct_2$3$B)
	.dwattr $C$DW$113, DW_AT_high_pc($C$DW$L$_direct_2$3$E)
	.dwendtag $C$DW$112

	.dwattr $C$DW$103, DW_AT_TI_end_file("../template2.c")
	.dwattr $C$DW$103, DW_AT_TI_end_line(0x66)
	.dwattr $C$DW$103, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$103

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_calloc
	.global	_L138_initialise_intr
	.global	_input_right_sample
	.global	_input_left_sample
	.global	_output_sample
	.global	_ASM_direct_2
	.global	__divf
	.global	__fixfu

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$T$21	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x04)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$114, DW_AT_name("uint")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_uint")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$115, DW_AT_name("channel")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("AIC31_data_type")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x20)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

$C$DW$T$20	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$116	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$116, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$20

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$22	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$22, DW_AT_address_class(0x20)
$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x20)

$C$DW$T$58	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x18)
$C$DW$117	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$117, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x60)
$C$DW$118	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$118, DW_AT_upper_bound(0x03)
$C$DW$119	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$119, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$59


$C$DW$T$61	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x0c)
$C$DW$120	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$120, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x24)
$C$DW$121	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$121, DW_AT_upper_bound(0x08)
	.dwendtag $C$DW$T$62

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_name("lat_ladder_coef")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$122, DW_AT_name("k")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$123, DW_AT_name("v")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_v")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("lat_ladder_coef")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x20)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg0]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_reg1]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg2]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg3]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_reg4]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_reg5]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_reg6]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_reg7]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg8]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg9]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg10]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg11]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg12]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg13]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg14]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg15]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg16]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg17]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg18]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg19]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg20]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg21]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg22]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg23]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg24]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg25]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg26]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg27]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg28]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg29]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg30]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg31]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x20]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x21]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x22]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x23]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x24]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x25]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x26]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x27]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x28]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x29]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x30]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x31]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x32]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x33]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x34]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x35]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x36]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x37]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x38]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x39]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x40]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x41]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x42]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x43]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x44]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_regx 0x45]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x46]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_regx 0x47]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_regx 0x48]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_regx 0x49]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_regx 0x50]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_regx 0x51]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x52]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_regx 0x53]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_regx 0x54]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_regx 0x55]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_regx 0x56]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_regx 0x57]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_regx 0x58]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_regx 0x59]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_regx 0x60]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_regx 0x61]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_regx 0x62]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_regx 0x63]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_regx 0x64]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_regx 0x65]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_regx 0x66]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_regx 0x67]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_regx 0x68]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_regx 0x69]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_regx 0x70]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_regx 0x71]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x72]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x73]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x74]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x75]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x76]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x77]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x78]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x79]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

