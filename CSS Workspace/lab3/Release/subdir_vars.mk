################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../linker_dsp.cmd 

SA_SRCS += \
../ASM_direct_2.sa \
../ASM_lat_iir.sa \
../ASM_test.sa 

ASM_SRCS += \
../vectors_intr.asm 

C_SRCS += \
../L138_aic3106_init.c \
../template2.c 

OBJS += \
./ASM_direct_2.obj \
./ASM_lat_iir.obj \
./ASM_test.obj \
./L138_aic3106_init.obj \
./template2.obj \
./vectors_intr.obj 

ASM_DEPS += \
./vectors_intr.pp 

C_DEPS += \
./L138_aic3106_init.pp \
./template2.pp 

SA_DEPS += \
./ASM_direct_2.pp \
./ASM_lat_iir.pp \
./ASM_test.pp 

C_DEPS__QUOTED += \
"L138_aic3106_init.pp" \
"template2.pp" 

OBJS__QUOTED += \
"ASM_direct_2.obj" \
"ASM_lat_iir.obj" \
"ASM_test.obj" \
"L138_aic3106_init.obj" \
"template2.obj" \
"vectors_intr.obj" 

SA_DEPS__QUOTED += \
"ASM_direct_2.pp" \
"ASM_lat_iir.pp" \
"ASM_test.pp" 

ASM_DEPS__QUOTED += \
"vectors_intr.pp" 

SA_SRCS__QUOTED += \
"../ASM_direct_2.sa" \
"../ASM_lat_iir.sa" \
"../ASM_test.sa" 

C_SRCS__QUOTED += \
"../L138_aic3106_init.c" \
"../template2.c" 

ASM_SRCS__QUOTED += \
"../vectors_intr.asm" 


