#include "L138_aic3106_init.h"
AIC31_data_type codec_data;

float x,d,e;
//float beta = 0.0000000298023223876953125;				// 2^-25
float beta = 0.000000000931322574615478515625;		// 2^-30
//float beta = 0.000000000002910383045673370361328125;	// 2^-35

extern float x_buf[256];
extern float h_buf[256];
int offset = 0;

float ASM_adaptive(float x, float d, float beta, int offset);
void ASM_init();

interrupt void interrupt4(void) {
	// Get the next sample of the input.
	x = (float)(input_left_sample()); // input from ADC
	d = (float)(input_right_sample()); // input from ADC

	e = ASM_adaptive(x,d,beta,offset);

    // output to BOTH right and left channels
	codec_data.channel[LEFT] = (uint16_t)(e);
	codec_data.channel[RIGHT] = (uint16_t)(e);
	output_sample(codec_data.uint);  // output to L and R DAC
	offset++;

	return;
}

void set_init_values(float h[], float x[]) {
	int i;
	for(i = 0; i < 256; i++) {
		h[i] = 0;
		x[i] = 0;
	}
	h[0] = 1;
}

int main(void) {
	set_init_values(h_buf,x_buf);
	ASM_init();
	L138_initialise_intr(FS_44100_HZ,ADC_GAIN_0DB,DAC_ATTEN_0DB);
	while(true);
}

