################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../linker_dsp.cmd 

SA_SRCS += \
../ASM_adaptive.sa \
../ASM_init.sa 

ASM_SRCS += \
../vectors_intr.asm 

C_SRCS += \
../L138_aic3106_init.c \
../adaptive.c 

OBJS += \
./ASM_adaptive.obj \
./ASM_init.obj \
./L138_aic3106_init.obj \
./adaptive.obj \
./vectors_intr.obj 

ASM_DEPS += \
./vectors_intr.pp 

C_DEPS += \
./L138_aic3106_init.pp \
./adaptive.pp 

SA_DEPS += \
./ASM_adaptive.pp \
./ASM_init.pp 

C_DEPS__QUOTED += \
"L138_aic3106_init.pp" \
"adaptive.pp" 

OBJS__QUOTED += \
"ASM_adaptive.obj" \
"ASM_init.obj" \
"L138_aic3106_init.obj" \
"adaptive.obj" \
"vectors_intr.obj" 

SA_DEPS__QUOTED += \
"ASM_adaptive.pp" \
"ASM_init.pp" 

ASM_DEPS__QUOTED += \
"vectors_intr.pp" 

SA_SRCS__QUOTED += \
"../ASM_adaptive.sa" \
"../ASM_init.sa" 

C_SRCS__QUOTED += \
"../L138_aic3106_init.c" \
"../adaptive.c" 

ASM_SRCS__QUOTED += \
"../vectors_intr.asm" 


