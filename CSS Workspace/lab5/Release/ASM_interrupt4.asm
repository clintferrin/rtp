;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Wed Mar 28 13:55:58 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=linasm --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Enabled with threshold = 0                           *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ASM_interrupt4.sa")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("E:\Documents\dev\rtp\CSS Workspace\lab5\Release")
	.sect	".text"
		.title  "ASM_interrupt4.sa"
		.def    _interrupt4
		.sect  ".text"
; 		=======================================================
;    			   	  		Program flow
; 		=======================================================
;		initialization
;		x = ch1 				// read incoming data
;		if(idx==1024) { 		// calculate fft
;			set idx to 0
;			calculate fft
;			set ch2 to pulse
;		}
;
;		elif(idx==512) {
;			set ch2 to pulse
;		}
;
;		else {
;			set ch2 to null
;		}
;
;		ch1 = magnitude of complex x[idx]
;		x[idx  ] = x
;		x[idx+1] = 0			// imaginary value
;		idx += 2				// for complex and real
;
;		write out data
;		_______________________________________________________
		.global _interrupt4, _cfftr2_dit, _bitrev
        .global     _W, _x, _scale, _i_data, _idx
	.sect	".text"
	.clink

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("interrupt4")
	.dwattr $C$DW$1, DW_AT_low_pc(_interrupt4)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_interrupt4")
	.dwattr $C$DW$1, DW_AT_TI_begin_file("../ASM_interrupt4.sa")
	.dwattr $C$DW$1, DW_AT_TI_begin_line(0x20)
	.dwattr $C$DW$1, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$1, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$1, DW_AT_TI_skeletal
	.dwpsn	file "../ASM_interrupt4.sa",line 32,column 1,is_stmt,address _interrupt4

;******************************************************************************
;* FUNCTION NAME: interrupt4                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,A16,A17,A18, *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,DP,SP,A16,   *
;*                           A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28, *
;*                           A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23,B24, *
;*                           B25,B26,B27,B28,B29,B30,B31                      *
;******************************************************************************
_interrupt4:

	.map	i_data_ptr/B10
	.map	x_mag_img/A6
	.map	N/B12
	.map	idx_512/A4
	.map	W_ptr/A5
	.map	tmp/B4
	.map	idx_ptr/B11
	.map	scale/A13
	.map	idx_1024/B5
	.map	equ/B0
	.map	equ'/A0
	.map	scale_ptr/A6
	.map	out_data/A11
	.map	idx/A3
	.map	idx'/B4
	.map	img/A5
	.map	output/A3
	.map	in_data/B4
	.map	x_ptr/A10
	.map	ch1_f/A6
	.map	ch1_f'/A4
	.map	ch1/A12
	.map	ch1'/A4
	.map	x_mag_real/A4
	.map	ch2/A7

;** --------------------------------------------------------------------------*
; _interrupt4:  .proc IRP
; 		.reg	ch1, ch2, ch1_f, in_data, out_data, output
; 		.reg	idx_1024, idx_512
; 		.reg    W,W_ptr,x,x_ptr,x_mag,x_mag_ptr
; 		.reg	scale,scale_ptr,i_data_ptr,idx,idx_ptr
;         .reg	x_mag_real,x_mag_img,img,equ,N,tmp,z,mask
           MVKL    .S2     _idx,idx_ptr      ; |56|  current counter 
           MVKH    .S2     _idx,idx_ptr      ; |57| 
           LDH     .D2T1   *idx_ptr,idx      ; |70| 
           MVKL    .S1     _scale,scale_ptr  ; |50|  scale factor 
           MVKL    .S1     _x,x_ptr          ; |47|  input buffer 
           MVK     .S2     0x800,idx_1024    ; |66| 
           MVKL    .S1     _W,W_ptr          ; |44|  twiddle factors 

           CMPEQ   .L2X    idx,idx_1024,equ  ; |90|  (1) check 1024
||         MVKL    .S2     0x1d00230,in_data ; |59|  base address for input
||         MVK     .S1     0x400,idx_512     ; |67| 

   [!equ]  B       .S2     $C$L1             ; |91| 
||         MVKL    .S1     0x1d0022c,out_data ; |62|  base address for output

           MVKH    .S2     0x1d00230,in_data ; |60| 
||         MVKH    .S1     _scale,scale_ptr  ; |51| 

$C$DW$2	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$2, DW_AT_low_pc(0x00)
	.dwattr $C$DW$2, DW_AT_name("_cfftr2_dit")
	.dwattr $C$DW$2, DW_AT_TI_call

   [ equ]  CALL    .S2     _cfftr2_dit       ; |94| 
||         LDH     .D1T1   *scale_ptr,scale  ; |71| 
||         MVKH    .S1     _x,x_ptr          ; |48| 

           LDH     .D2T1   *in_data,ch1      ; |76|  read in ch1
||         MVKH    .S1     _W,W_ptr          ; |45| 

           CMPEQ   .L1     idx,idx_512,equ'  ; |107|  (5) check if 512
||         MVKL    .S2     _i_data,i_data_ptr ; |53|  bitrev() index vec 
||         MV      .D1     A4,A6             ; |48| 
||         MV      .L2X    A4,B12            ; |48| 
||         MVKH    .S1     0x1d0022c,out_data ; |63| 

   [!equ]  MVK     .S1     0xffff8001,ch2    ; |115|  output all 1s
|| [ equ]  MVK     .L1     0x1,equ'          ; |115| nullify predicate
||         MV      .L2X    W_ptr,B4          ; |94| 
||         MV      .D1     x_ptr,A4          ; |94| 
||         MVKH    .S2     _i_data,i_data_ptr ; |54| 

           ; BRANCHCC OCCURS {$C$L1}         ; |91| 
;** --------------------------------------------------------------------------*
; 		.call	_cfftr2_dit(x_ptr,W_ptr,N)	; (3) call fft function
; 		.call	_bitrev(x_ptr,i_data_ptr,N)
           ADDKPC  .S2     $C$RL0,B3,1       ; |94| 
$C$RL0:    ; CALL OCCURS {_cfftr2_dit} {0}   ; |94| 
;** --------------------------------------------------------------------------*
$C$DW$3	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$3, DW_AT_low_pc(0x00)
	.dwattr $C$DW$3, DW_AT_name("_bitrev")
	.dwattr $C$DW$3, DW_AT_TI_call

           CALLP   .S2     _bitrev,B3
||         MV      .L1     x_ptr,A4          ; |95| 
||         MV      .L2     i_data_ptr,B4     ; |95| 
||         MV      .S1X    N,A6              ; |95| 

$C$RL1:    ; CALL OCCURS {_bitrev} {0}       ; |95| 
;** --------------------------------------------------------------------------*

           B       .S1     $C$L2             ; |96|  (4) set pulse
||         ZERO    .L1     idx               ; |93| 

           LDW     .D1T1   *+x_ptr[idx],x_mag_real ; |130|  (1) find magnitude
||         ADD     .L1     0x1,idx,img       ; |133|  load img value

           LDW     .D1T1   *+x_ptr[img],x_mag_img ; |134| 
           ZERO    .L1     ch2               ; |111| 
           ZERO    .L2     tmp               ; |143| 
           NOP             1
           ; BRANCH OCCURS {$C$L2}           ; |96| 
;** --------------------------------------------------------------------------*
$C$L1:    
   [ equ'] ZERO    .L1     ch2               ; |111| 
;** --------------------------------------------------------------------------*

           ADD     .L1     0x1,idx,img       ; |133|  load img value
||         LDW     .D1T1   *+x_ptr[idx],x_mag_real ; |130|  (1) find magnitude

           LDW     .D1T1   *+x_ptr[img],x_mag_img ; |134| 
           ZERO    .L2     tmp               ; |143| 
           NOP             2
;** --------------------------------------------------------------------------*
$C$L2:    
           MPYSP   .M1     x_mag_real,x_mag_real,x_mag_real ; |131| 
           MPYSP   .M1     x_mag_img,x_mag_img,x_mag_img ; |135| 
           NOP             3
           ADDSP   .L1     x_mag_real,x_mag_img,ch1_f' ; |137| 
           NOP             3

           MPYSP   .M1     ch1_f',scale,ch1_f ; |138|  (2) scale output
||         INTSP   .L1     ch1,ch1'          ; |141|  cast ch1 to float

           NOP             3

           SPINT   .L1     ch1_f,ch1_f       ; |149|  cast ch1 to float
||         STW     .D1T1   ch1',*+x_ptr[idx] ; |142|  (4) store new value

           STW     .D1T2   tmp,*+x_ptr[img]  ; |144|  set imginary to 0
           ADD     .L2X    0x2,idx,idx'      ; |146|  (5) update index
           STH     .D2T2   idx',*idx_ptr     ; |147| 
           PACK2   .L1     ch1_f,ch2,output  ; |150|  (6) output data
	.dwpsn	file "../ASM_interrupt4.sa",line 156,column 1,is_stmt
           STW     .D1T1   output,*out_data  ; |151| 
	.dwattr $C$DW$1, DW_AT_TI_end_file("../ASM_interrupt4.sa")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0x9c)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$1

	.clearmap


; 	   .endproc IRP
		b IRP
		nop 5
;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_cfftr2_dit
	.global	_bitrev

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$4	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg1]
$C$DW$6	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg2]
$C$DW$7	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_reg3]
$C$DW$8	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_reg4]
$C$DW$9	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg5]
$C$DW$10	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg6]
$C$DW$11	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg7]
$C$DW$12	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg8]
$C$DW$13	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg9]
$C$DW$14	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg10]
$C$DW$15	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg11]
$C$DW$16	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg12]
$C$DW$17	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg13]
$C$DW$18	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg14]
$C$DW$19	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_reg15]
$C$DW$20	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_reg16]
$C$DW$21	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg17]
$C$DW$22	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg18]
$C$DW$23	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg19]
$C$DW$24	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg20]
$C$DW$25	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg21]
$C$DW$26	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg22]
$C$DW$27	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg23]
$C$DW$28	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg24]
$C$DW$29	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg25]
$C$DW$30	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg26]
$C$DW$31	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg27]
$C$DW$32	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg28]
$C$DW$33	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg29]
$C$DW$34	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg30]
$C$DW$35	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg31]
$C$DW$36	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_regx 0x20]
$C$DW$37	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_regx 0x21]
$C$DW$38	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_regx 0x22]
$C$DW$39	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_regx 0x23]
$C$DW$40	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_regx 0x24]
$C$DW$41	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_regx 0x25]
$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_regx 0x26]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_regx 0x27]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_regx 0x28]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_regx 0x29]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_regx 0x30]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_regx 0x31]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_regx 0x32]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_regx 0x33]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_regx 0x34]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_regx 0x35]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_regx 0x36]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_regx 0x37]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_regx 0x38]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x39]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x40]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x41]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x42]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x43]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x44]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x45]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x46]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x47]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x48]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x49]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x50]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x51]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x52]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x53]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x54]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x55]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x56]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x57]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x58]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x59]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x60]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x61]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x62]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x63]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x64]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x65]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x66]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x67]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x68]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x69]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x70]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x71]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x72]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x73]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x74]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x75]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x76]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x77]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x78]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x79]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

