################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../linker_dsp.cmd 

SA_SRCS += \
../ASM_init.sa \
../ASM_interrupt4.sa 

ASM_SRCS += \
../67-cfftr2.asm \
../bitrevf.asm \
../vectors_intr.asm 

C_SRCS += \
../L138_aic3106_init.c \
../fft.c 

OBJS += \
./67-cfftr2.obj \
./ASM_init.obj \
./ASM_interrupt4.obj \
./L138_aic3106_init.obj \
./bitrevf.obj \
./fft.obj \
./vectors_intr.obj 

ASM_DEPS += \
./67-cfftr2.pp \
./bitrevf.pp \
./vectors_intr.pp 

C_DEPS += \
./L138_aic3106_init.pp \
./fft.pp 

SA_DEPS += \
./ASM_init.pp \
./ASM_interrupt4.pp 

C_DEPS__QUOTED += \
"L138_aic3106_init.pp" \
"fft.pp" 

OBJS__QUOTED += \
"67-cfftr2.obj" \
"ASM_init.obj" \
"ASM_interrupt4.obj" \
"L138_aic3106_init.obj" \
"bitrevf.obj" \
"fft.obj" \
"vectors_intr.obj" 

SA_DEPS__QUOTED += \
"ASM_init.pp" \
"ASM_interrupt4.pp" 

ASM_DEPS__QUOTED += \
"67-cfftr2.pp" \
"bitrevf.pp" \
"vectors_intr.pp" 

ASM_SRCS__QUOTED += \
"../67-cfftr2.asm" \
"../bitrevf.asm" \
"../vectors_intr.asm" 

SA_SRCS__QUOTED += \
"../ASM_init.sa" \
"../ASM_interrupt4.sa" 

C_SRCS__QUOTED += \
"../L138_aic3106_init.c" \
"../fft.c" 


