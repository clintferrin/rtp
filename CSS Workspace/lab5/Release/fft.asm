;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Wed Mar 28 13:56:05 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=on --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Enabled with threshold = 0                           *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../fft.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("E:\Documents\dev\rtp\CSS Workspace\lab5\Release")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("L138_initialise_intr")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_L138_initialise_intr")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$23)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$24)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$24)
	.dwendtag $C$DW$1


$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("pow")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_pow")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$17)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$17)
	.dwendtag $C$DW$5


$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("sin")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_sin")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$17)
	.dwendtag $C$DW$8


$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("cos")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_cos")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$17)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ASM_init")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ASM_init")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("bitrev")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_bitrev")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$29)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$30)
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$10)
	.dwendtag $C$DW$13

	.global	_codec_data
_codec_data:	.usect	".far",4,4
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("codec_data")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_codec_data")
	.dwattr $C$DW$17, DW_AT_location[DW_OP_addr _codec_data]
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("W")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_W")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("x")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("scale")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_scale")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("i_data")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_i_data")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("idx")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_idx")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
;	C:\ccsv5\tools\compiler\c6000_7.4.4\bin\opt6x.exe C:\\Users\\A02179~1\\AppData\\Local\\Temp\\086562 C:\\Users\\A02179~1\\AppData\\Local\\Temp\\086564 
	.sect	".text"
	.clink
	.global	_set_init_values

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("set_init_values")
	.dwattr $C$DW$23, DW_AT_low_pc(_set_init_values)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_set_init_values")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x37)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(0x18)
	.dwattr $C$DW$23, DW_AT_frame_base[DW_OP_breg31 24]
	.dwattr $C$DW$23, DW_AT_TI_skeletal
	.dwpsn	file "../fft.c",line 55,column 72,is_stmt,address _set_init_values
$C$DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg4]
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("scale")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_scale")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg20]
$C$DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_name("idx")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_idx")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg6]
$C$DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg22]

;******************************************************************************
;* FUNCTION NAME: set_init_values                                             *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,SP,A16,A17,A18,A19,  *
;*                           A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30,A31, *
;*                           B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26,B27, *
;*                           B28,B29,B30,B31                                  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,DP,SP,A16,A17,A18,   *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 24 Save = 24 byte                  *
;******************************************************************************
_set_init_values:
;** --------------------------------------------------------------------------*
           STW     .D2T2   B10,*SP--(8)      ; |55| 

           STDW    .D2T1   A13:A12,*SP--     ; |55| 
||         ZERO    .L2     B5
||         ZERO    .L1     A5
||         MV      .S1X    B4,A12            ; |55| 

           MV      .L1X    B3,A13            ; |55| 
||         MVKH    .S2     0xc0000000,B5
||         MVKH    .S1     0x40240000,A5
||         MV      .L2     B6,B10            ; |55| 

$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_pow")
	.dwattr $C$DW$28, DW_AT_TI_call

           CALLP   .S2     _pow,B3
||         STDW    .D2T1   A11:A10,*SP--     ; |55| 
||         MV      .L1     A4,A11            ; |55| 
||         MV      .S1     A6,A10            ; |55| 
||         ZERO    .D1     A4                ; |57| 
||         ZERO    .L2     B4                ; |57| 

$C$RL0:    ; CALL OCCURS {_pow} {0}          ; |57| 
;** --------------------------------------------------------------------------*

           CMPGT   .L2     B10,0,B0          ; |59| 
||         DPSP    .L1     A5:A4,A3          ; |57| 
||         MV      .S2X    A12,B4            ; |55| 
||         ZERO    .S1     A4                ; |58| 
||         ZERO    .D1     A5                ; |58| 

   [!B0]   BNOP    .S1     $C$L4,2           ; |59| 
|| [ B0]   MVC     .S2     B10,ILC

           STW     .D2T1   A3,*B4            ; |57| 
           MV      .L1     A11,A3            ; |55| 
           STH     .D1T1   A4,*A10           ; |58| 
           ; BRANCHCC OCCURS {$C$L4}         ; |59| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../fft.c
;*      Loop source line                 : 59
;*      Loop opening brace source line   : 59
;*      Loop closing brace source line   : 61
;*      Loop Unroll Multiple             : 2x
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 16384                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             1*       1*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        0     
;*      Bound(.L .S .D .LS .LSD)     1*       0     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1 (after unrolling)
;*----------------------------------------------------------------------------*
$C$L1:    ; PIPED LOOP PROLOG
	.dwpsn	file "../fft.c",line 59,column 0,is_stmt
           SPLOOP  1       ;2                ; (P) 
;** --------------------------------------------------------------------------*
$C$L2:    ; PIPED LOOP KERNEL
$C$DW$L$_set_init_values$4$B:
           STNDW   .D1T1   A5:A4,*A3++(8)    ; |60| (P) <0,0> 
	.dwpsn	file "../fft.c",line 61,column 0,is_stmt
           SPKERNEL 1,0
$C$DW$L$_set_init_values$4$E:
;** --------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L4:    

           LDDW    .D2T1   *++SP,A11:A10     ; |62| 
||         MV      .L2X    A13,B3            ; |62| 

$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x04)
	.dwattr $C$DW$29, DW_AT_TI_return

           LDDW    .D2T1   *++SP,A13:A12     ; |62| 
||         RET     .S2     B3                ; |62| 

           LDW     .D2T2   *++SP(8),B10      ; |62| 
	.dwpsn	file "../fft.c",line 62,column 1,is_stmt
           NOP             4
           ; BRANCH OCCURS {B3}              ; |62| 

$C$DW$30	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$30, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L2:1:1522266965")
	.dwattr $C$DW$30, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0x3b)
	.dwattr $C$DW$30, DW_AT_TI_end_line(0x3d)
$C$DW$31	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$31, DW_AT_low_pc($C$DW$L$_set_init_values$4$B)
	.dwattr $C$DW$31, DW_AT_high_pc($C$DW$L$_set_init_values$4$E)
	.dwendtag $C$DW$30

	.dwattr $C$DW$23, DW_AT_TI_end_file("../fft.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x3e)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$23

	.sect	".text"
	.clink
	.global	_print_arr

$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("print_arr")
	.dwattr $C$DW$32, DW_AT_low_pc(_print_arr)
	.dwattr $C$DW$32, DW_AT_high_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_print_arr")
	.dwattr $C$DW$32, DW_AT_external
	.dwattr $C$DW$32, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$32, DW_AT_TI_begin_line(0x30)
	.dwattr $C$DW$32, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$32, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$32, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$32, DW_AT_TI_skeletal
	.dwpsn	file "../fft.c",line 48,column 38,is_stmt,address _print_arr
$C$DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_name("arr")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_arr")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg4]
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: print_arr                                                   *
;*                                                                            *
;*   Regs Modified     : A3,A4,B0                                             *
;*   Regs Used         : A3,A4,B0,B3,B4                                       *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_print_arr:
;** --------------------------------------------------------------------------*

           CMPGT   .L2     B4,0,B0           ; |50| 
||         MVK     .L1     0x1,A3

   [!B0]   BNOP    .S1     $C$L8,5           ; |50| 
|| [ B0]   MVC     .S2     B4,ILC

           ; BRANCHCC OCCURS {$C$L8}         ; |50| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../fft.c
;*      Loop source line                 : 50
;*      Loop opening brace source line   : 50
;*      Loop closing brace source line   : 52
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             1*       0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             0        0     
;*      Bound(.L .S .D .LS .LSD)     1*       0     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L5:    ; PIPED LOOP PROLOG
	.dwpsn	file "../fft.c",line 50,column 0,is_stmt
           SPLOOP  1       ;2                ; (P) 
;** --------------------------------------------------------------------------*
$C$L6:    ; PIPED LOOP KERNEL
$C$DW$L$_print_arr$3$B:
           STH     .D1T1   A3,*A4++          ; |51| (P) <0,0> 
	.dwpsn	file "../fft.c",line 52,column 0,is_stmt
           SPKERNEL 1,0
$C$DW$L$_print_arr$3$E:
;** --------------------------------------------------------------------------*
$C$L7:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L8:    
	.dwpsn	file "../fft.c",line 53,column 1,is_stmt
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |53| 
           ; BRANCH OCCURS {B3}              ; |53| 

$C$DW$36	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$36, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L6:1:1522266965")
	.dwattr $C$DW$36, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$36, DW_AT_TI_begin_line(0x32)
	.dwattr $C$DW$36, DW_AT_TI_end_line(0x34)
$C$DW$37	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$37, DW_AT_low_pc($C$DW$L$_print_arr$3$B)
	.dwattr $C$DW$37, DW_AT_high_pc($C$DW$L$_print_arr$3$E)
	.dwendtag $C$DW$36

	.dwattr $C$DW$32, DW_AT_TI_end_file("../fft.c")
	.dwattr $C$DW$32, DW_AT_TI_end_line(0x35)
	.dwattr $C$DW$32, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$32

	.sect	".text"
	.clink
	.global	_calc_twid

$C$DW$38	.dwtag  DW_TAG_subprogram, DW_AT_name("calc_twid")
	.dwattr $C$DW$38, DW_AT_low_pc(_calc_twid)
	.dwattr $C$DW$38, DW_AT_high_pc(0x00)
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_calc_twid")
	.dwattr $C$DW$38, DW_AT_external
	.dwattr $C$DW$38, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$38, DW_AT_TI_begin_line(0x40)
	.dwattr $C$DW$38, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$38, DW_AT_TI_max_frame_size(0x28)
	.dwattr $C$DW$38, DW_AT_frame_base[DW_OP_breg31 40]
	.dwattr $C$DW$38, DW_AT_TI_skeletal
	.dwpsn	file "../fft.c",line 64,column 39,is_stmt,address _calc_twid
$C$DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("twid")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_twid")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg4]
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: calc_twid                                                   *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,*
;*                           A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27,A28, *
;*                           A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23,B24, *
;*                           B25,B26,B27,B28,B29,B30,B31                      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,DP,SP, *
;*                           A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,A26,A27, *
;*                           A28,A29,A30,A31,B16,B17,B18,B19,B20,B21,B22,B23, *
;*                           B24,B25,B26,B27,B28,B29,B30,B31                  *
;*   Local Frame Size  : 0 Args + 0 Auto + 40 Save = 40 byte                  *
;******************************************************************************
_calc_twid:
;** --------------------------------------------------------------------------*

           CMPLT   .L2     B4,2,B0           ; |66| 
||         MV      .S2     B4,B6             ; |64| 
||         STW     .D2T1   A11,*SP--(8)      ; |64| 
||         MVKL    .S1     0x401921fb,A5

   [ B0]   B       .S1     $C$L10            ; |66| 
||         INTDP   .L2     B6,B5:B4
||         STDW    .D2T2   B11:B10,*SP--     ; |64| 
||         SHR     .S2     B6,1,B10

$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("__divd")
	.dwattr $C$DW$41, DW_AT_TI_call

   [!B0]   CALL    .S2     __divd
||         STDW    .D2T1   A15:A14,*SP--     ; |64| 
||         SUB     .L1     A4,8,A14
||         MVKL    .S1     0x54442d18,A4
||         ZERO    .D1     A15               ; |66| 
||         MV      .L2     B3,B11            ; |64| 

           MVKH    .S1     0x54442d18,A4
           STDW    .D2T1   A13:A12,*SP--     ; |64| 
           MVKH    .S1     0x401921fb,A5
           STW     .D2T1   A10,*SP--(8)      ; |64| 
           ; BRANCHCC OCCURS {$C$L10}        ; |66| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL1,B3,0
$C$RL1:    ; CALL OCCURS {__divd} {0} 
;** --------------------------------------------------------------------------*

           MV      .S1     A4,A10
||         MV      .D1     A5,A11
||         INTDP   .L1     A15,A5:A4         ; |67| 

           NOP             4
	.dwpsn	file "../fft.c",line 66,column 0,is_stmt
           MPYDP   .M1     A11:A10,A5:A4,A13:A12 ; |67| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*      Disqualified loop: Loop contains non-pipelinable instructions
;*      Disqualified loop: Loop contains a call
;*      Disqualified loop: Loop contains non-pipelinable instructions
;*----------------------------------------------------------------------------*
$C$L9:    
$C$DW$L$_calc_twid$4$B:
           NOP             4
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_name("_cos")
	.dwattr $C$DW$42, DW_AT_TI_call
           CALL    .S1     _cos              ; |67| 
           ADDKPC  .S2     $C$RL2,B3,3       ; |67| 

           MV      .L1     A13,A5            ; |67| 
||         MV      .S1     A12,A4            ; |67| 

$C$RL2:    ; CALL OCCURS {_cos} {0}          ; |67| 
$C$DW$L$_calc_twid$4$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_calc_twid$5$B:
           DPSP    .L1     A5:A4,A3          ; |67| 
           MV      .L1     A13,A5            ; |68| 
           MV      .L1     A12,A4            ; |68| 
           NOP             1
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_sin")
	.dwattr $C$DW$43, DW_AT_TI_call

           CALLP   .S2     _sin,B3
||         STW     .D1T1   A3,*++A14(8)      ; |67| 

$C$RL3:    ; CALL OCCURS {_sin} {0}          ; |68| 
$C$DW$L$_calc_twid$5$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_calc_twid$6$B:

           ADD     .L1     1,A15,A15         ; |66| 
||         SUB     .S1X    B10,1,A0          ; |66| 
||         MV      .L2X    A4,B4             ; |68| 

   [ A0]   B       .S1     $C$L9             ; |66| 
||         MV      .L2X    A5,B5             ; |68| 
||         INTDP   .L1     A15,A5:A4         ; |67| 

           DPSP    .L2     B5:B4,B5          ; |68| 
           SUB     .L2     B10,1,B10         ; |66| 
           NOP             2
	.dwpsn	file "../fft.c",line 69,column 0,is_stmt

           MPYDP   .M1     A11:A10,A5:A4,A13:A12 ; |67| 
||         STW     .D1T2   B5,*+A14(4)       ; |68| 

           ; BRANCHCC OCCURS {$C$L9}         ; |66| 
$C$DW$L$_calc_twid$6$E:
;** --------------------------------------------------------------------------*
$C$L10:    
           LDW     .D2T1   *++SP(8),A10      ; |70| 
           MV      .L2     B11,B3            ; |70| 
           NOP             3
           LDDW    .D2T1   *++SP,A13:A12     ; |70| 
           LDDW    .D2T1   *++SP,A15:A14     ; |70| 
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x04)
	.dwattr $C$DW$44, DW_AT_TI_return

           LDDW    .D2T2   *++SP,B11:B10     ; |70| 
||         RET     .S2     B3                ; |70| 

           LDW     .D2T1   *++SP(8),A11      ; |70| 
	.dwpsn	file "../fft.c",line 70,column 1,is_stmt
           NOP             4
           ; BRANCH OCCURS {B3}              ; |70| 

$C$DW$45	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$45, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L9:1:1522266965")
	.dwattr $C$DW$45, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$45, DW_AT_TI_begin_line(0x42)
	.dwattr $C$DW$45, DW_AT_TI_end_line(0x45)
$C$DW$46	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$46, DW_AT_low_pc($C$DW$L$_calc_twid$4$B)
	.dwattr $C$DW$46, DW_AT_high_pc($C$DW$L$_calc_twid$4$E)
$C$DW$47	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$47, DW_AT_low_pc($C$DW$L$_calc_twid$5$B)
	.dwattr $C$DW$47, DW_AT_high_pc($C$DW$L$_calc_twid$5$E)
$C$DW$48	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$48, DW_AT_low_pc($C$DW$L$_calc_twid$6$B)
	.dwattr $C$DW$48, DW_AT_high_pc($C$DW$L$_calc_twid$6$E)
	.dwendtag $C$DW$45

	.dwattr $C$DW$38, DW_AT_TI_end_file("../fft.c")
	.dwattr $C$DW$38, DW_AT_TI_end_line(0x46)
	.dwattr $C$DW$38, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$38

	.sect	".text"
	.clink
	.global	_digitrev_index

$C$DW$49	.dwtag  DW_TAG_subprogram, DW_AT_name("digitrev_index")
	.dwattr $C$DW$49, DW_AT_low_pc(_digitrev_index)
	.dwattr $C$DW$49, DW_AT_high_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_digitrev_index")
	.dwattr $C$DW$49, DW_AT_external
	.dwattr $C$DW$49, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$49, DW_AT_TI_begin_line(0x51)
	.dwattr $C$DW$49, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$49, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$49, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$49, DW_AT_TI_skeletal
	.dwpsn	file "../fft.c",line 81,column 52,is_stmt,address _digitrev_index
$C$DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg4]
$C$DW$51	.dwtag  DW_TAG_formal_parameter, DW_AT_name("n")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg20]
$C$DW$52	.dwtag  DW_TAG_formal_parameter, DW_AT_name("radix")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_radix")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg6]

;******************************************************************************
;* FUNCTION NAME: digitrev_index                                              *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,A16,A17,A18,A19,A20,A22,A23,B30,B31     *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A22,A23,B30,  *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_digitrev_index:
;** --------------------------------------------------------------------------*

           LMBD    .L2     1,B4,B5
||         MVK     .S2     31,B7

           SUB     .L2     B7,B5,B5

           CMPLT   .L2     B4,2,B0           ; |88| 
||         EXT     .S2     B5,16,16,B4
||         MV      .D2X    A6,B6             ; |81| 

   [ B0]   ZERO    .L2     B4                ; |86| 
||         EXT     .S2     B6,15,16,B5       ; |98| 

           SHR     .S2     B4,B5,B7          ; |98| 

           EXT     .S2     B7,16,16,B8       ; |98| 
||         SUB     .L2     B5,1,B7           ; |98| 

           AND     .L2     B5,B4,B5          ; |98| 
||         SHL     .S2     B8,B7,B4          ; |98| 

           ADD     .L2     B5,B4,B4          ; |98| 
||         MVK     .L1     1,A3              ; |98| 

           EXT     .S2     B4,16,16,B4       ; |98| 
           SHL     .S2X    A3,B4,B4          ; |98| 
           MV      .L1     A4,A7             ; |81| 
           MV      .L1X    B4,A3             ; |98| Define a twin register

           EXT     .S1     A3,16,16,A5       ; |98| 
||         MV      .L1     A4,A17            ; |81| 
||         ZERO    .D1     A4                ; |100| 

$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x04)
	.dwattr $C$DW$53, DW_AT_name("__divi")
	.dwattr $C$DW$53, DW_AT_TI_call

           MV      .L2     B3,B9             ; |81| 
||         CALLP   .S2     __divi,B3
||         STH     .D1T1   A4,*A7            ; |100| 
||         MV      .L1     A5,A4             ; |101| 
||         MV      .D2     B6,B4             ; |101| 
||         SUB     .S1     A6,1,A16

$C$RL4:    ; CALL OCCURS {__divi} {0}        ; |101| 
;** --------------------------------------------------------------------------*

           CMPLT   .L1     A5,3,A0           ; |101| 
||         MV      .S1     A4,A20
||         ADD     .D1     1,A4,A3           ; |101| 

           EXT     .S1     A20,16,16,A4
|| [ A0]   B       .S2     $C$L14            ; |101| 

           MPYLI   .M1     A4,A16,A19:A18
           SUB     .L2X    A5,2,B7           ; |102| 
           NOP             2
   [!A0]   CMPLT   .L1     A18,A3,A0         ; |103| 
           ; BRANCHCC OCCURS {$C$L14}        ; |101| 
;** --------------------------------------------------------------------------*

   [!A0]   BNOP    .S1     $C$L13,2          ; |103| 
||         SUB     .L1     A3,1,A6           ; |102| 
||         MV      .D1     A20,A4            ; |103| 

	.dwpsn	file "../fft.c",line 101,column 0,is_stmt
           STH     .D1T1   A6,*++A17         ; |102| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L11
;** --------------------------------------------------------------------------*
$C$L11:    
$C$DW$L$_digitrev_index$4$B:
           NOP             2
           ; BRANCHCC OCCURS {$C$L13}        ; |103| 
$C$DW$L$_digitrev_index$4$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_digitrev_index$5$B:
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("__divi")
	.dwattr $C$DW$54, DW_AT_TI_call
           CALL    .S1     __divi            ; |103| 
           EXT     .S1     A4,16,16,A6
           MPYLI   .M1     A6,A16,A9:A8
           MV      .L2     B6,B4             ; |103| 
	.dwpsn	file "../fft.c",line 103,column 0,is_stmt
           NOP             1
$C$DW$L$_digitrev_index$5$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*      Disqualified loop: Loop contains non-pipelinable instructions
;*----------------------------------------------------------------------------*
$C$L12:    
$C$DW$L$_digitrev_index$6$B:
	.dwpsn	file "../fft.c",line 104,column 0,is_stmt
           ADDKPC  .S2     $C$RL5,B3,0       ; |103| 
$C$RL5:    ; CALL OCCURS {__divi} {0}        ; |103| 
$C$DW$L$_digitrev_index$6$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_digitrev_index$7$B:
           EXT     .S1     A4,16,16,A6       ; |103| 
           MPYLI   .M1     A6,A16,A23:A22    ; |103| 
           SUB     .L1     A3,A8,A3          ; |104| 
           NOP             2
           CMPLT   .L1     A22,A3,A0         ; |103| 
   [ A0]   B       .S1     $C$L12            ; |103| 
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_name("__divi")
	.dwattr $C$DW$55, DW_AT_TI_call
   [ A0]   CALL    .S1     __divi            ; |103| 
           MV      .L1     A22,A8            ; |103| 
   [ A0]   MV      .L2     B6,B4             ; |103| 
           NOP             2
           ; BRANCHCC OCCURS {$C$L12}        ; |103| 
$C$DW$L$_digitrev_index$7$E:
;** --------------------------------------------------------------------------*
$C$L13:    
$C$DW$L$_digitrev_index$8$B:

           SUB     .L1X    B7,1,A0           ; |101| 
||         ADD     .S1     A4,A3,A3          ; |105| 
||         SUB     .L2     B7,1,B7           ; |101| 

   [ A0]   B       .S1     $C$L11            ; |101| 
||         MV      .D1     A0,A1             ; guard predicate rewrite
|| [ A0]   CMPLT   .L1     A18,A3,A0         ; |103| 

   [!A1]   MVK     .L1     0x1,A0            ; |103| nullify predicate
|| [ A1]   SUB     .S1     A3,1,A6           ; |102| 
|| [ A1]   MV      .D1     A20,A4            ; |103| 

	.dwpsn	file "../fft.c",line 106,column 0,is_stmt

   [!A0]   BNOP    .S1     $C$L13,3          ; |103| 
|| [ A1]   STH     .D1T1   A6,*++A17         ; |102| 

           ; BRANCHCC OCCURS {$C$L11}        ; |101| 
$C$DW$L$_digitrev_index$8$E:
;** --------------------------------------------------------------------------*
$C$L14:    
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_return
           RETNOP  .S2     B9,3              ; |108| 

           ADDAH   .D1     A7,A5,A4          ; |107| 
||         SUB     .L1     A5,1,A3           ; |107| 

	.dwpsn	file "../fft.c",line 108,column 1,is_stmt
           STH     .D1T1   A3,*-A4(2)        ; |107| 
           ; BRANCH OCCURS {B9}              ; |108| 

$C$DW$57	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$57, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L11:1:1522266965")
	.dwattr $C$DW$57, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$57, DW_AT_TI_begin_line(0x65)
	.dwattr $C$DW$57, DW_AT_TI_end_line(0x6a)
$C$DW$58	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$58, DW_AT_low_pc($C$DW$L$_digitrev_index$4$B)
	.dwattr $C$DW$58, DW_AT_high_pc($C$DW$L$_digitrev_index$4$E)
$C$DW$59	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$59, DW_AT_low_pc($C$DW$L$_digitrev_index$5$B)
	.dwattr $C$DW$59, DW_AT_high_pc($C$DW$L$_digitrev_index$5$E)
$C$DW$60	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$60, DW_AT_low_pc($C$DW$L$_digitrev_index$8$B)
	.dwattr $C$DW$60, DW_AT_high_pc($C$DW$L$_digitrev_index$8$E)

$C$DW$61	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$61, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L12:2:1522266965")
	.dwattr $C$DW$61, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$61, DW_AT_TI_begin_line(0x67)
	.dwattr $C$DW$61, DW_AT_TI_end_line(0x68)
$C$DW$62	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$62, DW_AT_low_pc($C$DW$L$_digitrev_index$6$B)
	.dwattr $C$DW$62, DW_AT_high_pc($C$DW$L$_digitrev_index$6$E)
$C$DW$63	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$63, DW_AT_low_pc($C$DW$L$_digitrev_index$7$B)
	.dwattr $C$DW$63, DW_AT_high_pc($C$DW$L$_digitrev_index$7$E)
	.dwendtag $C$DW$61

	.dwendtag $C$DW$57

	.dwattr $C$DW$49, DW_AT_TI_end_file("../fft.c")
	.dwattr $C$DW$49, DW_AT_TI_end_line(0x6c)
	.dwattr $C$DW$49, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$49

	.sect	".text"
	.clink
	.global	_main

$C$DW$64	.dwtag  DW_TAG_subprogram, DW_AT_name("main")
	.dwattr $C$DW$64, DW_AT_low_pc(_main)
	.dwattr $C$DW$64, DW_AT_high_pc(0x00)
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_main")
	.dwattr $C$DW$64, DW_AT_external
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$64, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$64, DW_AT_TI_begin_line(0x1f)
	.dwattr $C$DW$64, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$64, DW_AT_TI_max_frame_size(0x410)
	.dwattr $C$DW$64, DW_AT_frame_base[DW_OP_breg31 1040]
	.dwattr $C$DW$64, DW_AT_TI_skeletal
	.dwpsn	file "../fft.c",line 31,column 16,is_stmt,address _main

;******************************************************************************
;* FUNCTION NAME: main                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,B0,B1,B2,B3,B4,*
;*                           B5,B6,B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,*
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 1028 Auto + 8 Save = 1036 byte              *
;******************************************************************************
_main:
;** --------------------------------------------------------------------------*
           ADDK    .S2     -1040,SP          ; |31| 
           STW     .D2T1   A10,*+SP(1036)    ; |31| 
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_name("_ASM_init")
	.dwattr $C$DW$65, DW_AT_TI_call

           CALLP   .S2     _ASM_init,B3
||         STW     .D2T1   A11,*+SP(1040)    ; |31| 

$C$RL6:    ; CALL OCCURS {_ASM_init} {0}     ; |33| 
;** --------------------------------------------------------------------------*

           MVKL    .S2     _scale,B4
||         MVKL    .S1     _x,A4

           MVKL    .S1     _idx,A6
||         MVK     .S2     0x400,B6          ; |34| 

           MVKH    .S2     _scale,B4
||         MVKH    .S1     _x,A4

$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("_set_init_values")
	.dwattr $C$DW$66, DW_AT_TI_call

           CALLP   .S2     _set_init_values,B3
||         MVKH    .S1     _idx,A6

$C$RL7:    ; CALL OCCURS {_set_init_values} {0}  ; |34| 
           MVKL    .S1     _W,A10

           MVKH    .S1     _W,A10
||         MVK     .S2     0x400,B4          ; |37| 

$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_calc_twid")
	.dwattr $C$DW$67, DW_AT_TI_call

           CALLP   .S2     _calc_twid,B3
||         MV      .L1     A10,A4            ; |37| 

$C$RL8:    ; CALL OCCURS {_calc_twid} {0}    ; |37| 
           MVK     .S2     0x200,B4          ; |38| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("_digitrev_index")
	.dwattr $C$DW$68, DW_AT_TI_call

           CALLP   .S2     _digitrev_index,B3
||         MVK     .L1     0x2,A6            ; |38| 
||         ADD     .S1X    8,SP,A4           ; |38| 

$C$RL9:    ; CALL OCCURS {_digitrev_index} {0}  ; |38| 
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_bitrev")
	.dwattr $C$DW$69, DW_AT_TI_call

           CALLP   .S2     _bitrev,B3
||         MV      .L1     A10,A4            ; |37| 
||         ADD     .L2     8,SP,B4           ; |39| 
||         MVK     .S1     0x200,A6          ; |39| 

$C$RL10:   ; CALL OCCURS {_bitrev} {0}       ; |39| 
           MVKL    .S2     _i_data,B5
           MVKH    .S2     _i_data,B5
           MVK     .S2     0x400,B4          ; |42| 
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_name("_digitrev_index")
	.dwattr $C$DW$70, DW_AT_TI_call

           CALLP   .S2     _digitrev_index,B3
||         MV      .L1X    B5,A4             ; |42| 
||         MVK     .S1     0x2,A6            ; |42| 

$C$RL11:   ; CALL OCCURS {_digitrev_index} {0}  ; |42| 
	.dwpsn	file "../fft.c",line 45,column 0,is_stmt
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("_L138_initialise_intr")
	.dwattr $C$DW$71, DW_AT_TI_call

           CALLP   .S2     _L138_initialise_intr,B3
||         MVK     .S1     0x1f40,A4         ; |44| 
||         ZERO    .L2     B4                ; |44| 
||         ZERO    .L1     A6                ; |44| 

$C$RL12:   ; CALL OCCURS {_L138_initialise_intr} {0}  ; |44| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Bad loop structure
;*----------------------------------------------------------------------------*
$C$L15:    
$C$DW$L$_main$3$B:
	.dwpsn	file "../fft.c",line 46,column 1,is_stmt
           BNOP    .S1     $C$L15,5          ; |45| 
           ; BRANCH OCCURS {$C$L15}          ; |45| 
$C$DW$L$_main$3$E:

$C$DW$72	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$72, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L15:1:1522266965")
	.dwattr $C$DW$72, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$72, DW_AT_TI_begin_line(0x2d)
	.dwattr $C$DW$72, DW_AT_TI_end_line(0x2e)
$C$DW$73	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$73, DW_AT_low_pc($C$DW$L$_main$3$B)
	.dwattr $C$DW$73, DW_AT_high_pc($C$DW$L$_main$3$E)
	.dwendtag $C$DW$72

	.dwattr $C$DW$64, DW_AT_TI_end_file("../fft.c")
	.dwattr $C$DW$64, DW_AT_TI_end_line(0x2e)
	.dwattr $C$DW$64, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$64

	.sect	".text"
	.clink
	.global	_find_x_mag

$C$DW$74	.dwtag  DW_TAG_subprogram, DW_AT_name("find_x_mag")
	.dwattr $C$DW$74, DW_AT_low_pc(_find_x_mag)
	.dwattr $C$DW$74, DW_AT_high_pc(0x00)
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_find_x_mag")
	.dwattr $C$DW$74, DW_AT_external
	.dwattr $C$DW$74, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$74, DW_AT_TI_begin_line(0x48)
	.dwattr $C$DW$74, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$74, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$74, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$74, DW_AT_TI_skeletal
	.dwpsn	file "../fft.c",line 72,column 67,is_stmt,address _find_x_mag
$C$DW$75	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg4]
$C$DW$76	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x_mag")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_x_mag")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg20]
$C$DW$77	.dwtag  DW_TAG_formal_parameter, DW_AT_name("scale")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_scale")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg6]
$C$DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_name("len")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg22]

;******************************************************************************
;* FUNCTION NAME: find_x_mag                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A3,A4,A5,A6,A7,B0,B1,B4,B5,B6,B7                  *
;*   Regs Used         : A0,A3,A4,A5,A6,A7,B0,B1,B3,B4,B5,B6,B7,DP,SP         *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_find_x_mag:
;** --------------------------------------------------------------------------*

           CMPGT   .L2     B6,0,B1           ; |74| 
||         SHL     .S2     B6,3,B7           ; |75| 
||         SHL     .S1X    B6,2,A3           ; |75| 
||         MV      .L1     A6,A5             ; |72| 

   [!B1]   B       .S1     $C$L22            ; |74| 
||         SUB     .L2X    B4,A4,B5          ; |75| 
||         SUB     .L1X    A4,B4,A6          ; |75| 
|| [ B1]   MV      .S2     B6,B0

           CMPGT   .L2     B7,B5,B5          ; |75| 
||         CMPGT   .L1     A3,A6,A3          ; |75| 
||         MV      .S1     A5,A6             ; |72| 
|| [ B1]   MV      .D1     A4,A5

   [ B1]   SUB     .L1     A4,8,A4

           AND     .L1X    A3,B5,A0          ; |75| 
|| [ B1]   MV      .L2     B4,B5

   [!B1]   MVK     .L1     0x1,A0            ; nullify predicate
   [!A0]   B       .S1     $C$L19            ; |75| 
           ; BRANCHCC OCCURS {$C$L22}        ; |74| 
;** --------------------------------------------------------------------------*
   [!A0]   MVC     .S2     B0,ILC
           NOP             4
           ; BRANCHCC OCCURS {$C$L19}        ; |75| 
;** --------------------------------------------------------------------------*

           SUB     .L2     B6,1,B0
||         MV      .L1     A6,A5
||         DINT                              ; interrupts off

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../fft.c
;*      Loop source line                 : 74
;*      Loop opening brace source line   : 74
;*      Loop closing brace source line   : 78
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 65536                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 28
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        1     
;*      .D units                     2*       2*    
;*      .M units                     2*       1     
;*      .X cross paths               1        0     
;*      .T address paths             2*       2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           1        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        1     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 28 Schedule found with 1 iterations in parallel
;*      Done
;*
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L16:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
$C$L17:    ; PIPED LOOP KERNEL
$C$DW$L$_find_x_mag$5$B:
	.dwpsn	file "../fft.c",line 74,column 0,is_stmt
           LDW     .D1T2   *++A4(8),B5       ; |75| <0,0>  ^ 
           NOP             4
           MPYSP   .M2     B5,B5,B5          ; |75| <0,5>  ^ 
           NOP             3
           STW     .D2T2   B5,*B4            ; |75| <0,9>  ^ 
           LDW     .D1T1   *+A4(4),A3        ; |76| <0,10>  ^ 
           NOP             4
           MPYSP   .M1     A3,A3,A3          ; |76| <0,15>  ^ 
           NOP             3
           ADDSP   .L1X    A3,B5,A3          ; |76| <0,19>  ^ 
           NOP             2
   [ B0]   BDEC    .S2     $C$L17,B0         ; |74| <0,22> 
           MPYSP   .M1     A5,A3,A3          ; |76| <0,23>  ^ 
           NOP             3
	.dwpsn	file "../fft.c",line 78,column 0,is_stmt
           STW     .D2T1   A3,*B4++          ; |76| <0,27>  ^ 
$C$DW$L$_find_x_mag$5$E:
;** --------------------------------------------------------------------------*
$C$L18:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_return
           RETNOP  .S2     B3,4              ; |79| 
           RINT                              ; interrupts on
           ; BRANCH OCCURS {B3}              ; |79| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../fft.c
;*      Loop source line                 : 74
;*      Loop opening brace source line   : 74
;*      Loop closing brace source line   : 78
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 65536                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 2
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1        1     
;*      .M units                     2*       1     
;*      .X cross paths               0        1     
;*      .T address paths             1        2*    
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           1        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        1     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 2  Schedule found with 10 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L19:    ; PIPED LOOP PROLOG
	.dwpsn	file "../fft.c",line 74,column 0,is_stmt
           SPLOOP  2       ;20               ; (P) 
;** --------------------------------------------------------------------------*
$C$L20:    ; PIPED LOOP KERNEL
$C$DW$L$_find_x_mag$9$B:

           SPMASK          L2
||         MV      .L2X    A6,B6
||         LDNDW   .D1T1   *A5++(8),A7:A6    ; |75| (P) <0,0> 

           NOP             4
           MPYSP   .M1     A7,A7,A4          ; |75| (P) <0,5> 
           MPYSP   .M1     A6,A6,A7          ; |75| (P) <0,6> 
           NOP             3
           ADDSP   .L1     A4,A7,A3          ; |75| (P) <0,10> 
           NOP             4
           MPYSP   .M2X    B6,A3,B4          ; |75| (P) <0,15> 
           NOP             2
           NOP             1
	.dwpsn	file "../fft.c",line 78,column 0,is_stmt

           SPKERNEL 9,0
||         STW     .D2T2   B4,*B5++          ; |75| <0,19> 

$C$DW$L$_find_x_mag$9$E:
;** --------------------------------------------------------------------------*
$C$L21:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L22:    
	.dwpsn	file "../fft.c",line 79,column 1,is_stmt
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |79| 
           ; BRANCH OCCURS {B3}              ; |79| 

$C$DW$81	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$81, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L20:1:1522266965")
	.dwattr $C$DW$81, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$81, DW_AT_TI_begin_line(0x4a)
	.dwattr $C$DW$81, DW_AT_TI_end_line(0x4e)
$C$DW$82	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$82, DW_AT_low_pc($C$DW$L$_find_x_mag$9$B)
	.dwattr $C$DW$82, DW_AT_high_pc($C$DW$L$_find_x_mag$9$E)
	.dwendtag $C$DW$81


$C$DW$83	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$83, DW_AT_name("E:\Documents\dev\rtp\CSS Workspace\lab5\Release\fft.asm:$C$L17:1:1522266965")
	.dwattr $C$DW$83, DW_AT_TI_begin_file("../fft.c")
	.dwattr $C$DW$83, DW_AT_TI_begin_line(0x4a)
	.dwattr $C$DW$83, DW_AT_TI_end_line(0x4e)
$C$DW$84	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$84, DW_AT_low_pc($C$DW$L$_find_x_mag$5$B)
	.dwattr $C$DW$84, DW_AT_high_pc($C$DW$L$_find_x_mag$5$E)
	.dwendtag $C$DW$83

	.dwattr $C$DW$74, DW_AT_TI_end_file("../fft.c")
	.dwattr $C$DW$74, DW_AT_TI_end_line(0x4f)
	.dwattr $C$DW$74, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$74

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_L138_initialise_intr
	.global	_pow
	.global	_sin
	.global	_cos
	.global	_ASM_init
	.global	_bitrev
	.global	_W
	.global	_x
	.global	_scale
	.global	_i_data
	.global	_idx
	.global	__divd
	.global	__divi

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$T$21	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x04)
$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$85, DW_AT_name("uint")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_uint")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$86, DW_AT_name("channel")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("AIC31_data_type")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

$C$DW$T$20	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$87	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$87, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$20

$C$DW$T$30	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x20)

$C$DW$T$42	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x800)
$C$DW$88	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$88, DW_AT_upper_bound(0x3ff)
	.dwendtag $C$DW$T$42


$C$DW$T$43	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x02)
$C$DW$89	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$89, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$43

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x20)

$C$DW$T$49	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x2000)
$C$DW$90	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$90, DW_AT_upper_bound(0x7ff)
	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x04)
$C$DW$91	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$91, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$50

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg0]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg1]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg2]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg3]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg4]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg5]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg6]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg7]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg8]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_reg9]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg10]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_reg11]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg12]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_reg13]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg14]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg15]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg16]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg17]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg18]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg19]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg20]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg21]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg22]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg23]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg24]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg25]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg26]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg27]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg28]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg29]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg30]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg31]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x20]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x21]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x22]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x23]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x24]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x25]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x26]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x27]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x28]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x29]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x30]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x31]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x32]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x33]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x34]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x35]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x36]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x37]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x38]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x39]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x40]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x41]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x42]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x43]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x44]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x45]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x46]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x47]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x48]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x49]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x50]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x51]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x52]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x53]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x54]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x55]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x56]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x57]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x58]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x59]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x60]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x61]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x62]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x63]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x64]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_regx 0x65]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x66]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_regx 0x67]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_regx 0x68]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_regx 0x69]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_regx 0x70]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_regx 0x71]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x72]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_regx 0x73]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_regx 0x74]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_regx 0x75]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_regx 0x76]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_regx 0x77]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_regx 0x78]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_regx 0x79]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

