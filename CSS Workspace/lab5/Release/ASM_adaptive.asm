;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Tue Mar 06 16:09:33 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=linasm --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Enabled with threshold = 0                           *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ASM_adaptive.sa")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\CSS Workspace\lab4\Release")
	.sect	".text"
        .global     _x_buf
_x_buf .usect ".x_buf", 1024, 4

        .global     _h_buf
_h_buf .usect ".h_buf", 1024, 4

		.title  "ASM_adaptive.sa"
		.def    _ASM_adaptive
		.sect  ".text"
		.global _ASM_adaptive
	.sect	".text"
	.clink

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("ASM_adaptive")
	.dwattr $C$DW$1, DW_AT_low_pc(_ASM_adaptive)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ASM_adaptive")
	.dwattr $C$DW$1, DW_AT_TI_begin_file("../ASM_adaptive.sa")
	.dwattr $C$DW$1, DW_AT_TI_begin_line(0x0b)
	.dwattr $C$DW$1, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$1, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$1, DW_AT_TI_skeletal
	.dwpsn	file "../ASM_adaptive.sa",line 11,column 1,is_stmt,address _ASM_adaptive

;******************************************************************************
;* FUNCTION NAME: ASM_adaptive                                                *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B4,B5,B6,B7,B8,B9,  *
;*                           A16,B16                                          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B4,B5,B6,B7,B8,B9,  *
;*                           A16,B16                                          *
;******************************************************************************
_ASM_adaptive:

	.map	h_tmp/A3
	.map	h_tmp'/B9
	.map	equ/B4
	.map	equ'/A0
	.map	equ''/B0
	.map	d/A16
	.map	e/A7
	.map	i/A6
	.map	i'/B8
	.map	len/A5
	.map	len'/B7
	.map	beta/A9
	.map	x_tmp/A3
	.map	x_tmp'/B8
	.map	bet_e/A4
	.map	x/A4
	.map	y/B7
	.map	y'/B16
	.map	offset/B5
	.map	y_tmp/B4

;** --------------------------------------------------------------------------*
; _ASM_adaptive:  .proc A4, B4, A6, B6, B3
; 		.reg    x, d, beta, offset, e, y, len		; I/O vars
; 		.reg	i, equ, x_tmp, h_tmp, y_tmp, bet_e	; tmp vars
; 		.reserve B5, B6			; B5 = x_buf, B6 = h_buf
; loop1: .trip 40
           MVK     .L1     0x1,A2
           MV      .L1     A2,A3

           MV      .L1     A3,A1
||         MVKL    .S2     _x_buf,B6         ; |25|  move x_buf into B6
||         MV      .L2     B6,offset         ; |23| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../ASM_adaptive.sa
;*      Loop source line                 : 46
;*      Loop closing brace source line   : 54
;*      Known Minimum Trip Count         : 40                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 4
;*      Unpartitioned Resource Bound     : 3
;*      Partitioned Resource Bound(*)    : 4
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     1        0     
;*      .S units                     0        0     
;*      .D units                     0        2     
;*      .M units                     0        1     
;*      .X cross paths               0        0     
;*      .T address paths             0        2     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        1     (.L or .S unit)
;*      Addition ops (.LSD)         10        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        1     
;*      Bound(.L .S .D .LS .LSD)     4*       2     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 4  Cannot allocate machine registers
;*                   Regs Live Always   :  2/2  (A/B-side)
;*                   Max Regs Live      :  9/7 
;*                   Max Cond Regs Live :  3/0 
;*         ii = 4  Cannot allocate machine registers
;*                   Regs Live Always   :  2/2  (A/B-side)
;*                   Max Regs Live      :  8/7 
;*                   Max Cond Regs Live :  3/0 
;*         ii = 4  Cannot allocate machine registers
;*                   Regs Live Always   :  2/2  (A/B-side)
;*                   Max Regs Live      :  9/7 
;*                   Max Cond Regs Live :  3/0 
;*         ii = 4  Schedule found with 4 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 3
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh56
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L1:    ; PIPED LOOP PROLOG
	.dwpsn	file "../ASM_adaptive.sa",line 46,column 0,is_stmt

   [ A1]   SPLOOPW 4       ;16               ; (P) 
||         MVKH    .S2     _x_buf,B6         ; |26| 

;** --------------------------------------------------------------------------*
$C$L2:    ; PIPED LOOP KERNEL
$C$DW$L$_ASM_adaptive$3$B:

           SPMASK          S2,D2
||         MVKL    .S2     _h_buf,B5         ; |28|  move h_buf into B5
||         STW     .D2T1   x,*++B6[offset]   ; |31|  store new value in x_buff

           SPMASK          L1,S1,D1,S2
||         MVK     .S1     0x100,len         ; |19|  input len
||         MV      .L1     A6,beta           ; |22| 
||         ZERO    .D1     i                 ; |43| 
||         MVKH    .S2     _h_buf,B5         ; |29| 
|| [ A2]   LDW     .D2T2   *B6++,x_tmp'      ; |46| (P) <0,1>  get x_buff val

           ADD     .S1     0x1,i,i           ; |52| (P) <0,2> 
|| [ A2]   LDW     .D2T2   *B5++,h_tmp'      ; |47| (P) <0,2>  get h_buff val

           CMPEQ   .L1     i,len,equ'        ; |53| (P) <0,3> 
   [ equ'] ZERO    .S1     A2                ; |53| (P) <0,4> 
           MV      .D1     A2,A4             ; |53| (P) <0,5> Split a long life(pre-sched)
           ROTL    .M1     A2,0,A8           ; |53| (P) <0,6> Post-sched spill

           SPMASK          S1
||         MV      .S1X    B4,d              ; |21| 
||         MPYSP   .M2     x_tmp',h_tmp',y_tmp ; |49| (P) <0,7>  (2)

           MV      .D1     A8,A1             ; |53| (P) <0,8> Split a long life(pre-sched)
           MV      .S1     A4,A7             ; |53| (P) <0,9> Split a long life(pre-sched)

           SPMASK          L2
||         ZERO    .L2     y'                ; |43| 

           SPMASK          S2
||         ZERO    .S2     B7                ; |43| 
||         ADDSP   .L2     y_tmp,y',y'       ; |50| (P) <0,11>  ^ 

           NOP             1

           ROTL    .M1     A3,0,A0           ; |53| <0,13> Post-sched spill
||         MV      .L1     A7,A3             ; |53| <0,13> Split a long life(pre-sched)

           NOP             1
	.dwpsn	file "../ASM_adaptive.sa",line 54,column 0,is_stmt

           SPKERNEL 0,0
|| [ A0]   MV      .S2     y',y              ; |53| <0,15> 

$C$DW$L$_ASM_adaptive$3$E:
;** --------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../ASM_adaptive.sa
;*      Loop source line                 : 70
;*      Loop closing brace source line   : 79
;*      Known Minimum Trip Count         : 40                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 14
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound(*)    : 3
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        1     
;*      .S units                     0        0     
;*      .D units                     0        3*    
;*      .M units                     1        0     
;*      .X cross paths               1        0     
;*      .T address paths             2        1     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           1        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        2     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        1     
;*      Bound(.L .S .D .LS .LSD)     1        2     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 14 Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 1
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh56
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L4:    ; PIPED LOOP PROLOG
; loop2: .trip 40
           NOP             1
           ZERO    .L2     B0                ; |68| 
	.dwpsn	file "../ASM_adaptive.sa",line 70,column 0,is_stmt
   [!equ''] SPLOOPW 14     ;28               ; (P) 
;** --------------------------------------------------------------------------*
$C$L5:    ; PIPED LOOP KERNEL
$C$DW$L$_ASM_adaptive$7$B:
           NOP             6

           SPMASK          L1
||         SUBSP   .L1X    d,y,e             ; |65|  (3)

           NOP             3

           SPMASK          M1
||         MPYSP   .M1     beta,e,bet_e      ; |67|  reduce MPYs

           NOP             2

           SPMASK          L2,S2
||         MV      .L2X    len,len'
||         ZERO    .S2     i'                ; |68| 

           LDW     .D2T1   *B6++,x_tmp       ; |70| <0,14>  ^ 
           LDW     .D2T2   *B5,h_tmp'        ; |71| <0,15> 
           NOP             3
           MPYSP   .M1     bet_e,x_tmp,x_tmp ; |73| <0,19>  ^  (5)
           NOP             1
           ADD     .L2     0x1,i',i'         ; |77| <0,21> 
           CMPEQ   .L2     i',len',equ       ; |78| <0,22> 

           MV      .L2     equ,equ''         ; |78| <0,23> 
||         ADDSP   .L1X    x_tmp,h_tmp',h_tmp ; |74| <0,23>  ^ 

           NOP             2
           NOP             1
	.dwpsn	file "../ASM_adaptive.sa",line 79,column 0,is_stmt

           SPKERNEL 0,0
||         STW     .D2T1   h_tmp,*B5++       ; |75| <0,27>  ^ 

$C$DW$L$_ASM_adaptive$7$E:
;** --------------------------------------------------------------------------*
$C$L6:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
	.dwpsn	file "../ASM_adaptive.sa",line 85,column 1,is_stmt
           MV      .L1     e,A4              ; |84| 

$C$DW$2	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$2, DW_AT_name("D:\CSS Workspace\lab4\Release\ASM_adaptive.asm:$C$L5:1:1520377773")
	.dwattr $C$DW$2, DW_AT_TI_begin_file("../ASM_adaptive.sa")
	.dwattr $C$DW$2, DW_AT_TI_begin_line(0x41)
	.dwattr $C$DW$2, DW_AT_TI_end_line(0x4f)
$C$DW$3	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$3, DW_AT_low_pc($C$DW$L$_ASM_adaptive$7$B)
	.dwattr $C$DW$3, DW_AT_high_pc($C$DW$L$_ASM_adaptive$7$E)
	.dwendtag $C$DW$2


$C$DW$4	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$4, DW_AT_name("D:\CSS Workspace\lab4\Release\ASM_adaptive.asm:$C$L2:1:1520377773")
	.dwattr $C$DW$4, DW_AT_TI_begin_file("../ASM_adaptive.sa")
	.dwattr $C$DW$4, DW_AT_TI_begin_line(0x13)
	.dwattr $C$DW$4, DW_AT_TI_end_line(0x36)
$C$DW$5	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$5, DW_AT_low_pc($C$DW$L$_ASM_adaptive$3$B)
	.dwattr $C$DW$5, DW_AT_high_pc($C$DW$L$_ASM_adaptive$3$E)
	.dwendtag $C$DW$4

	.dwattr $C$DW$1, DW_AT_TI_end_file("../ASM_adaptive.sa")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0x55)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$1

	.clearmap


; 	   .endproc A4, 		B3
		b B3
		nop 5

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$6	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg0]
$C$DW$7	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_reg1]
$C$DW$8	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_reg2]
$C$DW$9	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg3]
$C$DW$10	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg4]
$C$DW$11	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg5]
$C$DW$12	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg6]
$C$DW$13	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg7]
$C$DW$14	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg8]
$C$DW$15	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg9]
$C$DW$16	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg10]
$C$DW$17	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg11]
$C$DW$18	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg12]
$C$DW$19	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_reg13]
$C$DW$20	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_reg14]
$C$DW$21	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg15]
$C$DW$22	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg16]
$C$DW$23	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg17]
$C$DW$24	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg18]
$C$DW$25	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg19]
$C$DW$26	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg20]
$C$DW$27	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg21]
$C$DW$28	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg22]
$C$DW$29	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg23]
$C$DW$30	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg24]
$C$DW$31	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg25]
$C$DW$32	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg26]
$C$DW$33	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg27]
$C$DW$34	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg28]
$C$DW$35	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg29]
$C$DW$36	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg30]
$C$DW$37	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg31]
$C$DW$38	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_regx 0x20]
$C$DW$39	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_regx 0x21]
$C$DW$40	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_regx 0x22]
$C$DW$41	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_regx 0x23]
$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_regx 0x24]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_regx 0x25]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_regx 0x26]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_regx 0x27]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_regx 0x28]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_regx 0x29]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_regx 0x30]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_regx 0x31]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_regx 0x32]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_regx 0x33]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_regx 0x34]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_regx 0x35]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_regx 0x36]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x37]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x38]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x39]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x40]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x41]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x42]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x43]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x44]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x45]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x46]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x47]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x48]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x49]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x50]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x51]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x52]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x53]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x54]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x55]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x56]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x57]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x58]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x59]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x60]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x61]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x62]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x63]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x64]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x65]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x66]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x67]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x68]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x69]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x70]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x71]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x72]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x73]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x74]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x75]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x76]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x77]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x78]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x79]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

