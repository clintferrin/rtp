#include "L138_aic3106_init.h"
#include <stdio.h>
#include <math.h>

#define PI 3.1415926535897932384626
#define N 1024
#define RADIX 2

// L2 Shared Internal RAM ADDED Variables
extern float W[N*2];
extern float x[N*2];
extern float scale[1];					// scale factor for output
extern short i_data[N];					// input data index
extern short idx[1];					// input data index

// ASM function
void ASM_init();

// written functions
void calc_twid(float twid[], int len);
void set_init_values(float x[], float scale[], short idx[], short len);

// given functions
void digitrev_index(short *index, int n, int radix);
void bitrev(float *xs, short *index, int n);

int main(void) {
	short i_W[N/2];  					// W index
	ASM_init();
	set_init_values(x,scale,idx,N);

	// initialize twiddle factors w/ index
	calc_twid(W,N);     				// must use total N
	digitrev_index(i_W,N/RADIX,RADIX);
	bitrev(W,i_W,N/RADIX);

	// initialize output reverse vector
	digitrev_index(i_data,N,RADIX);

	L138_initialise_intr(FS_8000_HZ,ADC_GAIN_0DB,DAC_ATTEN_0DB);
	while(true);
}

void set_init_values(float x[], float scale[], short idx[], short len) {
	short i;
	scale[0] = pow(10,-9);
	idx[0] = 0;
	for(i = 0; i < len*2; i++) {
		x[i] = 0;
	}
}

void calc_twid(float twid[], int len) {
	int i;
	for(i = 0; i < len/2; i++) {
		twid[i*2] = cos(2*PI/len*i);	// real
		twid[i*2+1] = sin(2*PI/len*i);	// image
	}
}

void find_x_mag(float x[], float x_mag[], float scale, short len) {
    int i;
	for (i = 0; i < len; i++) {
        x_mag[i] = x[i*2]*x[i*2];
        x_mag[i] += x[i*2+1]*x[i*2+1];
        x_mag[i] *= scale;
    }
}

void digitrev_index(short *index, int n, int radix){

	int		i,j,k;
	short	nbits, nbot, ntop, ndiff, n2, raddiv2;

	nbits = 0;
	i = n;
	while (i > 1){
		i = i >> 1;
		nbits++;
	}

	raddiv2	= radix >> 1;
	nbot	= nbits >> raddiv2;
	nbot	= nbot << raddiv2 - 1;
	ndiff	= nbits & raddiv2;
	ntop	= nbot + ndiff;
	n2		= 1 << ntop;

	index[0] = 0;
	for ( i = 1, j = n2/radix + 1; i < n2 - 1; i++){
		index[i] = j - 1;
		for (k = n2/radix; k*(radix-1) < j; k /= radix)
				j -= k*(radix-1);
		j += k;
	}
	index[n2 - 1] = n2 - 1;
}
