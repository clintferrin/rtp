#include "L138_aic3106_init.h"
#include <stdio.h>
#include <math.h>

#define num_sos 5		// change in ASM_float_to_fixed and interrupt
#define scale_factor 2 	// change in ASM_float_to_fixed and interrupt
#define num_bits 16	 	// change in ASM_float_to_fixed and interrupt
// check scaling in set_init_values function

// L2 Shared Internal RAM ADDED Variables
extern int32_t num[num_sos*3];
extern int32_t den[num_sos*3];
extern int32_t g[num_sos*3];

// ASM function
void ASM_init();
int32_t ASM_float_to_fixed(int32_t num_SOS, int32_t num_Bits, int32_t scale_Factor);
int32_t ASM_direct_2_fixed(int32_t x, int32_t *num, int32_t *den, int32_t *g, int32_t len);

// L2 Shared Internal RAM ADDED Variables
extern int32_t num[num_sos*3];
extern int32_t den[num_sos*3];
extern int32_t g[num_sos*3];

// ASM function
void ASM_init();

// written functions
void set_init_values(int32_t num[], int32_t den[], int32_t N);

int main(void) {
	ASM_init();
	set_init_values(num,den,num_sos);

	L138_initialise_intr(FS_48000_HZ,ADC_GAIN_0DB,DAC_ATTEN_0DB);
	while(true);
}

void set_init_values(int32_t num[], int32_t den[], int32_t N) {
	int i;
	float round_factor = pow(2,31-num_bits);
	float scale = pow(2,32-scale_factor);


	float SOS_Num[] = {0.0331774610388562,0,-0.0331774610388562,
	           	   	   0.0331774610388562,0,-0.0331774610388562,
	           	   	   0.0223846361306439,0,-0.0223846361306439,
	           	   	   0.0223846361306439,0,-0.0223846361306439,
	           	   	   0.0117220794355669,0,-0.0117220794355669};

	float SOS_Den[] = {1,-1.53963958515431,0.992390511236914,
	           	   	   1,-1.62043849159704,0.993017925820779,
	           	   	   1,-1.54675738172820,0.980496237929246,
	           	   	   1,-1.59688581879472,0.981501228904223,
	           	   	   1,-1.56894721281840,0.976555841128866};


	for(i = 0; i < N*3; i++) {
		num[i] = (int32_t)(SOS_Num[i] * scale + round_factor) & 0xFFFF0000;
		den[i] = (int32_t)(SOS_Den[i] * scale + round_factor) & 0xFFFF0000;
		g[i] = 0;
	}
}
