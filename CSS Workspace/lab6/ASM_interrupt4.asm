		.title  "ASM_interrupt4.sa"
		.def    _interrupt4
		.sect  ".text"
		.global _interrupt4, _cfftr2_dit, _bitrev
        .global     _W, _x, _scale, _i_data, _idx
; 		=======================================================
;    			   	  		Program flow
; 		=======================================================
;
;		_______________________________________________________

_interrupt4:  .proc IRP
; 		=======================================================
;    					   Initialization
; 		=======================================================
		.reg	ch1, ch2, in_data, out_data, output
		.reg	idx_1024, idx_512
		.reg    W,W_ptr,x,x_ptr,x_mag,x_mag_ptr
		.reg	scale,scale_ptr,i_data_ptr,idx,idx_ptr
        .reg	x_mag_real,x_mag_img,img,equ,N,z

		MVKL 	_W, W_ptr			    ; twiddle factors 
		MVKH 	_W, W_ptr

		MVKL 	_x, x_ptr			    ; input buffer 
		MVKH 	_x, x_ptr

		MVKL 	_scale, scale_ptr       ; scale factor 
		MVKH 	_scale, scale_ptr 

		MVKL 	_i_data, i_data_ptr	    ; bitrev() index vec 
		MVKH 	_i_data, i_data_ptr

		MVKL 	_idx, idx_ptr           ; current counter 
		MVKH 	_idx, idx_ptr 

		MVKL	0x1D00230,in_data		; base address for input
		MVKH	0x1D00230,in_data

		MVKL	0x1D0022C,out_data		; base address for output
		MVKH	0x1D0022C,out_data

		MVK		1024, N					; number of complex values
		MVK		2048, idx_1024
		MVK		1024, idx_512
		ZERO	z

		LDH 	*idx_ptr, idx        
		LDW 	*scale_ptr, scale

; 		=======================================================
;    			   	  	  Read incoming data
; 		=======================================================
		LDH		*in_data,ch1			; read in ch1
		LDH		*in_data[1],ch2			; read in ch2
		INTSP	ch1,ch1					; cast ch1 to float
		MV		ch1,x

; 		=======================================================
;							Calculate fft
; 		=======================================================
;		if(idx==1024) {				(1)
;			idx = 0;				(2)
;			cfftr2_dit(x,W,N);		(3)
;			set ch2 to pulse		(4)
;		}
;		_______________________________________________________

if_1024:
		CMPEQ	idx,idx_1024, equ			; (1) check 1024
		[!equ] 	b if_512

		MVK		0,idx						; (2) reset index
		.call	_cfftr2_dit(x_ptr,W_ptr,N)	; (3) call fft function
		.call	_bitrev(x_ptr,i_data_ptr,N)
		b		pulse						; (4) set pulse


; 		=======================================================
;							Send pulse
; 		=======================================================
;		if(idx==512) {				(5)
;			set ch2 to pulse		(6)
;		}
;		_______________________________________________________

if_512:
		CMPEQ	idx,idx_512, equ			; (5) check if 512
		[!equ] 	b else

pulse:
		MVK		0, ch2						; (6) set ch2 to pulse
		b		write

else:
		MVK		-0x7FFF ,ch2				; - puts 1 in sign bit

; 		=======================================================
;    			   	  	  Write output data
; 		=======================================================
;		ch1 = x.r*x.r + x.i*x.i		(7)
;		ch1 = ch1*scale				(8)
;		ch1 = short(ch1)			(9)
;
;		x[idx  ] = x				(10)
;		x[idx+1] = 0
;		idx += 2					(11)
;		output_data()				(12)
;		_______________________________________________________

write:
		LDW		*x_ptr[idx], x_mag_real		; (7) find magnitude
		MPYSP	x_mag_real,x_mag_real,x_mag_real

		ADD		idx,1, img					; load img value
		LDW		*x_ptr[img], x_mag_img
		MPYSP	x_mag_img,x_mag_img,x_mag_img

		ADDSP	x_mag_real,x_mag_img,ch1

		MPYSP	ch1,scale,ch1				; (8) scale output

		SPINT	ch1,ch1						; (9) convert to short

		STW		x,*x_ptr[idx]				; (10) store new value
		STW		z,*x_ptr[img]				; set imginary to 0

		ADD		idx,2,idx					; (11) update index
		STH		idx,*idx_ptr

		PACK2	ch1,ch2,output				; (12) output data
		STW		output,*out_data

; 		=======================================================
;    				      Return to program
; 		=======================================================
	   .endproc IRP
		b IRP
		nop 5
