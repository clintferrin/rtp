#ifndef _TEMPLATE1_H_
#define _TEMPLATE1_H_

#include "types.h"
#include "math.h"
#include "stdio.h"
#include "evmomapl138.h"
#include "evmomapl138_timer.h"
#include "evmomapl138_gpio.h"
#include "evmomapl138_i2c.h"
#include "evmomapl138_mcasp.h"
#include "evmomapl138_aic3106.h"
#include "evmomapl138_dip.h"
#include "evmomapl138_mcasp.h"
#include "evmomapl138_aic3106.h"

//-----------------------------------------------------------------------------
// Prototypes
//-----------------------------------------------------------------------------

void McASP_Init();				// init McASP
void AIC3106_Init();			// init AIC3106
void McASP_Start();				// start McASP
void SetGpio();					// Setup GPIO


//-----------------------------------------------------------------------------
// defines
//-----------------------------------------------------------------------------
#define PINMUX_MCASP_REG_17   17
#define PINMUX_MCASP_MASK_17  0x0000ff00
#define PINMUX_MCASP_VAL_17   0x00000800

// Set the sampling frequency at which the DAC will be operating.
// You may use any of the Sampling Frequencies specified below.
// Anything else will result in the default fs of 8kHz.
//#define SAMP_FREQ	(8000)		// Default
//#define SAMP_FREQ	(9600)
//#define SAMP_FREQ	(24000)
#define SAMP_FREQ	(48000)


#endif // _TEMPLATE1_H_
