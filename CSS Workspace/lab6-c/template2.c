#include "L138_aic3106_init.h"
#include <math.h>

// direct form II variables
int16_t direct_filt_len = 3;

#define num_sos 5		// change in ASM_float_to_fixed and interrupt
#define scale_factor 2 	// change in ASM_float_to_fixed and interrupt
#define num_bits 6	 	// change in ASM_float_to_fixed and interrupt

float round_factor;
float scale;
float scale_inv;

// check scaling in set_init_values function


float num[15];
float den[15];
float g[15] = {0};

float y;
float xn;

// This data structure allows for outputting to both channels.
AIC31_data_type codec_data;

float ASM_direct_2(float x, float num[], float den[], float g[], int16_t len);
float direct_2(float x, float num[], float den[], float g[], int16_t len);
float fixed_point_round(float x);
void set_init_values(float num[], float den[], int32_t N);
int32_t ASM_direct_2_fixed(int32_t x, int32_t *num, int32_t *den, int32_t *g, int32_t len);


interrupt void interrupt4(void)
{
	// Get the next sample of the input.
	y = (float)(input_left_sample()); // input from ADC
	xn = (float)(input_right_sample()); // input from ADC

	y = fixed_point_round(y);

	y = direct_2(y, &num[0], &den[0], &g[0], direct_filt_len);
	y = direct_2(y, &num[3], &den[3], &g[3], direct_filt_len);
	y = direct_2(y, &num[6], &den[6], &g[6], direct_filt_len);
	y = direct_2(y, &num[9], &den[9], &g[9], direct_filt_len);
	y = direct_2(y, &num[12],&den[12],&g[12],direct_filt_len);

	// output to BOTH right and left channels...
	codec_data.channel[LEFT] = (uint16_t)(y);
	codec_data.channel[RIGHT] = (uint16_t)(xn);
	output_sample(codec_data.uint);  // output to L and R DAC

	return;
}

int main(void)
{
	round_factor = pow(2,-1);
	scale = pow(2,num_bits);
	scale_inv = pow(2,-num_bits);

	set_init_values(num,den,5);

	L138_initialise_intr(FS_48000_HZ,ADC_GAIN_0DB,DAC_ATTEN_0DB);

	while(true);
}


void set_init_values(float num[], float den[], int32_t N) {
	int i;

	float SOS_Num[] = {0.0331774610388562,0,-0.0331774610388562,
	           	   	   0.0331774610388562,0,-0.0331774610388562,
	           	   	   0.0223846361306439,0,-0.0223846361306439,
	           	   	   0.0223846361306439,0,-0.0223846361306439,
	           	   	   0.0117220794355669,0,-0.0117220794355669};

	float SOS_Den[] = {1,-1.53963958515431,0.992390511236914,
	           	   	   1,-1.62043849159704,0.993017925820779,
	           	   	   1,-1.54675738172820,0.980496237929246,
	           	   	   1,-1.59688581879472,0.981501228904223,
	           	   	   1,-1.56894721281840,0.976555841128866};


	// fill in with int values
	for(i = 0; i < N*3; i++) {
		num[i] = fixed_point_round(SOS_Num[i]);
		den[i] = fixed_point_round(SOS_Den[i]);
		g[i] = 0;
	}
}

float direct_2(float x, float num[], float den[], float g[], int16_t len) {
    int16_t i;
    float y = 0;
    for (i = 0; i < len-1; i++) {
        x += fixed_point_round(g[i]*(-den[i+1]));
        y += fixed_point_round(g[i]*(num[i+1]));
    }

    for (i = 0; i < len-1; i++) {
        g[len-1-i] = fixed_point_round(g[len-2-i]);
    }
    g[0]=fixed_point_round(x);
    return fixed_point_round((y+x*num[0]));
}

float fixed_point_round(float x) {
	int x_tmp = (int)(x *  scale + round_factor);
	return (float)x_tmp*scale_inv;
}

