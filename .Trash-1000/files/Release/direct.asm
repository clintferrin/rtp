;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Fri Feb 02 10:27:37 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=on --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Enabled with threshold = 0                           *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../direct.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\CCS Workspace\lab2\Release")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("EVMOMAPL138_init")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_EVMOMAPL138_init")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("EVMOMAPL138_initRAM")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_EVMOMAPL138_initRAM")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("EVMOMAPL138_enableDsp")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_EVMOMAPL138_enableDsp")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$19)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$19)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$19)
	.dwendtag $C$DW$4


$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("USTIMER_init")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_USTIMER_init")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setDir")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_GPIO_setDir")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$19)
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$49)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$9


$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_init")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_I2C_init")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$33)
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$30)
	.dwendtag $C$DW$13


$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("McASP_Init")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_McASP_Init")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external

$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("AIC3106_Init")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_AIC3106_Init")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external

$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("McASP_Start")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_McASP_Start")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external

$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("calloc")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_calloc")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
	.sect	".const:_$P$T0$1"
	.clink
	.align	8
_$P$T0$1:
	.word	03bfeeee2h		; _$P$T0$1[0] @ 0
	.word	0bbeae009h		; _$P$T0$1[1] @ 32
	.word	03d6f93beh		; _$P$T0$1[2] @ 64
	.word	03c822cb5h		; _$P$T0$1[3] @ 96
	.word	0bd24fb5bh		; _$P$T0$1[4] @ 128
	.word	0bda3d7d6h		; _$P$T0$1[5] @ 160
	.word	0bd8dda6bh		; _$P$T0$1[6] @ 192
	.word	039139fddh		; _$P$T0$1[7] @ 224
	.word	03da758fdh		; _$P$T0$1[8] @ 256
	.word	03dd53f66h		; _$P$T0$1[9] @ 288
	.word	03d1322c4h		; _$P$T0$1[10] @ 320
	.word	0bd9d7ce1h		; _$P$T0$1[11] @ 352
	.word	0be113c07h		; _$P$T0$1[12] @ 384
	.word	0bdc8ac95h		; _$P$T0$1[13] @ 416
	.word	03cd6a242h		; _$P$T0$1[14] @ 448
	.word	03e06e961h		; _$P$T0$1[15] @ 480
	.word	03e06e961h		; _$P$T0$1[16] @ 512
	.word	03cd6a242h		; _$P$T0$1[17] @ 544
	.word	0bdc8ac95h		; _$P$T0$1[18] @ 576
	.word	0be113c07h		; _$P$T0$1[19] @ 608
	.word	0bd9d7ce1h		; _$P$T0$1[20] @ 640
	.word	03d1322c4h		; _$P$T0$1[21] @ 672
	.word	03dd53f66h		; _$P$T0$1[22] @ 704
	.word	03da758fdh		; _$P$T0$1[23] @ 736
	.word	039139fddh		; _$P$T0$1[24] @ 768
	.word	0bd8dda6bh		; _$P$T0$1[25] @ 800
	.word	0bda3d7d6h		; _$P$T0$1[26] @ 832
	.word	0bd24fb5bh		; _$P$T0$1[27] @ 864
	.word	03c822cb5h		; _$P$T0$1[28] @ 896
	.word	03d6f93beh		; _$P$T0$1[29] @ 928
	.word	0bbeae009h		; _$P$T0$1[30] @ 960
	.word	03bfeeee2h		; _$P$T0$1[31] @ 992

$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("$P$T0$1")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_$P$T0$1")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_addr _$P$T0$1]
;	C:\ccsv5\tools\compiler\c6000_7.4.4\bin\opt6x.exe C:\\Users\\A02179~1\\AppData\\Local\\Temp\\060682 C:\\Users\\A02179~1\\AppData\\Local\\Temp\\060684 
	.sect	".text"
	.clink
	.global	_sendReceiveAudio

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("sendReceiveAudio")
	.dwattr $C$DW$21, DW_AT_low_pc(_sendReceiveAudio)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_sendReceiveAudio")
	.dwattr $C$DW$21, DW_AT_external
	.dwattr $C$DW$21, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x43)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(0x00)
	.dwattr $C$DW$21, DW_AT_frame_base[DW_OP_breg31 0]
	.dwattr $C$DW$21, DW_AT_TI_skeletal
	.dwpsn	file "../direct.c",line 67,column 39,is_stmt,address _sendReceiveAudio
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("audio")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_audio")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg4]

;******************************************************************************
;* FUNCTION NAME: sendReceiveAudio                                            *
;*                                                                            *
;*   Regs Modified     : A0,A3,A4,A5,A6,A7,A8,B0,B4,B5,B6,B7                  *
;*   Regs Used         : A0,A3,A4,A5,A6,A7,A8,B0,B3,B4,B5,B6,B7               *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_sendReceiveAudio:
;** --------------------------------------------------------------------------*

           MVKL    .S2     0x1d001ac,B4
||         MV      .L1     A4,A6             ; |67| 

           MVKH    .S2     0x1d001ac,B4
||         LDW     .D1T1   *+A6(4),A3        ; |69| 

           LDW     .D2T2   *B4,B7            ; |71| 
||         LDW     .D1T1   *A6,A4            ; |68| 

           NOP             3
           SPTRUNC .L2X    A3,B6             ; |69| 

           EXTU    .S2     B7,27,31,B0       ; |71| 
||         SPTRUNC .L2X    A4,B5             ; |68| 

   [ B0]   BNOP    .S1     $C$L4,3           ; |71| 
|| [!B0]   MVK     .L2     0x1,B0

           EXT     .S2     B6,16,16,B6       ; |69| 
           EXT     .S2     B5,16,16,B7       ; |68| 
           ; BRANCHCC OCCURS {$C$L4}         ; |71| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../direct.c
;*      Loop source line                 : 71
;*      Loop opening brace source line   : 71
;*      Loop closing brace source line   : 71
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1*       0     
;*      .D units                     1*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             1*       0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1*       0     
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 1
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L1:    ; PIPED LOOP PROLOG
	.dwpsn	file "../direct.c",line 71,column 0,is_stmt
   [ B0]   SPLOOPW 7       ;14               ; (P) 
;** --------------------------------------------------------------------------*
$C$L2:    ; PIPED LOOP KERNEL
$C$DW$L$_sendReceiveAudio$3$B:
           NOP             1

           SPMASK          L1
||         MV      .L1X    B4,A4

           MV      .L1     A4,A5             ; (P) <0,2> 
   [ B0]   LDW     .D1T1   *A5,A3            ; |71| (P) <0,3>  ^ 
           NOP             4
           EXTU    .S1     A3,27,31,A0       ; |71| <0,8>  ^ 
   [ A0]   ZERO    .L2     B0                ; |71| <0,9>  ^ 
           NOP             2
           NOP             1
           SPKERNEL 0,0
$C$DW$L$_sendReceiveAudio$3$E:
;** --------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
           MV      .L2X    A4,B4
;** --------------------------------------------------------------------------*
$C$L4:    
           ADDAD   .D2     B4,16,B5
           STW     .D2T2   B7,*B5            ; |72| 
           ADD     .L1X    4,B5,A8

           LDW     .D1T1   *A8,A4            ; |73| 
||         LDW     .D2T2   *B4,B7            ; |75| 

           NOP             4

           EXTU    .S2     B7,27,31,B0       ; |75| 
||         EXT     .S1     A4,16,16,A7       ; |73| 

   [ B0]   BNOP    .S1     $C$L8,5           ; |75| 
|| [!B0]   MVK     .L2     0x1,B0

           ; BRANCHCC OCCURS {$C$L8}         ; |75| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../direct.c
;*      Loop source line                 : 75
;*      Loop opening brace source line   : 75
;*      Loop closing brace source line   : 75
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound(*)    : 1
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     1*       0     
;*      .D units                     1*       0     
;*      .M units                     0        0     
;*      .X cross paths               0        0     
;*      .T address paths             1*       0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           0        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        1     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1*       0     
;*      Bound(.L .S .D .LS .LSD)     1*       1*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Schedule found with 2 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 1
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*----------------------------------------------------------------------------*
$C$L5:    ; PIPED LOOP PROLOG
	.dwpsn	file "../direct.c",line 75,column 0,is_stmt
   [ B0]   SPLOOPW 7       ;14               ; (P) 
;** --------------------------------------------------------------------------*
$C$L6:    ; PIPED LOOP KERNEL
$C$DW$L$_sendReceiveAudio$8$B:
           NOP             1

           SPMASK          L1
||         MV      .L1X    B4,A4

           MV      .L1     A4,A5             ; (P) <0,2> 
   [ B0]   LDW     .D1T1   *A5,A3            ; |75| (P) <0,3>  ^ 
           NOP             4
           EXTU    .S1     A3,27,31,A0       ; |75| <0,8>  ^ 
   [ A0]   ZERO    .L2     B0                ; |75| <0,9>  ^ 
           NOP             2
           NOP             1
           SPKERNEL 0,0
$C$DW$L$_sendReceiveAudio$8$E:
;** --------------------------------------------------------------------------*
$C$L7:    ; PIPED LOOP EPILOG
;** --------------------------------------------------------------------------*
$C$L8:    
           STW     .D2T2   B6,*B5            ; |76| 
           LDW     .D1T1   *A8,A3            ; |77| 
           INTSP   .L1     A7,A4             ; |79| 
           NOP             3
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x04)
	.dwattr $C$DW$23, DW_AT_TI_return

           EXT     .S1     A3,16,16,A3       ; |77| 
||         RET     .S2     B3                ; |81| 

           INTSP   .L1     A3,A3             ; |80| 
           STW     .D1T1   A4,*A6            ; |79| 
           NOP             2
	.dwpsn	file "../direct.c",line 81,column 1,is_stmt
           STW     .D1T1   A3,*+A6(4)        ; |80| 
           ; BRANCH OCCURS {B3}              ; |81| 

$C$DW$24	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$24, DW_AT_name("D:\CCS Workspace\lab2\Release\direct.asm:$C$L6:1:1517592457")
	.dwattr $C$DW$24, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$24, DW_AT_TI_begin_line(0x4b)
	.dwattr $C$DW$24, DW_AT_TI_end_line(0x4b)
$C$DW$25	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$25, DW_AT_low_pc($C$DW$L$_sendReceiveAudio$8$B)
	.dwattr $C$DW$25, DW_AT_high_pc($C$DW$L$_sendReceiveAudio$8$E)
	.dwendtag $C$DW$24


$C$DW$26	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$26, DW_AT_name("D:\CCS Workspace\lab2\Release\direct.asm:$C$L2:1:1517592457")
	.dwattr $C$DW$26, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$26, DW_AT_TI_begin_line(0x47)
	.dwattr $C$DW$26, DW_AT_TI_end_line(0x47)
$C$DW$27	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$27, DW_AT_low_pc($C$DW$L$_sendReceiveAudio$3$B)
	.dwattr $C$DW$27, DW_AT_high_pc($C$DW$L$_sendReceiveAudio$3$E)
	.dwendtag $C$DW$26

	.dwattr $C$DW$21, DW_AT_TI_end_file("../direct.c")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x51)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$21

	.sect	".text"
	.clink
	.global	_SetGpio

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("SetGpio")
	.dwattr $C$DW$28, DW_AT_low_pc(_SetGpio)
	.dwattr $C$DW$28, DW_AT_high_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_SetGpio")
	.dwattr $C$DW$28, DW_AT_external
	.dwattr $C$DW$28, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$28, DW_AT_TI_begin_line(0x53)
	.dwattr $C$DW$28, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$28, DW_AT_TI_max_frame_size(0x08)
	.dwattr $C$DW$28, DW_AT_frame_base[DW_OP_breg31 8]
	.dwattr $C$DW$28, DW_AT_TI_skeletal
	.dwpsn	file "../direct.c",line 83,column 20,is_stmt,address _SetGpio

;******************************************************************************
;* FUNCTION NAME: SetGpio                                                     *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 0 Auto + 4 Save = 4 byte                    *
;******************************************************************************
_SetGpio:
;** --------------------------------------------------------------------------*
           ZERO    .L2     B4

           SET     .S2     B4,0x8,0xf,B4
||         MVK     .S1     0x11,A4           ; |84| 

$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x04)
	.dwattr $C$DW$29, DW_AT_name("_EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$29, DW_AT_TI_call

           STW     .D2T2   B3,*SP--(8)       ; |83| 
||         CALLP   .S2     _EVMOMAPL138_pinmuxConfig,B3
||         MVK     .S1     0x800,A6          ; |84| 

$C$RL0:    ; CALL OCCURS {_EVMOMAPL138_pinmuxConfig} {0}  ; |84| 
;** --------------------------------------------------------------------------*
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_call
	.dwattr $C$DW$30, DW_AT_TI_return
           CALLRET .S1     _GPIO_setDir      ; |85| 
           LDW     .D2T2   *++SP(8),B3       ; |86| 
           MVK     .L1     0x7,A4            ; |85| 
           ZERO    .L1     A6                ; |85| 
           MV      .L2X    A4,B4             ; |85| 
	.dwpsn	file "../direct.c",line 86,column 1,is_stmt
           NOP             1
$C$RL1:    ; CALL-RETURN OCCURS {_GPIO_setDir} 0  ; |85| 
	.dwattr $C$DW$28, DW_AT_TI_end_file("../direct.c")
	.dwattr $C$DW$28, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$28, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$28

	.sect	".text"
	.clink
	.global	_main

$C$DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("main")
	.dwattr $C$DW$31, DW_AT_low_pc(_main)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_main")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$31, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0x0f)
	.dwattr $C$DW$31, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(0x98)
	.dwattr $C$DW$31, DW_AT_frame_base[DW_OP_breg31 152]
	.dwattr $C$DW$31, DW_AT_TI_skeletal
	.dwpsn	file "../direct.c",line 15,column 16,is_stmt,address _main

;******************************************************************************
;* FUNCTION NAME: main                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5, *
;*                           B6,B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,B0,B1,B2,B3,B4,*
;*                           B5,B6,B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,*
;*                           A23,A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18, *
;*                           B19,B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30, *
;*                           B31                                              *
;*   Local Frame Size  : 0 Args + 140 Auto + 8 Save = 148 byte                *
;******************************************************************************
_main:
;** --------------------------------------------------------------------------*
           ADDK    .S2     -152,SP           ; |15| 
           STW     .D2T1   A10,*+SP(148)     ; |15| 
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_EVMOMAPL138_init")
	.dwattr $C$DW$32, DW_AT_TI_call

           CALLP   .S2     _EVMOMAPL138_init,B3
||         STW     .D2T1   A11,*+SP(152)     ; |15| 

$C$RL2:    ; CALL OCCURS {_EVMOMAPL138_init} {0}  ; |16| 
;** --------------------------------------------------------------------------*
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_EVMOMAPL138_initRAM")
	.dwattr $C$DW$33, DW_AT_TI_call
           CALLP   .S2     _EVMOMAPL138_initRAM,B3
$C$RL3:    ; CALL OCCURS {_EVMOMAPL138_initRAM} {0}  ; |17| 
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_EVMOMAPL138_enableDsp")
	.dwattr $C$DW$34, DW_AT_TI_call
           CALLP   .S2     _EVMOMAPL138_enableDsp,B3
$C$RL4:    ; CALL OCCURS {_EVMOMAPL138_enableDsp} {0}  ; |18| 
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_USTIMER_init")
	.dwattr $C$DW$35, DW_AT_TI_call
           CALLP   .S2     _USTIMER_init,B3
$C$RL5:    ; CALL OCCURS {_USTIMER_init} {0}  ; |21| 
           MVKL    .S1     0x1c22000,A4
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("_I2C_init")
	.dwattr $C$DW$36, DW_AT_TI_call

           CALLP   .S2     _I2C_init,B3
||         MVKH    .S1     0x1c22000,A4
||         MVK     .L2     0x1,B4            ; |22| 

$C$RL6:    ; CALL OCCURS {_I2C_init} {0}     ; |22| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_name("_SetGpio")
	.dwattr $C$DW$37, DW_AT_TI_call
           CALLP   .S2     _SetGpio,B3
$C$RL7:    ; CALL OCCURS {_SetGpio} {0}      ; |25| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_McASP_Init")
	.dwattr $C$DW$38, DW_AT_TI_call
           CALLP   .S2     _McASP_Init,B3
$C$RL8:    ; CALL OCCURS {_McASP_Init} {0}   ; |26| 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_AIC3106_Init")
	.dwattr $C$DW$39, DW_AT_TI_call
           CALLP   .S2     _AIC3106_Init,B3
$C$RL9:    ; CALL OCCURS {_AIC3106_Init} {0}  ; |27| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("_McASP_Start")
	.dwattr $C$DW$40, DW_AT_TI_call
           CALLP   .S2     _McASP_Start,B3
$C$RL10:   ; CALL OCCURS {_McASP_Start} {0}  ; |28| 
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("_calloc")
	.dwattr $C$DW$41, DW_AT_TI_call

           CALLP   .S2     _calloc,B3
||         MVK     .S1     0x20,A4           ; |37| 
||         MVK     .L2     0x4,B4            ; |37| 

$C$RL11:   ; CALL OCCURS {_calloc} {0}       ; |37| 
           MVKL    .S2     _$P$T0$1,B4
           MVKH    .S2     _$P$T0$1,B4
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_name("_memcpy")
	.dwattr $C$DW$42, DW_AT_TI_call

           CALLP   .S2     _memcpy,B3
||         MVK     .S1     0x80,A6           ; |41| 
||         MV      .L1     A4,A10            ; |37| 
||         ADDAW   .D1X    SP,4,A4           ; |41| 

$C$RL12:   ; CALL OCCURS {_memcpy} {0}       ; |41| 
;** --------------------------------------------------------------------------*

           MVK     .S2     0x7c,B17
||         ZERO    .L1     A19               ; |34| 
||         ZERO    .S1     A20               ; |34| 

;** --------------------------------------------------------------------------*
$C$L9:    
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_sendReceiveAudio")
	.dwattr $C$DW$43, DW_AT_TI_call
           CALL    .S1     _sendReceiveAudio ; |51| 
           NOP             4
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L10
;** --------------------------------------------------------------------------*
$C$L10:    
$C$DW$L$_main$5$B:

           ADD     .L1X    8,SP,A4           ; |51| 
||         ADDKPC  .S2     $C$RL13,B3,0      ; |51| 

$C$RL13:   ; CALL OCCURS {_sendReceiveAudio} {0}  ; |51| 
$C$DW$L$_main$5$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_main$6$B:
           LDW     .D2T2   *+SP(8),B4        ; |54| 
           ADD     .D1     A20,31,A3         ; |53| 
           EXTU    .S1     A3,27,27,A3       ; |53| 
           EXT     .S1     A3,16,16,A20      ; |53| 

           MVK     .L2     0x8,B5            ; |58| 
||         MVK     .S2     7,B6              ; |58| 

           STW     .D1T2   B4,*+A10[A20]     ; |54| 
||         ADDAW   .D2     SP,4,B4
||         EXT     .S1     A3,16,14,A18      ; |53| 
||         MVC     .S2     B5,RILC
||         MV      .L2X    A10,B9

$C$DW$L$_main$6$E:
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : ../direct.c
;*      Loop source line                 : 58
;*      Loop opening brace source line   : 58
;*      Loop closing brace source line   : 60
;*      Loop Unroll Multiple             : 4x
;*      Known Minimum Trip Count         : 8                    
;*      Known Maximum Trip Count         : 8                    
;*      Known Max Trip Count Factor      : 8
;*      Loop Carried Dependency Bound(^) : 4
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound(*)    : 4
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     2        2     
;*      .D units                     3        3     
;*      .M units                     2        2     
;*      .X cross paths               4*       0     
;*      .T address paths             3        3     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        2     (.L or .S unit)
;*      Addition ops (.LSD)          4        4     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             2        2     
;*      Bound(.L .S .D .LS .LSD)     4*       4*    
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 4  Schedule found with 4 iterations in parallel
;*      Done
;*
;*      Loop will be splooped
;*      Collapsed epilog stages       : 0
;*      Collapsed prolog stages       : 0
;*      Minimum required memory pad   : 0 bytes
;*
;*      For further improvement on this loop, try option -mh124
;*
;*      Minimum safe trip count       : 1 (after unrolling)
;*----------------------------------------------------------------------------*
$C$L11:    ; PIPED LOOP PROLOG
	.dwpsn	file "../direct.c",line 58,column 0,is_stmt

           SPLOOPD 4       ;16               ; (P) 
||         MVC     .S2     B6,ILC
||         ADD     .L1     4,A18,A17
||         ADD     .L2X    12,A18,B16
||         ADD     .S1X    8,B4,A3

;** --------------------------------------------------------------------------*
$C$L12:    ; PIPED LOOP KERNEL
$C$DW$L$_main$8$B:

           SPMASK          L2
||         ADD     .L2X    8,A18,B20
||         AND     .S1X    B17,A17,A6        ; |59| (P) <0,0> 

           ADDK    .S1     16,A18            ; |58| (P) <0,1> 
||         AND     .D1X    B17,A18,A4        ; |59| (P) <0,1> 
||         AND     .S2     B17,B20,B5        ; |59| (P) <0,1> 
||         AND     .D2     B17,B16,B6        ; |59| (P) <0,1> 

           ADDK    .S1     16,A17            ; |58| (P) <0,2> Define a twin register
||         LDDW    .D2T1   *B4++(16),A5:A4   ; |59| (P) <0,2> 
||         LDDW    .D1T2   *A3++(16),B19:B18 ; |59| (P) <0,2> 
||         ADD     .L1X    B9,A4,A7          ; |59| (P) <0,2> 
||         ADD     .L2     B9,B5,B5          ; |59| (P) <0,2> 
||         ADD     .S2     B9,B6,B21         ; |59| (P) <0,2> 

           ADDK    .S2     16,B20            ; |58| (P) <0,3> Define a twin register
||         LDW     .D1T1   *A7,A8            ; |59| (P) <0,3> 
||         ADD     .S1X    B9,A6,A6          ; |59| (P) <0,3> 
||         LDW     .D2T2   *B5,B6            ; |59| (P) <0,3> 

           ADDK    .S2     16,B16            ; |58| (P) <0,4> 
||         LDW     .D1T1   *A6,A4            ; |59| (P) <0,4> 
||         LDW     .D2T2   *B21,B5           ; |59| (P) <0,4> 

           NOP             3

           SPMASK          L1
||         MV      .L1     A19,A16           ; |58| 
||         MPYSP   .M1     A8,A4,A7          ; |59| (P) <0,8> 
||         MPYSP   .M2     B6,B18,B5         ; |59| (P) <0,8> 

           SPMASK          L2
||         MV      .L2X    A19,B8            ; |58| 
||         MPYSP   .M1     A4,A5,A7          ; |59| (P) <0,9> 
||         MPYSP   .M2     B5,B19,B6         ; |59| (P) <0,9> 

           NOP             1

           SPMASK          L1,L2
||         MV      .L1     A19,A9            ; |58| 
||         MV      .L2X    A19,B7            ; |58| 

           ADDSP   .L1     A7,A16,A16        ; |59| <0,12>  ^ 
||         ADDSP   .L2     B5,B8,B8          ; |59| <0,12>  ^ 

	.dwpsn	file "../direct.c",line 60,column 0,is_stmt

           SPKERNEL 0,0
||         ADDSP   .L1     A7,A9,A9          ; |59| <0,13>  ^ 
||         ADDSP   .L2     B6,B7,B7          ; |59| <0,13>  ^ 

$C$DW$L$_main$8$E:
;** --------------------------------------------------------------------------*
$C$L13:    ; PIPED LOOP EPILOG
           NOP             1
           MV      .S1X    B9,A10
           NOP             8
;** --------------------------------------------------------------------------*
$C$DW$L$_main$10$B:
           NOP             3
           ADDSP   .L1     A16,A9,A3         ; |61| 
           NOP             3
           ADDSP   .L1X    B7,A3,A3          ; |61| 
           NOP             4

           B       .S1     $C$L10            ; |50| 
||         ADDSP   .L2X    B8,A3,B4          ; |61| 

$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("_sendReceiveAudio")
	.dwattr $C$DW$44, DW_AT_TI_call
           CALL    .S1     _sendReceiveAudio ; |51| 
           NOP             2
           STW     .D2T2   B4,*+SP(8)        ; |61| 
	.dwpsn	file "../direct.c",line 64,column 1,is_stmt
           STW     .D2T2   B4,*+SP(12)       ; |62| 
           ; BRANCH OCCURS {$C$L10}          ; |50| 
$C$DW$L$_main$10$E:

$C$DW$45	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$45, DW_AT_name("D:\CCS Workspace\lab2\Release\direct.asm:$C$L10:1:1517592457")
	.dwattr $C$DW$45, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$45, DW_AT_TI_begin_line(0x32)
	.dwattr $C$DW$45, DW_AT_TI_end_line(0x40)
$C$DW$46	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$46, DW_AT_low_pc($C$DW$L$_main$5$B)
	.dwattr $C$DW$46, DW_AT_high_pc($C$DW$L$_main$5$E)
$C$DW$47	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$47, DW_AT_low_pc($C$DW$L$_main$6$B)
	.dwattr $C$DW$47, DW_AT_high_pc($C$DW$L$_main$6$E)
$C$DW$48	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$48, DW_AT_low_pc($C$DW$L$_main$10$B)
	.dwattr $C$DW$48, DW_AT_high_pc($C$DW$L$_main$10$E)

$C$DW$49	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$49, DW_AT_name("D:\CCS Workspace\lab2\Release\direct.asm:$C$L12:2:1517592457")
	.dwattr $C$DW$49, DW_AT_TI_begin_file("../direct.c")
	.dwattr $C$DW$49, DW_AT_TI_begin_line(0x3a)
	.dwattr $C$DW$49, DW_AT_TI_end_line(0x3c)
$C$DW$50	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$50, DW_AT_low_pc($C$DW$L$_main$8$B)
	.dwattr $C$DW$50, DW_AT_high_pc($C$DW$L$_main$8$E)
	.dwendtag $C$DW$49

	.dwendtag $C$DW$45

	.dwattr $C$DW$31, DW_AT_TI_end_file("../direct.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0x40)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$31

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_EVMOMAPL138_init
	.global	_EVMOMAPL138_initRAM
	.global	_EVMOMAPL138_enableDsp
	.global	_EVMOMAPL138_pinmuxConfig
	.global	_USTIMER_init
	.global	_GPIO_setDir
	.global	_I2C_init
	.global	_McASP_Init
	.global	_AIC3106_Init
	.global	_McASP_Start
	.global	_calloc
	.global	_memcpy

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$T$29	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x04)
$C$DW$51	.dwtag  DW_TAG_enumerator, DW_AT_name("I2C_CLK_100K"), DW_AT_const_value(0x00)
$C$DW$52	.dwtag  DW_TAG_enumerator, DW_AT_name("I2C_CLK_400K"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$29

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("i2c_clk_e")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x60)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$53, DW_AT_name("ICOAR")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ICOAR")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$54, DW_AT_name("ICIMR")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ICIMR")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$55, DW_AT_name("ICSTR")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ICSTR")
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$56, DW_AT_name("ICCLKL")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ICCLKL")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$57, DW_AT_name("ICCLKH")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ICCLKH")
	.dwattr $C$DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$58, DW_AT_name("ICCNT")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ICCNT")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$59, DW_AT_name("ICDRR")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ICDRR")
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$60, DW_AT_name("ICSAR")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ICSAR")
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$61	.dwtag  DW_TAG_member
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$61, DW_AT_name("ICDXR")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ICDXR")
	.dwattr $C$DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$62	.dwtag  DW_TAG_member
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$62, DW_AT_name("ICMDR")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ICMDR")
	.dwattr $C$DW$62, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$62, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$63	.dwtag  DW_TAG_member
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$63, DW_AT_name("ICIVR")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ICIVR")
	.dwattr $C$DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$63, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$64	.dwtag  DW_TAG_member
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$64, DW_AT_name("ICEMDR")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ICEMDR")
	.dwattr $C$DW$64, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$64, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$65, DW_AT_name("ICPSC")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ICPSC")
	.dwattr $C$DW$65, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$66, DW_AT_name("REVID1")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_REVID1")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$67	.dwtag  DW_TAG_member
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$67, DW_AT_name("REVID2")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_REVID2")
	.dwattr $C$DW$67, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$68	.dwtag  DW_TAG_member
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$68, DW_AT_name("RSVD0")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_RSVD0")
	.dwattr $C$DW$68, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$68, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$69	.dwtag  DW_TAG_member
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$69, DW_AT_name("ICPFUNC")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ICPFUNC")
	.dwattr $C$DW$69, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$70	.dwtag  DW_TAG_member
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$70, DW_AT_name("ICPDIR")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ICPDIR")
	.dwattr $C$DW$70, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$70, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$71	.dwtag  DW_TAG_member
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$71, DW_AT_name("ICPDIN")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ICPDIN")
	.dwattr $C$DW$71, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$72	.dwtag  DW_TAG_member
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$72, DW_AT_name("ICPDOUT")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ICPDOUT")
	.dwattr $C$DW$72, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$72, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$73, DW_AT_name("ICPDSET")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_ICPDSET")
	.dwattr $C$DW$73, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$74	.dwtag  DW_TAG_member
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$74, DW_AT_name("ICPDCLR")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ICPDCLR")
	.dwattr $C$DW$74, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("i2c_regs_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_address_class(0x20)

$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)
$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$75, DW_AT_name("left")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_left")
	.dwattr $C$DW$75, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$76, DW_AT_name("right")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_right")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("IO_audio")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x20)

$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x2c0)
$C$DW$77	.dwtag  DW_TAG_member
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$77, DW_AT_name("REVID")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_REVID")
	.dwattr $C$DW$77, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$77, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$78	.dwtag  DW_TAG_member
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$78, DW_AT_name("RSVD0")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_RSVD0")
	.dwattr $C$DW$78, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$78, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$79	.dwtag  DW_TAG_member
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$79, DW_AT_name("PFUNC")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_PFUNC")
	.dwattr $C$DW$79, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$79, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$80	.dwtag  DW_TAG_member
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$80, DW_AT_name("PDIR")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_PDIR")
	.dwattr $C$DW$80, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$80, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$81	.dwtag  DW_TAG_member
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$81, DW_AT_name("PDOUT")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_PDOUT")
	.dwattr $C$DW$81, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$81, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$82	.dwtag  DW_TAG_member
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$82, DW_AT_name("PDIN")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_PDIN")
	.dwattr $C$DW$82, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$82, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$83, DW_AT_name("PDCLR")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_PDCLR")
	.dwattr $C$DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$84, DW_AT_name("RSVD1")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_RSVD1")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$85, DW_AT_name("GBLCTL")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_GBLCTL")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$86, DW_AT_name("AMUTE")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_AMUTE")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$87, DW_AT_name("DLBCTL")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_DLBCTL")
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$88, DW_AT_name("DITCTL")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_DITCTL")
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$89, DW_AT_name("RSVD2")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_RSVD2")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$90, DW_AT_name("RGBLCTL")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_RGBLCTL")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$91, DW_AT_name("RMASK")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_RMASK")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$92, DW_AT_name("RFMT")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_RFMT")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$93, DW_AT_name("AFSRCTL")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_AFSRCTL")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$94, DW_AT_name("ACLKRCTL")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_ACLKRCTL")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$95, DW_AT_name("AHCLKRCTL")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_AHCLKRCTL")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$96, DW_AT_name("RTDM")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_RTDM")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$97, DW_AT_name("RINTCTL")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_RINTCTL")
	.dwattr $C$DW$97, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$98, DW_AT_name("RSTAT")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_RSTAT")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$99, DW_AT_name("RSLOT")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_RSLOT")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x84]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$100, DW_AT_name("RCLKCHK")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_RCLKCHK")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$101, DW_AT_name("REVTCTL")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_REVTCTL")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x8c]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$102, DW_AT_name("RSVD3")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_RSVD3")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$103, DW_AT_name("XGBLCTL")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_XGBLCTL")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$104, DW_AT_name("XMASK")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_XMASK")
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0xa4]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$105, DW_AT_name("XFMT")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_XFMT")
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$106, DW_AT_name("AFSXCTL")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_AFSXCTL")
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0xac]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$107, DW_AT_name("ACLKXCTL")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_ACLKXCTL")
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$108, DW_AT_name("AHCLKXCTL")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_AHCLKXCTL")
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0xb4]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$109, DW_AT_name("XTDM")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_XTDM")
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$110, DW_AT_name("XINTCTL")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_XINTCTL")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$111, DW_AT_name("XSTAT")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_XSTAT")
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$112, DW_AT_name("XSLOT")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_XSLOT")
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$113, DW_AT_name("XCLKCHK")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_XCLKCHK")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$114, DW_AT_name("XEVTCTL")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_XEVTCTL")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$115, DW_AT_name("RSVD4")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_RSVD4")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$116, DW_AT_name("DITCSRA0")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_DITCSRA0")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$117, DW_AT_name("DITCSRA1")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_DITCSRA1")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$118, DW_AT_name("DITCSRA2")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_DITCSRA2")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$119, DW_AT_name("DITCSRA3")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_DITCSRA3")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$120, DW_AT_name("DITCSRA4")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_DITCSRA4")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$121, DW_AT_name("DITCSRA5")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_DITCSRA5")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x114]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$122, DW_AT_name("DITCSRB0")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_DITCSRB0")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x118]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$123, DW_AT_name("DITCSRB1")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_DITCSRB1")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x11c]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$124, DW_AT_name("DITCSRB2")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_DITCSRB2")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x120]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$125, DW_AT_name("DITCSRB3")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_DITCSRB3")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x124]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$126, DW_AT_name("DITCSRB4")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_DITCSRB4")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x128]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$127, DW_AT_name("DITCSRB5")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_DITCSRB5")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$128, DW_AT_name("DITUDRA0")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_DITUDRA0")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$129, DW_AT_name("DITUDRA1")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_DITUDRA1")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x134]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$130, DW_AT_name("DITUDRA2")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_DITUDRA2")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x138]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$131, DW_AT_name("DITUDRA3")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_DITUDRA3")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x13c]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$132, DW_AT_name("DITUDRA4")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_DITUDRA4")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x140]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$133, DW_AT_name("DITUDRA5")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_DITUDRA5")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x144]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$134, DW_AT_name("DITUDRB0")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_DITUDRB0")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x148]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$135, DW_AT_name("DITUDRB1")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_DITUDRB1")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x14c]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$136, DW_AT_name("DITUDRB2")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_DITUDRB2")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x150]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$137, DW_AT_name("DITUDRB3")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_DITUDRB3")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x154]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$138, DW_AT_name("DITUDRB4")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_DITUDRB4")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x158]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$139, DW_AT_name("DITUDRB5")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_DITUDRB5")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x15c]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$140, DW_AT_name("RSVD5")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_RSVD5")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x160]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$141, DW_AT_name("SRCTL0")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_SRCTL0")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x180]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$142, DW_AT_name("SRCTL1")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_SRCTL1")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x184]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$143, DW_AT_name("SRCTL2")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_SRCTL2")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x188]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$144, DW_AT_name("SRCTL3")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_SRCTL3")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x18c]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$145, DW_AT_name("SRCTL4")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_SRCTL4")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x190]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$146, DW_AT_name("SRCTL5")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_SRCTL5")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x194]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$147, DW_AT_name("SRCTL6")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_SRCTL6")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x198]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$148, DW_AT_name("SRCTL7")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_SRCTL7")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x19c]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$149, DW_AT_name("SRCTL8")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_SRCTL8")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a0]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$150, DW_AT_name("SRCTL9")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_SRCTL9")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a4]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$151, DW_AT_name("SRCTL10")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_SRCTL10")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a8]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$152, DW_AT_name("SRCTL11")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_SRCTL11")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ac]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$153, DW_AT_name("SRCTL12")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_SRCTL12")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b0]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$154, DW_AT_name("SRCTL13")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_SRCTL13")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b4]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$155, DW_AT_name("SRCTL14")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_SRCTL14")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b8]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$156, DW_AT_name("SRCTL15")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_SRCTL15")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x1bc]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$157, DW_AT_name("RSVD6")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_RSVD6")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c0]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$158, DW_AT_name("XBUF0")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_XBUF0")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x200]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$159, DW_AT_name("XBUF1")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_XBUF1")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x204]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$160, DW_AT_name("XBUF2")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_XBUF2")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x208]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$161, DW_AT_name("XBUF3")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_XBUF3")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x20c]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$162, DW_AT_name("XBUF4")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_XBUF4")
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x210]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$163, DW_AT_name("XBUF5")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_XBUF5")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x214]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$164, DW_AT_name("XBUF6")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_XBUF6")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x218]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$165, DW_AT_name("XBUF7")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_XBUF7")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x21c]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$166, DW_AT_name("XBUF8")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_XBUF8")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x220]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$167, DW_AT_name("XBUF9")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_XBUF9")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x224]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$168, DW_AT_name("XBUF10")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_XBUF10")
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x228]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$169, DW_AT_name("XBUF11")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_XBUF11")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x22c]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$170, DW_AT_name("XBUF12")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_XBUF12")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x230]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$171, DW_AT_name("XBUF13")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_XBUF13")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x234]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$172, DW_AT_name("XBUF14")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_XBUF14")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x238]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$173, DW_AT_name("XBUF15")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_XBUF15")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x23c]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$174, DW_AT_name("RSVD7")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_RSVD7")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x240]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$175, DW_AT_name("RBUF0")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_RBUF0")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x280]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$176, DW_AT_name("RBUF1")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_RBUF1")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x284]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$177, DW_AT_name("RBUF2")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_RBUF2")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x288]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$178, DW_AT_name("RBUF3")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_RBUF3")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x28c]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$179, DW_AT_name("RBUF4")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_RBUF4")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x290]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$180, DW_AT_name("RBUF5")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_RBUF5")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x294]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$181, DW_AT_name("RBUF6")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_RBUF6")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x298]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$182, DW_AT_name("RBUF7")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_RBUF7")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x29c]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$183, DW_AT_name("RBUF8")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_RBUF8")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a0]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$184, DW_AT_name("RBUF9")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_RBUF9")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a4]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$185, DW_AT_name("RBUF10")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_RBUF10")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a8]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$186, DW_AT_name("RBUF11")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_RBUF11")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x2ac]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$187, DW_AT_name("RBUF12")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_RBUF12")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b0]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$188, DW_AT_name("RBUF13")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_RBUF13")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b4]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$189, DW_AT_name("RBUF14")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_RBUF14")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b8]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$190, DW_AT_name("RBUF15")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_RBUF15")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x2bc]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("mcasp_regs_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$20	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)

$C$DW$T$21	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x0c)
$C$DW$191	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$191, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$21


$C$DW$T$24	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x20)
$C$DW$192	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$192, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x10)
$C$DW$193	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$193, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x30)
$C$DW$194	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$194, DW_AT_upper_bound(0x0b)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x40)
$C$DW$195	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$195, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$27

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$66	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$16)

$C$DW$T$67	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x80)
$C$DW$196	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$196, DW_AT_upper_bound(0x1f)
	.dwendtag $C$DW$T$67

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_reg0]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg1]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg2]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg3]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg4]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg5]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_reg6]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg7]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg8]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg9]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg10]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg11]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_reg12]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_reg13]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_reg14]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg15]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg16]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_reg17]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg18]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_reg19]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_reg20]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg21]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg22]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg23]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg24]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg25]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg26]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg27]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg28]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_reg29]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg30]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg31]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_regx 0x20]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_regx 0x21]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_regx 0x22]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_regx 0x23]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_regx 0x24]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_regx 0x25]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_regx 0x26]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_regx 0x27]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_regx 0x28]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x29]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x30]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x31]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x32]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x33]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x34]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x35]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x36]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x37]
$C$DW$253	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$253, DW_AT_location[DW_OP_regx 0x38]
$C$DW$254	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$254, DW_AT_location[DW_OP_regx 0x39]
$C$DW$255	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$255, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$256	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$256, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$257	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$258	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$258, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$259	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$259, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$260	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$260, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$261	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$261, DW_AT_location[DW_OP_regx 0x40]
$C$DW$262	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$262, DW_AT_location[DW_OP_regx 0x41]
$C$DW$263	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$263, DW_AT_location[DW_OP_regx 0x42]
$C$DW$264	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$264, DW_AT_location[DW_OP_regx 0x43]
$C$DW$265	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$265, DW_AT_location[DW_OP_regx 0x44]
$C$DW$266	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$266, DW_AT_location[DW_OP_regx 0x45]
$C$DW$267	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_regx 0x46]
$C$DW$268	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_regx 0x47]
$C$DW$269	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_regx 0x48]
$C$DW$270	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_regx 0x49]
$C$DW$271	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$272	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$273	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$274	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$274, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$275	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$275, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$276	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$277	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_regx 0x50]
$C$DW$278	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_regx 0x51]
$C$DW$279	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_regx 0x52]
$C$DW$280	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_regx 0x53]
$C$DW$281	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_regx 0x54]
$C$DW$282	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_regx 0x55]
$C$DW$283	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_regx 0x56]
$C$DW$284	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_regx 0x57]
$C$DW$285	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_regx 0x58]
$C$DW$286	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_regx 0x59]
$C$DW$287	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$288	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$289	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$290	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$291	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$292	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$293	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_regx 0x60]
$C$DW$294	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_regx 0x61]
$C$DW$295	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_regx 0x62]
$C$DW$296	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_regx 0x63]
$C$DW$297	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$297, DW_AT_location[DW_OP_regx 0x64]
$C$DW$298	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$298, DW_AT_location[DW_OP_regx 0x65]
$C$DW$299	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$299, DW_AT_location[DW_OP_regx 0x66]
$C$DW$300	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$300, DW_AT_location[DW_OP_regx 0x67]
$C$DW$301	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$301, DW_AT_location[DW_OP_regx 0x68]
$C$DW$302	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$302, DW_AT_location[DW_OP_regx 0x69]
$C$DW$303	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$304	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$305	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$305, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$306	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$306, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$307	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$307, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$308	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$308, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$309	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$309, DW_AT_location[DW_OP_regx 0x70]
$C$DW$310	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$310, DW_AT_location[DW_OP_regx 0x71]
$C$DW$311	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$311, DW_AT_location[DW_OP_regx 0x72]
$C$DW$312	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$312, DW_AT_location[DW_OP_regx 0x73]
$C$DW$313	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$313, DW_AT_location[DW_OP_regx 0x74]
$C$DW$314	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$314, DW_AT_location[DW_OP_regx 0x75]
$C$DW$315	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_regx 0x76]
$C$DW$316	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_regx 0x77]
$C$DW$317	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_regx 0x78]
$C$DW$318	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_regx 0x79]
$C$DW$319	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$320	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$320, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$321	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$321, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$322	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$322, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$323	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$323, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$324	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$324, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$325	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$325, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

