;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Fri Jan 12 11:17:09 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=on --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:dwarf 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Disabled                                             *
;*   Optimizing for    : Compile time, Ease of Development                    *
;*                       Based on options: no -o, no -ms                      *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../aic3106.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("E:\clint_test\Debug")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_read")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_I2C_read")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$24)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$30)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$27)
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$30)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$26)
	.dwendtag $C$DW$1


$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_write")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_I2C_write")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$24)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$30)
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$27)
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$30)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$26)
	.dwendtag $C$DW$7

;	C:\ccsv5\tools\compiler\c6000_7.4.4\bin\acp6x.exe -@C:\\Users\\A02179~1\\AppData\\Local\\Temp\\1006814 
	.sect	".text"
	.clink
	.global	_AIC3106_Init

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("AIC3106_Init")
	.dwattr $C$DW$13, DW_AT_low_pc(_AIC3106_Init)
	.dwattr $C$DW$13, DW_AT_high_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_AIC3106_Init")
	.dwattr $C$DW$13, DW_AT_external
	.dwattr $C$DW$13, DW_AT_TI_begin_file("../aic3106.c")
	.dwattr $C$DW$13, DW_AT_TI_begin_line(0x2a)
	.dwattr $C$DW$13, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$13, DW_AT_TI_max_frame_size(0x08)
	.dwpsn	file "../aic3106.c",line 43,column 1,is_stmt,address _AIC3106_Init

	.dwfde $C$DW$CIE, _AIC3106_Init

;******************************************************************************
;* FUNCTION NAME: AIC3106_Init                                                *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 0 Auto + 4 Save = 4 byte                    *
;******************************************************************************
_AIC3106_Init:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           STW     .D2T2   B3,*SP--(8)       ; |43| 
	.dwcfi	cfa_offset, 8
	.dwcfi	save_reg_to_mem, 19, 0
	.dwpsn	file "../aic3106.c",line 45,column 4,is_stmt
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$14, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         ZERO    .L1     A4                ; |45| 
||         ZERO    .L2     B4                ; |45| 

$C$RL0:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |45| 
	.dwpsn	file "../aic3106.c",line 46,column 4,is_stmt
           MVK     .S2     0x80,B4           ; |46| 
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$15, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0x1,A4            ; |46| 

$C$RL1:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |46| 
	.dwpsn	file "../aic3106.c",line 90,column 5,is_stmt
           MVK     .S2     0x22,B4           ; |90| 
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$16, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0x3,A4            ; |90| 

$C$RL2:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |90| 
	.dwpsn	file "../aic3106.c",line 95,column 5,is_stmt
           MVK     .S2     0x22,B4           ; |95| 
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$17, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0x2,A4            ; |95| 

$C$RL3:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |95| 
	.dwpsn	file "../aic3106.c",line 102,column 4,is_stmt
           MVK     .S2     0x8a,B4           ; |102| 
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$18, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0x7,A4            ; |102| 

$C$RL4:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |102| 
	.dwpsn	file "../aic3106.c",line 111,column 4,is_stmt
           MVK     .S2     0xc0,B4           ; |111| 
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$19, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0x8,A4            ; |111| 

$C$RL5:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |111| 
	.dwpsn	file "../aic3106.c",line 121,column 4,is_stmt
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$20, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0x9,A4            ; |121| 
||         ZERO    .L2     B4                ; |121| 

$C$RL6:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |121| 
	.dwpsn	file "../aic3106.c",line 134,column 4,is_stmt
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$21, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .L1     0xf,A4            ; |134| 
||         ZERO    .L2     B4                ; |134| 

$C$RL7:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |134| 
	.dwpsn	file "../aic3106.c",line 139,column 4,is_stmt
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$22, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x10,A4           ; |139| 
||         ZERO    .L2     B4                ; |139| 

$C$RL8:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |139| 
	.dwpsn	file "../aic3106.c",line 155,column 4,is_stmt
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$23, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x13,A4           ; |155| 
||         MVK     .L2     0x4,B4            ; |155| 

$C$RL9:    ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |155| 
	.dwpsn	file "../aic3106.c",line 156,column 4,is_stmt
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$24, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x16,A4           ; |156| 
||         MVK     .L2     0x4,B4            ; |156| 

$C$RL10:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |156| 
;** --------------------------------------------------------------------------*
	.dwpsn	file "../aic3106.c",line 158,column 4,is_stmt
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$25, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x1b,A4           ; |158| 
||         ZERO    .L2     B4                ; |158| 

$C$RL11:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |158| 
	.dwpsn	file "../aic3106.c",line 159,column 4,is_stmt
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$26, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x1e,A4           ; |159| 
||         ZERO    .L2     B4                ; |159| 

$C$RL12:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |159| 
	.dwpsn	file "../aic3106.c",line 161,column 4,is_stmt
           MVK     .S2     0xe0,B4           ; |161| 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$27, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x25,A4           ; |161| 

$C$RL13:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |161| 
	.dwpsn	file "../aic3106.c",line 169,column 4,is_stmt
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$28, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x2b,A4           ; |169| 
||         ZERO    .L2     B4                ; |169| 

$C$RL14:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |169| 
	.dwpsn	file "../aic3106.c",line 173,column 4,is_stmt
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$29, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x2c,A4           ; |173| 
||         ZERO    .L2     B4                ; |173| 

$C$RL15:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |173| 
	.dwpsn	file "../aic3106.c",line 188,column 4,is_stmt
           MVK     .S2     0x80,B4           ; |188| 
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$30, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x52,A4           ; |188| 

$C$RL16:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |188| 
	.dwpsn	file "../aic3106.c",line 192,column 4,is_stmt
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$31, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x56,A4           ; |192| 
||         MVK     .L2     0x9,B4            ; |192| 

$C$RL17:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |192| 
	.dwpsn	file "../aic3106.c",line 200,column 4,is_stmt
           MVK     .S2     0x80,B4           ; |200| 
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$32, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x5c,A4           ; |200| 

$C$RL18:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |200| 
	.dwpsn	file "../aic3106.c",line 205,column 4,is_stmt
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$33, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x5d,A4           ; |205| 
||         MVK     .L2     0x9,B4            ; |205| 

$C$RL19:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |205| 
	.dwpsn	file "../aic3106.c",line 213,column 4,is_stmt
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$34, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x65,A4           ; |213| 
||         MVK     .L2     0x1,B4            ; |213| 

$C$RL20:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |213| 
	.dwpsn	file "../aic3106.c",line 222,column 4,is_stmt
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_AIC3106_writeRegister")
	.dwattr $C$DW$35, DW_AT_TI_call

           CALLP   .S2     _AIC3106_writeRegister,B3
||         MVK     .S1     0x66,A4           ; |222| 
||         ZERO    .L2     B4                ; |222| 

$C$RL21:   ; CALL OCCURS {_AIC3106_writeRegister} {0}  ; |222| 
;** --------------------------------------------------------------------------*
	.dwpsn	file "../aic3106.c",line 229,column 1,is_stmt
           LDW     .D2T2   *++SP(8),B3       ; |229| 
           NOP             4
	.dwcfi	cfa_offset, 0
	.dwcfi	restore_reg, 19
	.dwcfi	cfa_offset, 0
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |229| 
           ; BRANCH OCCURS {B3}              ; |229| 
	.dwattr $C$DW$13, DW_AT_TI_end_file("../aic3106.c")
	.dwattr $C$DW$13, DW_AT_TI_end_line(0xe5)
	.dwattr $C$DW$13, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$13

	.sect	".text"
	.clink
	.global	_AIC3106_readRegister

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("AIC3106_readRegister")
	.dwattr $C$DW$37, DW_AT_low_pc(_AIC3106_readRegister)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_AIC3106_readRegister")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../aic3106.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0xf1)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(0x10)
	.dwpsn	file "../aic3106.c",line 242,column 1,is_stmt,address _AIC3106_readRegister

	.dwfde $C$DW$CIE, _AIC3106_readRegister
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("in_reg_addr")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_in_reg_addr")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg4]
$C$DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dest_buffer")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_dest_buffer")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: AIC3106_readRegister                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 12 Auto + 4 Save = 16 byte                  *
;******************************************************************************
_AIC3106_readRegister:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           STW     .D2T2   B3,*SP--(16)      ; |242| 
	.dwcfi	cfa_offset, 16
	.dwcfi	save_reg_to_mem, 19, 0
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("in_reg_addr")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_in_reg_addr")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg31 4]
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("dest_buffer")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_dest_buffer")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg31 8]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("rtn")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_rtn")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg31 12]
           STW     .D2T2   B4,*+SP(8)        ; |242| 
           STB     .D2T1   A4,*+SP(4)        ; |242| 
	.dwpsn	file "../aic3106.c",line 246,column 4,is_stmt

           MVKL    .S1     0x1c22000,A4
||         MVK     .S2     0x18,B4           ; |246| 

$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_I2C_write")
	.dwattr $C$DW$43, DW_AT_TI_call

           CALLP   .S2     _I2C_write,B3
||         MVKH    .S1     0x1c22000,A4
||         ADD     .L1X    4,SP,A6           ; |246| 
||         MVK     .L2     0x1,B6            ; |246| 
||         ZERO    .D1     A8                ; |246| 

$C$RL22:   ; CALL OCCURS {_I2C_write} {0}    ; |246| 
           STW     .D2T1   A4,*+SP(12)       ; |246| 
	.dwpsn	file "../aic3106.c",line 247,column 4,is_stmt
           MV      .L2X    A4,B0
   [ B0]   BNOP    .S1     $C$L1,5           ; |247| 
           ; BRANCHCC OCCURS {$C$L1}         ; |247| 
;** --------------------------------------------------------------------------*
	.dwpsn	file "../aic3106.c",line 248,column 7,is_stmt
	.dwpsn	file "../aic3106.c",line 251,column 4,is_stmt

           MVKL    .S1     0x1c22000,A4
||         MVK     .S2     0x18,B4           ; |251| 

$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("_I2C_read")
	.dwattr $C$DW$44, DW_AT_TI_call

           CALLP   .S2     _I2C_read,B3
||         LDW     .D2T1   *+SP(8),A6        ; |251| 
||         MVKH    .S1     0x1c22000,A4
||         MVK     .L2     0x1,B6            ; |251| 
||         ZERO    .L1     A8                ; |251| 

$C$RL23:   ; CALL OCCURS {_I2C_read} {0}     ; |251| 
           STW     .D2T1   A4,*+SP(12)       ; |251| 
	.dwpsn	file "../aic3106.c",line 253,column 4,is_stmt
;** --------------------------------------------------------------------------*
$C$L1:    
	.dwpsn	file "../aic3106.c",line 254,column 1,is_stmt
           LDW     .D2T2   *++SP(16),B3      ; |254| 
           NOP             4
	.dwcfi	cfa_offset, 0
	.dwcfi	restore_reg, 19
	.dwcfi	cfa_offset, 0
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |254| 
           ; BRANCH OCCURS {B3}              ; |254| 
	.dwattr $C$DW$37, DW_AT_TI_end_file("../aic3106.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0xfe)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text"
	.clink
	.global	_AIC3106_writeRegister

$C$DW$46	.dwtag  DW_TAG_subprogram, DW_AT_name("AIC3106_writeRegister")
	.dwattr $C$DW$46, DW_AT_low_pc(_AIC3106_writeRegister)
	.dwattr $C$DW$46, DW_AT_high_pc(0x00)
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_AIC3106_writeRegister")
	.dwattr $C$DW$46, DW_AT_external
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$46, DW_AT_TI_begin_file("../aic3106.c")
	.dwattr $C$DW$46, DW_AT_TI_begin_line(0x10a)
	.dwattr $C$DW$46, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$46, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "../aic3106.c",line 267,column 1,is_stmt,address _AIC3106_writeRegister

	.dwfde $C$DW$CIE, _AIC3106_writeRegister
$C$DW$47	.dwtag  DW_TAG_formal_parameter, DW_AT_name("in_reg_addr")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_in_reg_addr")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg4]
$C$DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_name("in_data")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_in_data")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: AIC3106_writeRegister                                       *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 20 Auto + 4 Save = 24 byte                  *
;******************************************************************************
_AIC3106_writeRegister:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           STW     .D2T2   B3,*SP--(24)      ; |267| 
	.dwcfi	cfa_offset, 24
	.dwcfi	save_reg_to_mem, 19, 0
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("in_reg_addr")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_in_reg_addr")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg31 4]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("in_data")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_in_data")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg31 5]
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("rtn")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_rtn")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg31 8]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("i2c_data")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_i2c_data")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg31 16]
           STB     .D2T2   B4,*+SP(5)        ; |267| 
           STB     .D2T1   A4,*+SP(4)        ; |267| 
	.dwpsn	file "../aic3106.c",line 271,column 4,is_stmt
           LDBU    .D2T2   *+SP(4),B4        ; |271| 
           NOP             4
           STB     .D2T2   B4,*+SP(16)       ; |271| 
	.dwpsn	file "../aic3106.c",line 272,column 4,is_stmt
           LDBU    .D2T2   *+SP(5),B4        ; |272| 
           NOP             4
           STB     .D2T2   B4,*+SP(17)       ; |272| 
	.dwpsn	file "../aic3106.c",line 275,column 4,is_stmt

           MVKL    .S1     0x1c22000,A4
||         MVK     .S2     0x18,B4           ; |275| 

$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_I2C_write")
	.dwattr $C$DW$53, DW_AT_TI_call

           CALLP   .S2     _I2C_write,B3
||         MVKH    .S1     0x1c22000,A4
||         ADDAW   .D1X    SP,4,A6           ; |275| 
||         MVK     .L2     0x2,B6            ; |275| 
||         MVK     .L1     0x1,A8            ; |275| 

$C$RL24:   ; CALL OCCURS {_I2C_write} {0}    ; |275| 
           STW     .D2T1   A4,*+SP(8)        ; |275| 
	.dwpsn	file "../aic3106.c",line 277,column 4,is_stmt
	.dwpsn	file "../aic3106.c",line 278,column 1,is_stmt
           LDW     .D2T2   *++SP(24),B3      ; |278| 
           NOP             4
	.dwcfi	cfa_offset, 0
	.dwcfi	restore_reg, 19
	.dwcfi	cfa_offset, 0
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |278| 
           ; BRANCH OCCURS {B3}              ; |278| 
	.dwattr $C$DW$46, DW_AT_TI_end_file("../aic3106.c")
	.dwattr $C$DW$46, DW_AT_TI_end_line(0x116)
	.dwattr $C$DW$46, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$46

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_I2C_read
	.global	_I2C_write

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x60)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$55, DW_AT_name("ICOAR")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ICOAR")
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$56, DW_AT_name("ICIMR")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ICIMR")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$57, DW_AT_name("ICSTR")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ICSTR")
	.dwattr $C$DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$58, DW_AT_name("ICCLKL")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ICCLKL")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$59, DW_AT_name("ICCLKH")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ICCLKH")
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$60, DW_AT_name("ICCNT")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ICCNT")
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$61	.dwtag  DW_TAG_member
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$61, DW_AT_name("ICDRR")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ICDRR")
	.dwattr $C$DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$62	.dwtag  DW_TAG_member
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$62, DW_AT_name("ICSAR")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ICSAR")
	.dwattr $C$DW$62, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$62, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$63	.dwtag  DW_TAG_member
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$63, DW_AT_name("ICDXR")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ICDXR")
	.dwattr $C$DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$63, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$64	.dwtag  DW_TAG_member
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$64, DW_AT_name("ICMDR")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ICMDR")
	.dwattr $C$DW$64, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$64, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$65, DW_AT_name("ICIVR")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ICIVR")
	.dwattr $C$DW$65, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$66, DW_AT_name("ICEMDR")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ICEMDR")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$67	.dwtag  DW_TAG_member
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$67, DW_AT_name("ICPSC")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ICPSC")
	.dwattr $C$DW$67, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$68	.dwtag  DW_TAG_member
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$68, DW_AT_name("REVID1")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_REVID1")
	.dwattr $C$DW$68, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$68, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$69	.dwtag  DW_TAG_member
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$69, DW_AT_name("REVID2")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_REVID2")
	.dwattr $C$DW$69, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$70	.dwtag  DW_TAG_member
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$70, DW_AT_name("RSVD0")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_RSVD0")
	.dwattr $C$DW$70, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$70, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$71	.dwtag  DW_TAG_member
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$71, DW_AT_name("ICPFUNC")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ICPFUNC")
	.dwattr $C$DW$71, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$72	.dwtag  DW_TAG_member
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$72, DW_AT_name("ICPDIR")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ICPDIR")
	.dwattr $C$DW$72, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$72, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$73, DW_AT_name("ICPDIN")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_ICPDIN")
	.dwattr $C$DW$73, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$74	.dwtag  DW_TAG_member
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$74, DW_AT_name("ICPDOUT")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ICPDOUT")
	.dwattr $C$DW$74, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$75, DW_AT_name("ICPDSET")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_ICPDSET")
	.dwattr $C$DW$75, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$76, DW_AT_name("ICPDCLR")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_ICPDCLR")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("i2c_regs_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x20)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$27	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_address_class(0x20)

$C$DW$T$28	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x02)
$C$DW$77	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$77, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$28

$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$20	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)

$C$DW$T$21	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x0c)
$C$DW$78	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$78, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$21

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 228
	.dwcfi	cfa_register, 31
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 4
	.dwcfi	undefined, 5
	.dwcfi	undefined, 6
	.dwcfi	undefined, 7
	.dwcfi	undefined, 8
	.dwcfi	undefined, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	same_value, 26
	.dwcfi	same_value, 27
	.dwcfi	same_value, 28
	.dwcfi	same_value, 29
	.dwcfi	same_value, 30
	.dwcfi	same_value, 31
	.dwcfi	same_value, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 41
	.dwcfi	undefined, 42
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 45
	.dwcfi	undefined, 46
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 49
	.dwcfi	undefined, 50
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 53
	.dwcfi	undefined, 54
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	undefined, 57
	.dwcfi	undefined, 58
	.dwcfi	undefined, 59
	.dwcfi	undefined, 60
	.dwcfi	undefined, 61
	.dwcfi	undefined, 62
	.dwcfi	undefined, 63
	.dwcfi	undefined, 64
	.dwcfi	undefined, 65
	.dwcfi	undefined, 66
	.dwcfi	undefined, 67
	.dwcfi	undefined, 68
	.dwcfi	undefined, 69
	.dwcfi	undefined, 70
	.dwcfi	undefined, 71
	.dwcfi	undefined, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 78
	.dwcfi	undefined, 79
	.dwcfi	undefined, 80
	.dwcfi	undefined, 81
	.dwcfi	undefined, 82
	.dwcfi	undefined, 83
	.dwcfi	undefined, 84
	.dwcfi	undefined, 85
	.dwcfi	undefined, 86
	.dwcfi	undefined, 87
	.dwcfi	undefined, 88
	.dwcfi	undefined, 89
	.dwcfi	undefined, 90
	.dwcfi	undefined, 91
	.dwcfi	undefined, 92
	.dwcfi	undefined, 93
	.dwcfi	undefined, 94
	.dwcfi	undefined, 95
	.dwcfi	undefined, 96
	.dwcfi	undefined, 97
	.dwcfi	undefined, 98
	.dwcfi	undefined, 99
	.dwcfi	undefined, 100
	.dwcfi	undefined, 101
	.dwcfi	undefined, 102
	.dwcfi	undefined, 103
	.dwcfi	undefined, 104
	.dwcfi	undefined, 105
	.dwcfi	undefined, 106
	.dwcfi	undefined, 107
	.dwcfi	undefined, 108
	.dwcfi	undefined, 109
	.dwcfi	undefined, 110
	.dwcfi	undefined, 111
	.dwcfi	undefined, 112
	.dwcfi	undefined, 113
	.dwcfi	undefined, 114
	.dwcfi	undefined, 115
	.dwcfi	undefined, 116
	.dwcfi	undefined, 117
	.dwcfi	undefined, 118
	.dwcfi	undefined, 119
	.dwcfi	undefined, 120
	.dwcfi	undefined, 121
	.dwcfi	undefined, 122
	.dwcfi	undefined, 123
	.dwcfi	undefined, 124
	.dwcfi	undefined, 125
	.dwcfi	undefined, 126
	.dwcfi	undefined, 127
	.dwcfi	undefined, 128
	.dwcfi	undefined, 129
	.dwcfi	undefined, 130
	.dwcfi	undefined, 131
	.dwcfi	undefined, 132
	.dwcfi	undefined, 133
	.dwcfi	undefined, 134
	.dwcfi	undefined, 135
	.dwcfi	undefined, 136
	.dwcfi	undefined, 137
	.dwcfi	undefined, 138
	.dwcfi	undefined, 139
	.dwcfi	undefined, 140
	.dwcfi	undefined, 141
	.dwcfi	undefined, 142
	.dwcfi	undefined, 143
	.dwcfi	undefined, 144
	.dwcfi	undefined, 145
	.dwcfi	undefined, 146
	.dwcfi	undefined, 147
	.dwcfi	undefined, 148
	.dwcfi	undefined, 149
	.dwcfi	undefined, 150
	.dwcfi	undefined, 151
	.dwcfi	undefined, 152
	.dwcfi	undefined, 153
	.dwcfi	undefined, 154
	.dwcfi	undefined, 155
	.dwcfi	undefined, 156
	.dwcfi	undefined, 157
	.dwcfi	undefined, 158
	.dwcfi	undefined, 159
	.dwcfi	undefined, 160
	.dwcfi	undefined, 161
	.dwcfi	undefined, 162
	.dwcfi	undefined, 163
	.dwcfi	undefined, 164
	.dwcfi	undefined, 165
	.dwcfi	undefined, 166
	.dwcfi	undefined, 167
	.dwcfi	undefined, 168
	.dwcfi	undefined, 169
	.dwcfi	undefined, 170
	.dwcfi	undefined, 171
	.dwcfi	undefined, 172
	.dwcfi	undefined, 173
	.dwcfi	undefined, 174
	.dwcfi	undefined, 175
	.dwcfi	undefined, 176
	.dwcfi	undefined, 177
	.dwcfi	undefined, 178
	.dwcfi	undefined, 179
	.dwcfi	undefined, 180
	.dwcfi	undefined, 181
	.dwcfi	undefined, 182
	.dwcfi	undefined, 183
	.dwcfi	undefined, 184
	.dwcfi	undefined, 185
	.dwcfi	undefined, 186
	.dwcfi	undefined, 187
	.dwcfi	undefined, 188
	.dwcfi	undefined, 189
	.dwcfi	undefined, 190
	.dwcfi	undefined, 191
	.dwcfi	undefined, 192
	.dwcfi	undefined, 193
	.dwcfi	undefined, 194
	.dwcfi	undefined, 195
	.dwcfi	undefined, 196
	.dwcfi	undefined, 197
	.dwcfi	undefined, 198
	.dwcfi	undefined, 199
	.dwcfi	undefined, 200
	.dwcfi	undefined, 201
	.dwcfi	undefined, 202
	.dwcfi	undefined, 203
	.dwcfi	undefined, 204
	.dwcfi	undefined, 205
	.dwcfi	undefined, 206
	.dwcfi	undefined, 207
	.dwcfi	undefined, 208
	.dwcfi	undefined, 209
	.dwcfi	undefined, 210
	.dwcfi	undefined, 211
	.dwcfi	undefined, 212
	.dwcfi	undefined, 213
	.dwcfi	undefined, 214
	.dwcfi	undefined, 215
	.dwcfi	undefined, 216
	.dwcfi	undefined, 217
	.dwcfi	undefined, 218
	.dwcfi	undefined, 219
	.dwcfi	undefined, 220
	.dwcfi	undefined, 221
	.dwcfi	undefined, 222
	.dwcfi	undefined, 223
	.dwcfi	undefined, 224
	.dwcfi	undefined, 225
	.dwcfi	undefined, 226
	.dwcfi	undefined, 227
	.dwcfi	undefined, 228
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg0]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg1]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg2]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg3]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg4]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg5]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg6]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg7]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg8]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg9]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg10]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg11]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg12]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg13]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg14]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg15]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg16]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg17]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg18]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg19]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg20]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg21]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_reg22]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg23]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_reg24]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg25]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_reg26]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg27]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg28]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg29]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg30]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg31]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x20]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x21]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x22]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x23]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x24]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x25]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x26]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x27]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x28]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x29]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x30]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x31]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x32]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x33]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x34]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x35]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x36]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x37]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x38]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x39]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x40]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x41]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x42]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x43]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x44]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x45]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x46]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x47]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x48]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x49]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x50]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x51]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x52]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x53]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x54]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x55]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x56]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x57]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x58]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x59]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x60]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x61]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x62]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x63]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x64]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x65]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x66]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x67]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x68]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x69]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x70]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x71]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_regx 0x72]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x73]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_regx 0x74]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_regx 0x75]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_regx 0x76]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_regx 0x77]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_regx 0x78]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_regx 0x79]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

