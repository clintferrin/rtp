;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v7.4.4 *
;* Date/Time created: Wed Jan 17 16:15:02 2018                                *
;******************************************************************************
	.compiler_opts --abi=coffabi --c64p_l1d_workaround=off --endian=little --hll_source=on --long_precision_bits=40 --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --object_format=coff --silicon_version=6740 --symdebug:dwarf 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Disabled                                             *
;*   Optimizing for    : Compile time, Ease of Development                    *
;*                       Based on options: no -o, no -ms                      *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../code/mcasp.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v7.4.4 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("D:\lab1\Debug")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$19)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$19)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$19)
	.dwendtag $C$DW$1


$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("EVMOMAPL138_lpscTransition")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_EVMOMAPL138_lpscTransition")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$37)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$19)
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$42)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$5

;	C:\Program Files\ti\ccsv5\tools\compiler\c6000_7.4.4\bin\acp6x.exe -@C:\\Users\\A02179~1\\AppData\\Local\\Temp\\1133614 
	.sect	".text"
	.clink
	.global	_McASP_Init

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("McASP_Init")
	.dwattr $C$DW$10, DW_AT_low_pc(_McASP_Init)
	.dwattr $C$DW$10, DW_AT_high_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_McASP_Init")
	.dwattr $C$DW$10, DW_AT_external
	.dwattr $C$DW$10, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$10, DW_AT_TI_begin_line(0x2a)
	.dwattr $C$DW$10, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$10, DW_AT_TI_max_frame_size(0x08)
	.dwpsn	file "../code/mcasp.c",line 43,column 1,is_stmt,address _McASP_Init

	.dwfde $C$DW$CIE, _McASP_Init

;******************************************************************************
;* FUNCTION NAME: McASP_Init                                                  *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,SP,A16,A17,A18,A19,A20,A21,A22,A23,A24, *
;*                           A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19,B20, *
;*                           B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,B0,B1,B2,B3,B4,B5,B6,  *
;*                           B7,B8,B9,DP,SP,A16,A17,A18,A19,A20,A21,A22,A23,  *
;*                           A24,A25,A26,A27,A28,A29,A30,A31,B16,B17,B18,B19, *
;*                           B20,B21,B22,B23,B24,B25,B26,B27,B28,B29,B30,B31  *
;*   Local Frame Size  : 0 Args + 0 Auto + 4 Save = 4 byte                    *
;******************************************************************************
_McASP_Init:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
           STW     .D2T2   B3,*SP--(8)       ; |43| 
	.dwcfi	cfa_offset, 8
	.dwcfi	save_reg_to_mem, 19, 0
	.dwpsn	file "../code/mcasp.c",line 45,column 4,is_stmt
           MVKL    .S1     0x1e27000,A4
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_name("_EVMOMAPL138_lpscTransition")
	.dwattr $C$DW$11, DW_AT_TI_call

           CALLP   .S2     _EVMOMAPL138_lpscTransition,B3
||         MVKH    .S1     0x1e27000,A4
||         MVK     .L2     0x1,B4            ; |45| 
||         MVK     .L1     0x7,A6            ; |45| 
||         MVK     .D2     0x3,B6            ; |45| 

$C$RL0:    ; CALL OCCURS {_EVMOMAPL138_lpscTransition} {0}  ; |45| 
	.dwpsn	file "../code/mcasp.c",line 46,column 4,is_stmt
           ZERO    .L2     B4

           MVKL    .S1     0x111111,A6
||         SET     .S2     B4,0x0,0x17,B4

$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_name("_EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$12, DW_AT_TI_call

           CALLP   .S2     _EVMOMAPL138_pinmuxConfig,B3
||         MVKH    .S1     0x111111,A6
||         ZERO    .L1     A4                ; |46| 

$C$RL1:    ; CALL OCCURS {_EVMOMAPL138_pinmuxConfig} {0}  ; |46| 
	.dwpsn	file "../code/mcasp.c",line 47,column 4,is_stmt
           ZERO    .L2     B4

           MVKL    .S1     0x11000,A6
||         SET     .S2     B4,0xc,0x13,B4

$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_name("_EVMOMAPL138_pinmuxConfig")
	.dwattr $C$DW$13, DW_AT_TI_call

           CALLP   .S2     _EVMOMAPL138_pinmuxConfig,B3
||         MVKH    .S1     0x11000,A6
||         MVK     .L1     0x1,A4            ; |47| 

$C$RL2:    ; CALL OCCURS {_EVMOMAPL138_pinmuxConfig} {0}  ; |47| 
	.dwpsn	file "../code/mcasp.c",line 50,column 4,is_stmt
           MVKL    .S1     0x1d00044,A4

           MVKH    .S1     0x1d00044,A4
||         ZERO    .L1     A3                ; |50| 

           STW     .D1T1   A3,*A4            ; |50| 
	.dwpsn	file "../code/mcasp.c",line 56,column 4,is_stmt
           MVK     .S2     32,B5

           ADD     .L2X    A4,B5,B5
||         MVK     .S2     -1,B4             ; |56| 

           STW     .D2T2   B4,*B5            ; |56| 
	.dwpsn	file "../code/mcasp.c",line 57,column 4,is_stmt
           MVKL    .S1     0x1807c,A3

           MVKH    .S1     0x1807c,A3
||         ADD     .L2     4,B5,B4

           STW     .D2T1   A3,*B4            ; |57| 
	.dwpsn	file "../code/mcasp.c",line 58,column 4,is_stmt

           MVK     .S1     272,A3            ; |58| 
||         ADD     .L2     4,B4,B4

           STW     .D2T1   A3,*B4            ; |58| 
	.dwpsn	file "../code/mcasp.c",line 59,column 4,is_stmt

           ADDAW   .D2     B4,3,B5
||         MVK     .L2     3,B4              ; |59| 

           STW     .D2T2   B4,*B5            ; |59| 
	.dwpsn	file "../code/mcasp.c",line 60,column 4,is_stmt

           ADD     .L2     4,B5,B5
||         ZERO    .S2     B4                ; |60| 

           STW     .D2T2   B4,*B5            ; |60| 
	.dwpsn	file "../code/mcasp.c",line 61,column 4,is_stmt
           MVKL    .S2     0xff0008,B4
           MVK     .S2     68,B5

           ADD     .L2X    A4,B5,B5
||         MVKH    .S2     0xff0008,B4

           STW     .D2T2   B4,*B5            ; |61| 
	.dwpsn	file "../code/mcasp.c",line 64,column 4,is_stmt
           MVK     .S1     28,A4

           ADD     .L1X    B5,A4,A4
||         MVK     .S1     -1,A3             ; |64| 

           STW     .D1T1   A3,*A4            ; |64| 
	.dwpsn	file "../code/mcasp.c",line 65,column 4,is_stmt
           MVKL    .S2     0x807c,B4

           MVKH    .S2     0x807c,B4
||         ADD     .L1     4,A4,A3

           STW     .D1T2   B4,*A3            ; |65| 
	.dwpsn	file "../code/mcasp.c",line 66,column 4,is_stmt

           ADD     .L2X    4,A3,B4
||         MVK     .S1     272,A3            ; |66| 

           STW     .D2T1   A3,*B4            ; |66| 
	.dwpsn	file "../code/mcasp.c",line 67,column 4,is_stmt
           MVK     .S1     12,A4

           ADD     .L1X    B4,A4,A4
||         MVK     .S1     3,A3              ; |67| 

           STW     .D1T1   A3,*A4            ; |67| 
	.dwpsn	file "../code/mcasp.c",line 68,column 4,is_stmt

           ADD     .L2X    4,A4,B5
||         ZERO    .S2     B4                ; |68| 

           STW     .D2T2   B4,*B5            ; |68| 
	.dwpsn	file "../code/mcasp.c",line 69,column 4,is_stmt
           MVKL    .S1     0xff0008,A3

           MVKH    .S1     0xff0008,A3
||         ADDAW   .D2     B5,3,B4

           STW     .D2T1   A3,*B4            ; |69| 
	.dwpsn	file "../code/mcasp.c",line 72,column 4,is_stmt
           MVK     .S1     88,A4

           SUB     .L1X    B4,A4,A4
||         ZERO    .S1     A3                ; |72| 

           STW     .D1T1   A3,*A4            ; |72| 
	.dwpsn	file "../code/mcasp.c",line 73,column 4,is_stmt
           ADD     .L2X    4,A4,B5
           MV      .L2X    A3,B4
           STW     .D2T2   B4,*B5            ; |73| 
	.dwpsn	file "../code/mcasp.c",line 74,column 4,is_stmt
           MVK     .S1     60,A4
           ADD     .L1X    B5,A4,A4
           STW     .D1T1   A3,*A4            ; |74| 
	.dwpsn	file "../code/mcasp.c",line 75,column 4,is_stmt
           ADD     .L2X    4,A4,B5
           STW     .D2T2   B4,*B5            ; |75| 
	.dwpsn	file "../code/mcasp.c",line 78,column 4,is_stmt
           MVK     .S1     248,A4

           ADD     .L1X    B5,A4,A4
||         MVK     .S1     13,A3             ; |78| 

           STW     .D1T1   A3,*A4            ; |78| 
	.dwpsn	file "../code/mcasp.c",line 79,column 4,is_stmt

           MVK     .L2     14,B4             ; |79| 
||         ADD     .S2X    4,A4,B5

           STW     .D2T2   B4,*B5            ; |79| 
	.dwpsn	file "../code/mcasp.c",line 82,column 4,is_stmt
           MVKL    .S1     0x1d00010,A3

           MVKH    .S1     0x1d00010,A3
||         ZERO    .L2     B4                ; |82| 

           STW     .D1T2   B4,*A3            ; |82| 
	.dwpsn	file "../code/mcasp.c",line 83,column 4,is_stmt

           ADD     .L2X    4,A3,B5
||         MVK     .S2     2048,B4           ; |83| 

           STW     .D2T2   B4,*B5            ; |83| 
	.dwpsn	file "../code/mcasp.c",line 85,column 4,is_stmt
           MVK     .S1     60,A3

           ADD     .L1X    B5,A3,A3
||         ZERO    .S1     A4                ; |85| 

           STW     .D1T1   A4,*A3            ; |85| 
	.dwpsn	file "../code/mcasp.c",line 86,column 4,is_stmt
           MV      .L2X    A4,B4
           ADD     .L2X    -4,A3,B5
           STW     .D2T2   B4,*B5            ; |86| 
	.dwpsn	file "../code/mcasp.c",line 87,column 4,is_stmt
           ADD     .L1X    -4,B5,A4
           MV      .L1X    B4,A3
           STW     .D1T1   A3,*A4            ; |87| 
	.dwpsn	file "../code/mcasp.c",line 89,column 1,is_stmt
           LDW     .D2T2   *++SP(8),B3       ; |89| 
           NOP             4
	.dwcfi	cfa_offset, 0
	.dwcfi	restore_reg, 19
	.dwcfi	cfa_offset, 0
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |89| 
           ; BRANCH OCCURS {B3}              ; |89| 
	.dwattr $C$DW$10, DW_AT_TI_end_file("../code/mcasp.c")
	.dwattr $C$DW$10, DW_AT_TI_end_line(0x59)
	.dwattr $C$DW$10, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$10

	.sect	".text"
	.clink
	.global	_McASP_Start

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("McASP_Start")
	.dwattr $C$DW$15, DW_AT_low_pc(_McASP_Start)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_McASP_Start")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x5f)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "../code/mcasp.c",line 96,column 1,is_stmt,address _McASP_Start

	.dwfde $C$DW$CIE, _McASP_Start

;******************************************************************************
;* FUNCTION NAME: McASP_Start                                                 *
;*                                                                            *
;*   Regs Modified     : A0,A3,A4,B0,B4,B5                                    *
;*   Regs Used         : A0,A3,A4,B0,B3,B4,B5                                 *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
_McASP_Start:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
	.dwcfi	save_reg_to_reg, 228, 19
	.dwpsn	file "../code/mcasp.c",line 99,column 2,is_stmt
           MVKL    .S1     0x1d000a0,A4
           MVKH    .S1     0x1d000a0,A4
           LDW     .D1T1   *A4,A3            ; |99| 
           NOP             4
           SET     .S1     A3,9,9,A3         ; |99| 
           STW     .D1T1   A3,*A4            ; |99| 
	.dwpsn	file "../code/mcasp.c",line 100,column 12,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |100| 
           NOP             4
           EXTU    .S2     B4,22,31,B0       ; |100| 
   [ B0]   BNOP    .S1     $C$L2,5           ; |100| 
           ; BRANCHCC OCCURS {$C$L2}         ; |100| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L1:    
$C$DW$L$_McASP_Start$2$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |100| 
           NOP             4
           EXTU    .S1     A3,22,31,A0       ; |100| 
   [!A0]   BNOP    .S1     $C$L1,5           ; |100| 
           ; BRANCHCC OCCURS {$C$L1}         ; |100| 
$C$DW$L$_McASP_Start$2$E:
;** --------------------------------------------------------------------------*
$C$L2:    
	.dwpsn	file "../code/mcasp.c",line 101,column 5,is_stmt
           MVKL    .S1     0x1d00060,A4
           MVKH    .S1     0x1d00060,A4
           LDW     .D1T1   *A4,A3            ; |101| 
           NOP             4
           OR      .L1     2,A3,A3           ; |101| 
           STW     .D1T1   A3,*A4            ; |101| 
	.dwpsn	file "../code/mcasp.c",line 102,column 12,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |102| 
           NOP             4
           AND     .L2     2,B4,B0           ; |102| 
   [ B0]   BNOP    .S1     $C$L4,5           ; |102| 
           ; BRANCHCC OCCURS {$C$L4}         ; |102| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L3:    
$C$DW$L$_McASP_Start$4$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |102| 
           NOP             4
           AND     .L1     2,A3,A0           ; |102| 
   [!A0]   BNOP    .S1     $C$L3,5           ; |102| 
           ; BRANCHCC OCCURS {$C$L3}         ; |102| 
$C$DW$L$_McASP_Start$4$E:
;** --------------------------------------------------------------------------*
$C$L4:    
	.dwpsn	file "../code/mcasp.c",line 104,column 5,is_stmt
           MVKL    .S1     0x1d000a0,A4
           MVKH    .S1     0x1d000a0,A4
           LDW     .D1T1   *A4,A3            ; |104| 
           NOP             4
           SET     .S1     A3,8,8,A3         ; |104| 
           STW     .D1T1   A3,*A4            ; |104| 
	.dwpsn	file "../code/mcasp.c",line 105,column 12,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |105| 
           NOP             4
           EXTU    .S2     B4,23,31,B0       ; |105| 
   [ B0]   BNOP    .S1     $C$L6,5           ; |105| 
           ; BRANCHCC OCCURS {$C$L6}         ; |105| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L5:    
$C$DW$L$_McASP_Start$6$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |105| 
           NOP             4
           EXTU    .S1     A3,23,31,A0       ; |105| 
   [!A0]   BNOP    .S1     $C$L5,5           ; |105| 
           ; BRANCHCC OCCURS {$C$L5}         ; |105| 
$C$DW$L$_McASP_Start$6$E:
;** --------------------------------------------------------------------------*
$C$L6:    
	.dwpsn	file "../code/mcasp.c",line 106,column 5,is_stmt
           MVKL    .S1     0x1d00060,A4
           MVKH    .S1     0x1d00060,A4
           LDW     .D1T1   *A4,A3            ; |106| 
           NOP             4
           OR      .L1     1,A3,A3           ; |106| 
           STW     .D1T1   A3,*A4            ; |106| 
	.dwpsn	file "../code/mcasp.c",line 107,column 12,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |107| 
           NOP             4
           AND     .L2     1,B4,B0           ; |107| 
   [ B0]   BNOP    .S1     $C$L8,5           ; |107| 
           ; BRANCHCC OCCURS {$C$L8}         ; |107| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L7:    
$C$DW$L$_McASP_Start$8$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |107| 
           NOP             4
           AND     .L1     1,A3,A0           ; |107| 
   [!A0]   BNOP    .S1     $C$L7,5           ; |107| 
           ; BRANCHCC OCCURS {$C$L7}         ; |107| 
$C$DW$L$_McASP_Start$8$E:
;** --------------------------------------------------------------------------*
$C$L8:    
	.dwpsn	file "../code/mcasp.c",line 114,column 5,is_stmt

           MVK     .S2     96,B4
||         ZERO    .L1     A3

           ADD     .L2X    A4,B4,B4
||         SET     .S1     A3,0x0,0xf,A3

           STW     .D2T1   A3,*B4            ; |114| 
	.dwpsn	file "../code/mcasp.c",line 115,column 5,is_stmt
           MVK     .S2     32,B4
           ADD     .L2X    A4,B4,B4
           STW     .D2T1   A3,*B4            ; |115| 
	.dwpsn	file "../code/mcasp.c",line 118,column 4,is_stmt
           MVK     .S1     32,A4
           ADD     .L1X    B4,A4,A4
           LDW     .D1T1   *A4,A3            ; |118| 
           NOP             4
           SET     .S1     A3,10,10,A3       ; |118| 
           STW     .D1T1   A3,*A4            ; |118| 
	.dwpsn	file "../code/mcasp.c",line 119,column 11,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |119| 
           NOP             4
           EXTU    .S2     B4,21,31,B0       ; |119| 
   [ B0]   BNOP    .S1     $C$L10,5          ; |119| 
           ; BRANCHCC OCCURS {$C$L10}        ; |119| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L9:    
$C$DW$L$_McASP_Start$10$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |119| 
           NOP             4
           EXTU    .S1     A3,21,31,A0       ; |119| 
   [!A0]   BNOP    .S1     $C$L9,5           ; |119| 
           ; BRANCHCC OCCURS {$C$L9}         ; |119| 
$C$DW$L$_McASP_Start$10$E:
;** --------------------------------------------------------------------------*
$C$L10:    
	.dwpsn	file "../code/mcasp.c",line 120,column 4,is_stmt
           MVKL    .S1     0x1d00060,A4
           MVKH    .S1     0x1d00060,A4
           LDW     .D1T1   *A4,A3            ; |120| 
           NOP             4
           OR      .L1     4,A3,A3           ; |120| 
           STW     .D1T1   A3,*A4            ; |120| 
	.dwpsn	file "../code/mcasp.c",line 121,column 11,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |121| 
           NOP             4
           AND     .L2     4,B4,B0           ; |121| 
   [ B0]   BNOP    .S1     $C$L12,5          ; |121| 
           ; BRANCHCC OCCURS {$C$L12}        ; |121| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L11:    
$C$DW$L$_McASP_Start$12$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |121| 
           NOP             4
           AND     .L1     4,A3,A0           ; |121| 
   [!A0]   BNOP    .S1     $C$L11,5          ; |121| 
           ; BRANCHCC OCCURS {$C$L11}        ; |121| 
$C$DW$L$_McASP_Start$12$E:
;** --------------------------------------------------------------------------*
$C$L12:    
	.dwpsn	file "../code/mcasp.c",line 124,column 4,is_stmt
           MVKL    .S2     0x1d0022c,B5

           MVKH    .S2     0x1d0022c,B5
||         ZERO    .L2     B4                ; |124| 

           STW     .D2T2   B4,*B5            ; |124| 
	.dwpsn	file "../code/mcasp.c",line 126,column 4,is_stmt
           MVKL    .S1     0x1d000a0,A4
           MVKH    .S1     0x1d000a0,A4
           LDW     .D1T1   *A4,A3            ; |126| 
           NOP             4
           SET     .S1     A3,11,11,A3       ; |126| 
           STW     .D1T1   A3,*A4            ; |126| 
	.dwpsn	file "../code/mcasp.c",line 127,column 11,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |127| 
           NOP             4
           EXTU    .S2     B4,20,31,B0       ; |127| 
   [ B0]   BNOP    .S1     $C$L14,5          ; |127| 
           ; BRANCHCC OCCURS {$C$L14}        ; |127| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L13:    
$C$DW$L$_McASP_Start$14$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |127| 
           NOP             4
           EXTU    .S1     A3,20,31,A0       ; |127| 
   [!A0]   BNOP    .S1     $C$L13,5          ; |127| 
           ; BRANCHCC OCCURS {$C$L13}        ; |127| 
$C$DW$L$_McASP_Start$14$E:
;** --------------------------------------------------------------------------*
$C$L14:    
	.dwpsn	file "../code/mcasp.c",line 128,column 4,is_stmt
           MVKL    .S1     0x1d00060,A4
           MVKH    .S1     0x1d00060,A4
           LDW     .D1T1   *A4,A3            ; |128| 
           NOP             4
           OR      .L1     8,A3,A3           ; |128| 
           STW     .D1T1   A3,*A4            ; |128| 
	.dwpsn	file "../code/mcasp.c",line 129,column 11,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |129| 
           NOP             4
           AND     .L2     8,B4,B0           ; |129| 
   [ B0]   BNOP    .S1     $C$L16,5          ; |129| 
           ; BRANCHCC OCCURS {$C$L16}        ; |129| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L15:    
$C$DW$L$_McASP_Start$16$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |129| 
           NOP             4
           AND     .L1     8,A3,A0           ; |129| 
   [!A0]   BNOP    .S1     $C$L15,5          ; |129| 
           ; BRANCHCC OCCURS {$C$L15}        ; |129| 
$C$DW$L$_McASP_Start$16$E:
;** --------------------------------------------------------------------------*
$C$L16:    
	.dwpsn	file "../code/mcasp.c",line 131,column 4,is_stmt
           MVKL    .S1     0x1d000a0,A4
           MVKH    .S1     0x1d000a0,A4
           LDW     .D1T1   *A4,A3            ; |131| 
           NOP             4
           SET     .S1     A3,12,12,A3       ; |131| 
           STW     .D1T1   A3,*A4            ; |131| 
	.dwpsn	file "../code/mcasp.c",line 132,column 11,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |132| 
           NOP             4
           EXTU    .S2     B4,19,31,B0       ; |132| 
   [ B0]   BNOP    .S1     $C$L18,5          ; |132| 
           ; BRANCHCC OCCURS {$C$L18}        ; |132| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L17:    
$C$DW$L$_McASP_Start$18$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |132| 
           NOP             4
           EXTU    .S1     A3,19,31,A0       ; |132| 
   [!A0]   BNOP    .S1     $C$L17,5          ; |132| 
           ; BRANCHCC OCCURS {$C$L17}        ; |132| 
$C$DW$L$_McASP_Start$18$E:
;** --------------------------------------------------------------------------*
$C$L18:    
	.dwpsn	file "../code/mcasp.c",line 133,column 4,is_stmt
           MVKL    .S1     0x1d00060,A4
           MVKH    .S1     0x1d00060,A4
           LDW     .D1T1   *A4,A3            ; |133| 
           NOP             4
           SET     .S1     A3,4,4,A3         ; |133| 
           STW     .D1T1   A3,*A4            ; |133| 
	.dwpsn	file "../code/mcasp.c",line 134,column 11,is_stmt
           MV      .L2X    A4,B4
           LDW     .D2T2   *B4,B4            ; |134| 
           NOP             4
           EXTU    .S2     B4,27,31,B0       ; |134| 
   [ B0]   BNOP    .S1     $C$L20,5          ; |134| 
           ; BRANCHCC OCCURS {$C$L20}        ; |134| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L19:    
$C$DW$L$_McASP_Start$20$B:
           MV      .L1     A4,A3
           LDW     .D1T1   *A3,A3            ; |134| 
           NOP             4
           EXTU    .S1     A3,27,31,A0       ; |134| 
   [!A0]   BNOP    .S1     $C$L19,5          ; |134| 
           ; BRANCHCC OCCURS {$C$L19}        ; |134| 
$C$DW$L$_McASP_Start$20$E:
;** --------------------------------------------------------------------------*
$C$L20:    
	.dwpsn	file "../code/mcasp.c",line 137,column 10,is_stmt
           MVK     .S1     128,A3
           SUB     .L1X    B5,A3,A3
           LDW     .D1T1   *A3,A3            ; |137| 
           NOP             4
           EXTU    .S1     A3,27,31,A0       ; |137| 
   [ A0]   BNOP    .S1     $C$L22,5          ; |137| 
           ; BRANCHCC OCCURS {$C$L22}        ; |137| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Software pipelining disabled
;*----------------------------------------------------------------------------*
$C$L21:    
$C$DW$L$_McASP_Start$22$B:
           MVK     .S1     128,A3
           SUB     .L1X    B5,A3,A3
           LDW     .D1T1   *A3,A3            ; |137| 
           NOP             4
           EXTU    .S1     A3,27,31,A0       ; |137| 
   [!A0]   BNOP    .S1     $C$L21,5          ; |137| 
           ; BRANCHCC OCCURS {$C$L21}        ; |137| 
$C$DW$L$_McASP_Start$22$E:
;** --------------------------------------------------------------------------*
$C$L22:    
	.dwpsn	file "../code/mcasp.c",line 138,column 4,is_stmt

           MV      .L1X    B5,A4
||         ZERO    .S1     A3                ; |138| 

           STW     .D1T1   A3,*A4            ; |138| 
	.dwpsn	file "../code/mcasp.c",line 140,column 1,is_stmt
	.dwcfi	cfa_offset, 0
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_return
           RETNOP  .S2     B3,5              ; |140| 
           ; BRANCH OCCURS {B3}              ; |140| 

$C$DW$17	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$17, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L21:1:1516230902")
	.dwattr $C$DW$17, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$17, DW_AT_TI_begin_line(0x89)
	.dwattr $C$DW$17, DW_AT_TI_end_line(0x89)
$C$DW$18	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$18, DW_AT_low_pc($C$DW$L$_McASP_Start$22$B)
	.dwattr $C$DW$18, DW_AT_high_pc($C$DW$L$_McASP_Start$22$E)
	.dwendtag $C$DW$17


$C$DW$19	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$19, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L19:1:1516230902")
	.dwattr $C$DW$19, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$19, DW_AT_TI_begin_line(0x86)
	.dwattr $C$DW$19, DW_AT_TI_end_line(0x86)
$C$DW$20	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$20, DW_AT_low_pc($C$DW$L$_McASP_Start$20$B)
	.dwattr $C$DW$20, DW_AT_high_pc($C$DW$L$_McASP_Start$20$E)
	.dwendtag $C$DW$19


$C$DW$21	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$21, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L17:1:1516230902")
	.dwattr $C$DW$21, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x84)
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x84)
$C$DW$22	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$22, DW_AT_low_pc($C$DW$L$_McASP_Start$18$B)
	.dwattr $C$DW$22, DW_AT_high_pc($C$DW$L$_McASP_Start$18$E)
	.dwendtag $C$DW$21


$C$DW$23	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$23, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L15:1:1516230902")
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x81)
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x81)
$C$DW$24	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$24, DW_AT_low_pc($C$DW$L$_McASP_Start$16$B)
	.dwattr $C$DW$24, DW_AT_high_pc($C$DW$L$_McASP_Start$16$E)
	.dwendtag $C$DW$23


$C$DW$25	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$25, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L13:1:1516230902")
	.dwattr $C$DW$25, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$25, DW_AT_TI_begin_line(0x7f)
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x7f)
$C$DW$26	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$26, DW_AT_low_pc($C$DW$L$_McASP_Start$14$B)
	.dwattr $C$DW$26, DW_AT_high_pc($C$DW$L$_McASP_Start$14$E)
	.dwendtag $C$DW$25


$C$DW$27	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$27, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L11:1:1516230902")
	.dwattr $C$DW$27, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$27, DW_AT_TI_begin_line(0x79)
	.dwattr $C$DW$27, DW_AT_TI_end_line(0x79)
$C$DW$28	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$28, DW_AT_low_pc($C$DW$L$_McASP_Start$12$B)
	.dwattr $C$DW$28, DW_AT_high_pc($C$DW$L$_McASP_Start$12$E)
	.dwendtag $C$DW$27


$C$DW$29	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$29, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L9:1:1516230902")
	.dwattr $C$DW$29, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$29, DW_AT_TI_begin_line(0x77)
	.dwattr $C$DW$29, DW_AT_TI_end_line(0x77)
$C$DW$30	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$30, DW_AT_low_pc($C$DW$L$_McASP_Start$10$B)
	.dwattr $C$DW$30, DW_AT_high_pc($C$DW$L$_McASP_Start$10$E)
	.dwendtag $C$DW$29


$C$DW$31	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$31, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L7:1:1516230902")
	.dwattr $C$DW$31, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0x6b)
	.dwattr $C$DW$31, DW_AT_TI_end_line(0x6b)
$C$DW$32	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$32, DW_AT_low_pc($C$DW$L$_McASP_Start$8$B)
	.dwattr $C$DW$32, DW_AT_high_pc($C$DW$L$_McASP_Start$8$E)
	.dwendtag $C$DW$31


$C$DW$33	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$33, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L5:1:1516230902")
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x69)
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x69)
$C$DW$34	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$34, DW_AT_low_pc($C$DW$L$_McASP_Start$6$B)
	.dwattr $C$DW$34, DW_AT_high_pc($C$DW$L$_McASP_Start$6$E)
	.dwendtag $C$DW$33


$C$DW$35	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$35, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L3:1:1516230902")
	.dwattr $C$DW$35, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$35, DW_AT_TI_begin_line(0x66)
	.dwattr $C$DW$35, DW_AT_TI_end_line(0x66)
$C$DW$36	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$36, DW_AT_low_pc($C$DW$L$_McASP_Start$4$B)
	.dwattr $C$DW$36, DW_AT_high_pc($C$DW$L$_McASP_Start$4$E)
	.dwendtag $C$DW$35


$C$DW$37	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$37, DW_AT_name("D:\lab1\Debug\mcasp.asm:$C$L1:1:1516230902")
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../code/mcasp.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0x64)
	.dwattr $C$DW$37, DW_AT_TI_end_line(0x64)
$C$DW$38	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$38, DW_AT_low_pc($C$DW$L$_McASP_Start$2$B)
	.dwattr $C$DW$38, DW_AT_high_pc($C$DW$L$_McASP_Start$2$E)
	.dwendtag $C$DW$37

	.dwattr $C$DW$15, DW_AT_TI_end_file("../code/mcasp.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x8c)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_EVMOMAPL138_pinmuxConfig
	.global	_EVMOMAPL138_lpscTransition

;******************************************************************************
;* BUILD ATTRIBUTES                                                           *
;******************************************************************************
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_needed(0)
	.battr "TI", Tag_File, 1, Tag_ABI_stack_align_preserved(0)
	.battr "TI", Tag_File, 1, Tag_Tramps_Use_SOC(1)

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0xa80)
$C$DW$39	.dwtag  DW_TAG_member
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$39, DW_AT_name("REVID")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_REVID")
	.dwattr $C$DW$39, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$39, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$40	.dwtag  DW_TAG_member
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$40, DW_AT_name("RSVD0")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_RSVD0")
	.dwattr $C$DW$40, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$40, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$41, DW_AT_name("INTEVAL")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_INTEVAL")
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$42, DW_AT_name("RSVD1")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_RSVD1")
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$43, DW_AT_name("MERRPR0")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_MERRPR0")
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$44, DW_AT_name("RSVD2")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_RSVD2")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$45, DW_AT_name("MERRCR0")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_MERRCR0")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$46, DW_AT_name("RSVD3")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_RSVD3")
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$47, DW_AT_name("PERRPR")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_PERRPR")
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$48, DW_AT_name("RSVD4")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_RSVD4")
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$49, DW_AT_name("PERRCR")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_PERRCR")
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$50, DW_AT_name("RSVD5")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_RSVD5")
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$51, DW_AT_name("PTCMD")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_PTCMD")
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x120]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$52, DW_AT_name("RSVD6")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_RSVD6")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x124]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$53, DW_AT_name("PTSTAT")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_PTSTAT")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x128]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$54, DW_AT_name("RSVD7")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_RSVD7")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$55, DW_AT_name("PDSTAT0")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_PDSTAT0")
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x200]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$56, DW_AT_name("PDSTAT1")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_PDSTAT1")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x204]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$57, DW_AT_name("RSVD8")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_RSVD8")
	.dwattr $C$DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x208]
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$58, DW_AT_name("PDCTL0")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_PDCTL0")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x300]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$59, DW_AT_name("PDCTL1")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_PDCTL1")
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x304]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$60, DW_AT_name("RSVD9")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_RSVD9")
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x308]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$61	.dwtag  DW_TAG_member
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$61, DW_AT_name("PDCFG0")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_PDCFG0")
	.dwattr $C$DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x400]
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$62	.dwtag  DW_TAG_member
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$62, DW_AT_name("PDCFG1")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_PDCFG1")
	.dwattr $C$DW$62, DW_AT_data_member_location[DW_OP_plus_uconst 0x404]
	.dwattr $C$DW$62, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$63	.dwtag  DW_TAG_member
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$63, DW_AT_name("RSVD10")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_RSVD10")
	.dwattr $C$DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x408]
	.dwattr $C$DW$63, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$64	.dwtag  DW_TAG_member
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$64, DW_AT_name("MDSTAT")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_MDSTAT")
	.dwattr $C$DW$64, DW_AT_data_member_location[DW_OP_plus_uconst 0x800]
	.dwattr $C$DW$64, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$65, DW_AT_name("RSVD11")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_RSVD11")
	.dwattr $C$DW$65, DW_AT_data_member_location[DW_OP_plus_uconst 0x880]
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$66, DW_AT_name("MDCTL")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_MDCTL")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0xa00]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30

$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("psc_regs_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x20)

$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x2c0)
$C$DW$67	.dwtag  DW_TAG_member
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$67, DW_AT_name("REVID")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_REVID")
	.dwattr $C$DW$67, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$68	.dwtag  DW_TAG_member
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$68, DW_AT_name("RSVD0")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_RSVD0")
	.dwattr $C$DW$68, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$68, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$69	.dwtag  DW_TAG_member
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$69, DW_AT_name("PFUNC")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_PFUNC")
	.dwattr $C$DW$69, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$70	.dwtag  DW_TAG_member
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$70, DW_AT_name("PDIR")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_PDIR")
	.dwattr $C$DW$70, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$70, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$71	.dwtag  DW_TAG_member
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$71, DW_AT_name("PDOUT")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_PDOUT")
	.dwattr $C$DW$71, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$72	.dwtag  DW_TAG_member
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$72, DW_AT_name("PDIN")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_PDIN")
	.dwattr $C$DW$72, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$72, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$73, DW_AT_name("PDCLR")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_PDCLR")
	.dwattr $C$DW$73, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$74	.dwtag  DW_TAG_member
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$74, DW_AT_name("RSVD1")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_RSVD1")
	.dwattr $C$DW$74, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$75, DW_AT_name("GBLCTL")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_GBLCTL")
	.dwattr $C$DW$75, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$76, DW_AT_name("AMUTE")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_AMUTE")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$77	.dwtag  DW_TAG_member
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$77, DW_AT_name("DLBCTL")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_DLBCTL")
	.dwattr $C$DW$77, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$77, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$78	.dwtag  DW_TAG_member
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$78, DW_AT_name("DITCTL")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_DITCTL")
	.dwattr $C$DW$78, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$78, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$79	.dwtag  DW_TAG_member
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$79, DW_AT_name("RSVD2")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_RSVD2")
	.dwattr $C$DW$79, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$79, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$80	.dwtag  DW_TAG_member
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$80, DW_AT_name("RGBLCTL")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_RGBLCTL")
	.dwattr $C$DW$80, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$80, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$81	.dwtag  DW_TAG_member
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$81, DW_AT_name("RMASK")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_RMASK")
	.dwattr $C$DW$81, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$81, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$82	.dwtag  DW_TAG_member
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$82, DW_AT_name("RFMT")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_RFMT")
	.dwattr $C$DW$82, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$82, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$83, DW_AT_name("AFSRCTL")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_AFSRCTL")
	.dwattr $C$DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$84, DW_AT_name("ACLKRCTL")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_ACLKRCTL")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$85, DW_AT_name("AHCLKRCTL")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_AHCLKRCTL")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$86, DW_AT_name("RTDM")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_RTDM")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$87, DW_AT_name("RINTCTL")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_RINTCTL")
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$88, DW_AT_name("RSTAT")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_RSTAT")
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$89, DW_AT_name("RSLOT")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_RSLOT")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x84]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$90, DW_AT_name("RCLKCHK")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_RCLKCHK")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$91, DW_AT_name("REVTCTL")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_REVTCTL")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x8c]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$92, DW_AT_name("RSVD3")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_RSVD3")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$93, DW_AT_name("XGBLCTL")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_XGBLCTL")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$94, DW_AT_name("XMASK")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_XMASK")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0xa4]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$95, DW_AT_name("XFMT")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_XFMT")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$96, DW_AT_name("AFSXCTL")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_AFSXCTL")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0xac]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$97, DW_AT_name("ACLKXCTL")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_ACLKXCTL")
	.dwattr $C$DW$97, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$98, DW_AT_name("AHCLKXCTL")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_AHCLKXCTL")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0xb4]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$99, DW_AT_name("XTDM")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_XTDM")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$100, DW_AT_name("XINTCTL")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_XINTCTL")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$101, DW_AT_name("XSTAT")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_XSTAT")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$102, DW_AT_name("XSLOT")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_XSLOT")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$103, DW_AT_name("XCLKCHK")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_XCLKCHK")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$104, DW_AT_name("XEVTCTL")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_XEVTCTL")
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$105, DW_AT_name("RSVD4")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_RSVD4")
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$106, DW_AT_name("DITCSRA0")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_DITCSRA0")
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$107, DW_AT_name("DITCSRA1")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_DITCSRA1")
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$108, DW_AT_name("DITCSRA2")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_DITCSRA2")
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$109, DW_AT_name("DITCSRA3")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_DITCSRA3")
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$110, DW_AT_name("DITCSRA4")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_DITCSRA4")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$111, DW_AT_name("DITCSRA5")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_DITCSRA5")
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x114]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$112, DW_AT_name("DITCSRB0")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_DITCSRB0")
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0x118]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$113, DW_AT_name("DITCSRB1")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_DITCSRB1")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x11c]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$114, DW_AT_name("DITCSRB2")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_DITCSRB2")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x120]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$115, DW_AT_name("DITCSRB3")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_DITCSRB3")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x124]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$116, DW_AT_name("DITCSRB4")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_DITCSRB4")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x128]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$117, DW_AT_name("DITCSRB5")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_DITCSRB5")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$118, DW_AT_name("DITUDRA0")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_DITUDRA0")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$119, DW_AT_name("DITUDRA1")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_DITUDRA1")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x134]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$120, DW_AT_name("DITUDRA2")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_DITUDRA2")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x138]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$121, DW_AT_name("DITUDRA3")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_DITUDRA3")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x13c]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$122, DW_AT_name("DITUDRA4")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_DITUDRA4")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x140]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$123, DW_AT_name("DITUDRA5")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_DITUDRA5")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x144]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$124, DW_AT_name("DITUDRB0")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_DITUDRB0")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x148]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$125, DW_AT_name("DITUDRB1")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_DITUDRB1")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x14c]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$126, DW_AT_name("DITUDRB2")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_DITUDRB2")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x150]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$127, DW_AT_name("DITUDRB3")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_DITUDRB3")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x154]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$128, DW_AT_name("DITUDRB4")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_DITUDRB4")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x158]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$129, DW_AT_name("DITUDRB5")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_DITUDRB5")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x15c]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$130, DW_AT_name("RSVD5")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_RSVD5")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x160]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$131, DW_AT_name("SRCTL0")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_SRCTL0")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x180]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$132, DW_AT_name("SRCTL1")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_SRCTL1")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x184]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$133, DW_AT_name("SRCTL2")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_SRCTL2")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x188]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$134, DW_AT_name("SRCTL3")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_SRCTL3")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x18c]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$135, DW_AT_name("SRCTL4")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_SRCTL4")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x190]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$136, DW_AT_name("SRCTL5")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_SRCTL5")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x194]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$137, DW_AT_name("SRCTL6")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_SRCTL6")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x198]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$138, DW_AT_name("SRCTL7")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_SRCTL7")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x19c]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$139, DW_AT_name("SRCTL8")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_SRCTL8")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a0]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$140, DW_AT_name("SRCTL9")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_SRCTL9")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a4]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$141, DW_AT_name("SRCTL10")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_SRCTL10")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a8]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$142, DW_AT_name("SRCTL11")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_SRCTL11")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x1ac]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$143, DW_AT_name("SRCTL12")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_SRCTL12")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b0]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$144, DW_AT_name("SRCTL13")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_SRCTL13")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b4]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$145, DW_AT_name("SRCTL14")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_SRCTL14")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b8]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$146, DW_AT_name("SRCTL15")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_SRCTL15")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x1bc]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$147, DW_AT_name("RSVD6")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_RSVD6")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c0]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$148, DW_AT_name("XBUF0")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_XBUF0")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x200]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$149, DW_AT_name("XBUF1")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_XBUF1")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x204]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$150, DW_AT_name("XBUF2")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_XBUF2")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x208]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$151, DW_AT_name("XBUF3")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_XBUF3")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x20c]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$152, DW_AT_name("XBUF4")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_XBUF4")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x210]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$153, DW_AT_name("XBUF5")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_XBUF5")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x214]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$154, DW_AT_name("XBUF6")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_XBUF6")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x218]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$155, DW_AT_name("XBUF7")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_XBUF7")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x21c]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$156, DW_AT_name("XBUF8")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_XBUF8")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x220]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$157, DW_AT_name("XBUF9")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_XBUF9")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x224]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$158, DW_AT_name("XBUF10")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_XBUF10")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x228]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$159, DW_AT_name("XBUF11")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_XBUF11")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x22c]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$160, DW_AT_name("XBUF12")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_XBUF12")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x230]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$161, DW_AT_name("XBUF13")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_XBUF13")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x234]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$162, DW_AT_name("XBUF14")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_XBUF14")
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x238]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$163, DW_AT_name("XBUF15")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_XBUF15")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x23c]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$164, DW_AT_name("RSVD7")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_RSVD7")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x240]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$165, DW_AT_name("RBUF0")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_RBUF0")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x280]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$166, DW_AT_name("RBUF1")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_RBUF1")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x284]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$167, DW_AT_name("RBUF2")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_RBUF2")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x288]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$168, DW_AT_name("RBUF3")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_RBUF3")
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x28c]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$169, DW_AT_name("RBUF4")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_RBUF4")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x290]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$170, DW_AT_name("RBUF5")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_RBUF5")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x294]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$171, DW_AT_name("RBUF6")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_RBUF6")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x298]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$172, DW_AT_name("RBUF7")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_RBUF7")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x29c]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$173, DW_AT_name("RBUF8")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_RBUF8")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a0]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$174, DW_AT_name("RBUF9")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_RBUF9")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a4]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$175, DW_AT_name("RBUF10")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_RBUF10")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a8]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$176, DW_AT_name("RBUF11")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_RBUF11")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x2ac]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$177, DW_AT_name("RBUF12")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_RBUF12")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b0]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$178, DW_AT_name("RBUF13")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_RBUF13")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b4]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$179, DW_AT_name("RBUF14")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_RBUF14")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b8]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$180, DW_AT_name("RBUF15")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_RBUF15")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x2bc]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35

$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("mcasp_regs_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$20	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)

$C$DW$T$21	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x14)
$C$DW$181	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$181, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x24)
$C$DW$182	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$182, DW_AT_upper_bound(0x08)
	.dwendtag $C$DW$T$22


$C$DW$T$23	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x0c)
$C$DW$183	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$183, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$23


$C$DW$T$24	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$24, DW_AT_byte_size(0xb4)
$C$DW$184	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$184, DW_AT_upper_bound(0x2c)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$25, DW_AT_byte_size(0xd4)
$C$DW$185	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$185, DW_AT_upper_bound(0x34)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$26, DW_AT_byte_size(0xf8)
$C$DW$186	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$186, DW_AT_upper_bound(0x3d)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x3f8)
$C$DW$187	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$187, DW_AT_upper_bound(0xfd)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x80)
$C$DW$188	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$188, DW_AT_upper_bound(0x1f)
	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x180)
$C$DW$189	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$189, DW_AT_upper_bound(0x5f)
	.dwendtag $C$DW$T$29


$C$DW$T$31	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x20)
$C$DW$190	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$190, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x10)
$C$DW$191	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$191, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x30)
$C$DW$192	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$192, DW_AT_upper_bound(0x0b)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x40)
$C$DW$193	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$193, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$34

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 228
	.dwcfi	cfa_register, 31
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 4
	.dwcfi	undefined, 5
	.dwcfi	undefined, 6
	.dwcfi	undefined, 7
	.dwcfi	undefined, 8
	.dwcfi	undefined, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	same_value, 26
	.dwcfi	same_value, 27
	.dwcfi	same_value, 28
	.dwcfi	same_value, 29
	.dwcfi	same_value, 30
	.dwcfi	same_value, 31
	.dwcfi	same_value, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 41
	.dwcfi	undefined, 42
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 45
	.dwcfi	undefined, 46
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 49
	.dwcfi	undefined, 50
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 53
	.dwcfi	undefined, 54
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	undefined, 57
	.dwcfi	undefined, 58
	.dwcfi	undefined, 59
	.dwcfi	undefined, 60
	.dwcfi	undefined, 61
	.dwcfi	undefined, 62
	.dwcfi	undefined, 63
	.dwcfi	undefined, 64
	.dwcfi	undefined, 65
	.dwcfi	undefined, 66
	.dwcfi	undefined, 67
	.dwcfi	undefined, 68
	.dwcfi	undefined, 69
	.dwcfi	undefined, 70
	.dwcfi	undefined, 71
	.dwcfi	undefined, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 78
	.dwcfi	undefined, 79
	.dwcfi	undefined, 80
	.dwcfi	undefined, 81
	.dwcfi	undefined, 82
	.dwcfi	undefined, 83
	.dwcfi	undefined, 84
	.dwcfi	undefined, 85
	.dwcfi	undefined, 86
	.dwcfi	undefined, 87
	.dwcfi	undefined, 88
	.dwcfi	undefined, 89
	.dwcfi	undefined, 90
	.dwcfi	undefined, 91
	.dwcfi	undefined, 92
	.dwcfi	undefined, 93
	.dwcfi	undefined, 94
	.dwcfi	undefined, 95
	.dwcfi	undefined, 96
	.dwcfi	undefined, 97
	.dwcfi	undefined, 98
	.dwcfi	undefined, 99
	.dwcfi	undefined, 100
	.dwcfi	undefined, 101
	.dwcfi	undefined, 102
	.dwcfi	undefined, 103
	.dwcfi	undefined, 104
	.dwcfi	undefined, 105
	.dwcfi	undefined, 106
	.dwcfi	undefined, 107
	.dwcfi	undefined, 108
	.dwcfi	undefined, 109
	.dwcfi	undefined, 110
	.dwcfi	undefined, 111
	.dwcfi	undefined, 112
	.dwcfi	undefined, 113
	.dwcfi	undefined, 114
	.dwcfi	undefined, 115
	.dwcfi	undefined, 116
	.dwcfi	undefined, 117
	.dwcfi	undefined, 118
	.dwcfi	undefined, 119
	.dwcfi	undefined, 120
	.dwcfi	undefined, 121
	.dwcfi	undefined, 122
	.dwcfi	undefined, 123
	.dwcfi	undefined, 124
	.dwcfi	undefined, 125
	.dwcfi	undefined, 126
	.dwcfi	undefined, 127
	.dwcfi	undefined, 128
	.dwcfi	undefined, 129
	.dwcfi	undefined, 130
	.dwcfi	undefined, 131
	.dwcfi	undefined, 132
	.dwcfi	undefined, 133
	.dwcfi	undefined, 134
	.dwcfi	undefined, 135
	.dwcfi	undefined, 136
	.dwcfi	undefined, 137
	.dwcfi	undefined, 138
	.dwcfi	undefined, 139
	.dwcfi	undefined, 140
	.dwcfi	undefined, 141
	.dwcfi	undefined, 142
	.dwcfi	undefined, 143
	.dwcfi	undefined, 144
	.dwcfi	undefined, 145
	.dwcfi	undefined, 146
	.dwcfi	undefined, 147
	.dwcfi	undefined, 148
	.dwcfi	undefined, 149
	.dwcfi	undefined, 150
	.dwcfi	undefined, 151
	.dwcfi	undefined, 152
	.dwcfi	undefined, 153
	.dwcfi	undefined, 154
	.dwcfi	undefined, 155
	.dwcfi	undefined, 156
	.dwcfi	undefined, 157
	.dwcfi	undefined, 158
	.dwcfi	undefined, 159
	.dwcfi	undefined, 160
	.dwcfi	undefined, 161
	.dwcfi	undefined, 162
	.dwcfi	undefined, 163
	.dwcfi	undefined, 164
	.dwcfi	undefined, 165
	.dwcfi	undefined, 166
	.dwcfi	undefined, 167
	.dwcfi	undefined, 168
	.dwcfi	undefined, 169
	.dwcfi	undefined, 170
	.dwcfi	undefined, 171
	.dwcfi	undefined, 172
	.dwcfi	undefined, 173
	.dwcfi	undefined, 174
	.dwcfi	undefined, 175
	.dwcfi	undefined, 176
	.dwcfi	undefined, 177
	.dwcfi	undefined, 178
	.dwcfi	undefined, 179
	.dwcfi	undefined, 180
	.dwcfi	undefined, 181
	.dwcfi	undefined, 182
	.dwcfi	undefined, 183
	.dwcfi	undefined, 184
	.dwcfi	undefined, 185
	.dwcfi	undefined, 186
	.dwcfi	undefined, 187
	.dwcfi	undefined, 188
	.dwcfi	undefined, 189
	.dwcfi	undefined, 190
	.dwcfi	undefined, 191
	.dwcfi	undefined, 192
	.dwcfi	undefined, 193
	.dwcfi	undefined, 194
	.dwcfi	undefined, 195
	.dwcfi	undefined, 196
	.dwcfi	undefined, 197
	.dwcfi	undefined, 198
	.dwcfi	undefined, 199
	.dwcfi	undefined, 200
	.dwcfi	undefined, 201
	.dwcfi	undefined, 202
	.dwcfi	undefined, 203
	.dwcfi	undefined, 204
	.dwcfi	undefined, 205
	.dwcfi	undefined, 206
	.dwcfi	undefined, 207
	.dwcfi	undefined, 208
	.dwcfi	undefined, 209
	.dwcfi	undefined, 210
	.dwcfi	undefined, 211
	.dwcfi	undefined, 212
	.dwcfi	undefined, 213
	.dwcfi	undefined, 214
	.dwcfi	undefined, 215
	.dwcfi	undefined, 216
	.dwcfi	undefined, 217
	.dwcfi	undefined, 218
	.dwcfi	undefined, 219
	.dwcfi	undefined, 220
	.dwcfi	undefined, 221
	.dwcfi	undefined, 222
	.dwcfi	undefined, 223
	.dwcfi	undefined, 224
	.dwcfi	undefined, 225
	.dwcfi	undefined, 226
	.dwcfi	undefined, 227
	.dwcfi	undefined, 228
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg0]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg1]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_reg2]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_reg3]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg4]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg5]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg6]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg7]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg8]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_reg9]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg10]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg11]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg12]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg13]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg14]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_reg15]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_reg16]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_reg17]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg18]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg19]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_reg20]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg21]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_reg22]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_reg23]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg24]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg25]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg26]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg27]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg28]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg29]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg30]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg31]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_regx 0x20]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_regx 0x21]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_regx 0x22]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_regx 0x23]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_regx 0x24]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_regx 0x25]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_regx 0x26]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_regx 0x27]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_regx 0x28]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_regx 0x29]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x30]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x31]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x32]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x33]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x34]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x35]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x36]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x37]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x38]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x39]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$253	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$253, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$254	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$254, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$255	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$255, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$256	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$256, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$257	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$258	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$258, DW_AT_location[DW_OP_regx 0x40]
$C$DW$259	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$259, DW_AT_location[DW_OP_regx 0x41]
$C$DW$260	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$260, DW_AT_location[DW_OP_regx 0x42]
$C$DW$261	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$261, DW_AT_location[DW_OP_regx 0x43]
$C$DW$262	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$262, DW_AT_location[DW_OP_regx 0x44]
$C$DW$263	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$263, DW_AT_location[DW_OP_regx 0x45]
$C$DW$264	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$264, DW_AT_location[DW_OP_regx 0x46]
$C$DW$265	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$265, DW_AT_location[DW_OP_regx 0x47]
$C$DW$266	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$266, DW_AT_location[DW_OP_regx 0x48]
$C$DW$267	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_regx 0x49]
$C$DW$268	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$269	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$270	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$271	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$272	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$273	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$274	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$274, DW_AT_location[DW_OP_regx 0x50]
$C$DW$275	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$275, DW_AT_location[DW_OP_regx 0x51]
$C$DW$276	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_regx 0x52]
$C$DW$277	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_regx 0x53]
$C$DW$278	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_regx 0x54]
$C$DW$279	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_regx 0x55]
$C$DW$280	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_regx 0x56]
$C$DW$281	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_regx 0x57]
$C$DW$282	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_regx 0x58]
$C$DW$283	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_regx 0x59]
$C$DW$284	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$285	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$286	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$287	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$288	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$289	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$290	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_regx 0x60]
$C$DW$291	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_regx 0x61]
$C$DW$292	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_regx 0x62]
$C$DW$293	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_regx 0x63]
$C$DW$294	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_regx 0x64]
$C$DW$295	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_regx 0x65]
$C$DW$296	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_regx 0x66]
$C$DW$297	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$297, DW_AT_location[DW_OP_regx 0x67]
$C$DW$298	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$298, DW_AT_location[DW_OP_regx 0x68]
$C$DW$299	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$299, DW_AT_location[DW_OP_regx 0x69]
$C$DW$300	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$300, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$301	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$301, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$302	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$302, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$303	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$304	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$305	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$305, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$306	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$306, DW_AT_location[DW_OP_regx 0x70]
$C$DW$307	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$307, DW_AT_location[DW_OP_regx 0x71]
$C$DW$308	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$308, DW_AT_location[DW_OP_regx 0x72]
$C$DW$309	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$309, DW_AT_location[DW_OP_regx 0x73]
$C$DW$310	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$310, DW_AT_location[DW_OP_regx 0x74]
$C$DW$311	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$311, DW_AT_location[DW_OP_regx 0x75]
$C$DW$312	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$312, DW_AT_location[DW_OP_regx 0x76]
$C$DW$313	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$313, DW_AT_location[DW_OP_regx 0x77]
$C$DW$314	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$314, DW_AT_location[DW_OP_regx 0x78]
$C$DW$315	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_regx 0x79]
$C$DW$316	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$317	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$318	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$319	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$320	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$320, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$321	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$321, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$322	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$322, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

